from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from people.models import Person
from application import db
from flask_script import Command

class PeopleImageScraper(Command):
    def run(self):
        #get countries
        people_list = Person.query.order_by(
			Person.id.asc()
		)
        browser = webdriver.Chrome()
        for person in people_list:
            name = person.name.replace(' ', '+')
            charity = person.charities[0]
            query = name + ' ' + charity.name
            url = 'https://www.google.com/search?q=' + query + '+logo&source=lnms&tbm=isch'
            browser.get(url)
            try:
                images = browser.find_elements_by_tag_name('img')
                header = False
                for image in images:
                    if image.get_attribute('src') is None:
                        header = True 
                    if image.get_attribute('src') is not None and header:
                        print(image.get_attribute('src'))
                        break
            except TimeoutException:
                print("probelms")