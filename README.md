Arjun Kunjilwar, ak37937, arjun_kunjilwar
Jorge Paredes, jep3485, jeparedes
Rodrigo Villarreal, rv22698, rvnunez
Darrin Hui, dkfhui
Ben Olea, bo4582, benolea

Phase I:
Git SHA - 90d8bebe663b9a3493eb9894b06a84b3eff67877
https://thecharitywatch.org

Estimates:
Arjun - 15 hrs
Jorge -  10 hrs
Rodrigo - 10 hrs
Darrin - 30 hrs
Ben - 10 hrs

Actual
Arjun - 20 hrs
Jorge - 15 hrs
Rodrigo - 12 hrs
Darrin - 15 hrs
Ben - 10 hrs

---------------------------------------------------

Phase II:
Git SHA - f93ab894f6b1909402e1b734f3e18441e2acec3e
https://thecharitywatch.org

Estimates:
Arjun - 25 hrs
Jorge - 20 hrs
Rodrigo - 25 hrs
Darrin - 25 hrs
Ben - 20 hrs

Actual:
Arjun - 60 hrs
Jorge - 55 hrs
Rodrigo - 60 hrs
Darrin - 55 hrs
Ben - 0 hrs

---------------------------------------------------

Phase III:
Git SHA - 4d0bb0fcc135df2bcadb2b1188c0cad599a2c157
https://thecharitywatch.org

Ben and Jorge dropped the class

Estimates:
Arjun - 15 hrs
Rodrigo - 15 hrs
Darrin - 15 hrs

Actual: 
Arjun - 30 hrs
Rodrigo - 30  hrs
Darrin - 30 hrs

---------------------------------------------------

Phase IV:
Git SHA - 9bb01c69c039927a8b9b555f99f5041ee2203782
https://thecharitywatch.org

Estimates:
Arjun - 10 hrs
Rodrigo - 9 hrs
Darrin - 10 hrs

Actual: 
Arjun - 15 hrs
Rodrigo - 15 hrs
Darrin - 15 hrs
