import React from "react";
import NavBar from './NavBar.js';
// OTHER LINK CLASSES
import About from './About.js';
import Home from './Home.js';
import CharityGrid from './model_grids/CharityGrid.js';
import PersonsGrid from './model_grids/PersonsGrid.js';
import CountryGrid from './model_grids/CountryGrid.js';
import { Router, Route } from "react-router-dom";
import Charity from './model_instances/CharityInstance.js'
import Person from './model_instances/PersonInstance.js'
import Country from './model_instances/CountryInstance.js'
// import SearchResults from './SearchResults.js'
import SearchGlobalResults from './search/SearchGlobalResults.js';
import VisualizationPage from './visualizations/VisualizationPage';
import ProviderVisualizationPage from './visualizations/ProviderVisualizationPage';
import history from './history';

import './styles/global.css'

function AppRouter() {

  return (
    <div>
  		<Router history={history}>
        <div className="container-fluid">
  				<NavBar />
          <Route exact path="/" component={Home}/>
          <Route path="/home" component={Home}/>
          <Route path="/about/" component={About} />
  				<Route path="/charities_list/" component={CharityGrid} />
          <Route path="/charity_instance/:ein/" component={Charity} />
  				<Route path="/persons_list/" component={PersonsGrid} />
          <Route path="/person_instance/:id/" component={Person} />
  				<Route path="/countries_list/" component={CountryGrid} />
          <Route path="/country_instance/:abbr/" component={Country} />
          {/* <Route path="/charity_search/:q" component={SearchResults} /> */}
          <Route path="/our-visualizations" component={VisualizationPage} />
          <Route path="/provider-visualizations" component={ProviderVisualizationPage} />
          <Route path="/global_search/:q" component={SearchGlobalResults} />
        </div>
      </Router>
      {/* <div className="footer">
        <h5>Made with ReactJS and Bootstrap by the CharityBros</h5>
      </div> */}
    </div>

	);
}
export default AppRouter;
