import './../node_modules/bootstrap/dist/css/bootstrap.min.css';
import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios';
import AppRouter from './AppRouter.js';
import ErrorBoundary from './ErrorBoundary.js'
import './styles/global.css'

ReactDOM.render(<ErrorBoundary><AppRouter /></ErrorBoundary>, document.getElementById("app"));
