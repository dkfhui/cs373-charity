import React from 'react'
import './styles/home.css'
import carousel1 from './img/carousel1.jpg'
import carousel2 from './img/carousel2.jpg'
import './styles/carousel.css'

const carousel_count = 3
const quotes = ["Giving back is important but so is knowing where your money goes", "Find out which charities are credible", "And say goodbye to scams"]


class Home extends React.Component{
	constructor(props){
		super(props)
		this.state = {}
	}

	render(){
		let carousel_indicators = []
		let carousel_images = []
		for (let index = 0; index < carousel_count; index++) {
			carousel_indicators.push(
				<li key={index} data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
			)
			carousel_images.push(
				<div key={index} className={`carousel-item ${(index == 0) ? 'active':''}`}>
					<div className={'carousel-image carousel-image' + (index + 1)}></div>
					<div className="carousel-caption d-none d-md-block">
						<h1>{quotes[index]}</h1>
					</div>
				</div>
			)
		}
		return(
			<div>
				<div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
					<ol className="carousel-indicators">
						{carousel_indicators}
					</ol>
					<div className="carousel-inner">
						{carousel_images}
					</div>
		  		<a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    		<span className="carousel-control-prev-icon" aria-hidden="true"></span>
		    		<span className="sr-only">Previous</span>
		  		</a>
		  		<a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    		<span className="carousel-control-next-icon" aria-hidden="true"></span>
		    		<span className="sr-only">Next</span>
		  		</a>
				</div>
			</div>
		)
	}
}

export default Home;
