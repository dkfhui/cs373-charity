import React from 'react';

const FilterElements = ({filters, sortOptions, sortBy, orderBy}) => {
	// make filters
	let filterElems = null
	if (filters) {
		filterElems = []
		for (let filterName in filters) {
			let filterOptions = []
			if (filters[filterName]['values']) {
				filters[filterName]['values'].forEach(function(option) {
					filterOptions.push(
						<option value={option}>{option}</option>
					)
				})
				filterElems.push(
					<div className="input-group mb-3">
						<div className="input-group-prepend">
							<span className="input-group-text" id="basic-addon1">{`${filterName} Filter`}</span>
						</div>
						<select className="custom-select state-filter" id="inputGroupSelect03" onChange={filters[filterName]['onChange']}>
							{filterOptions}
						</select>
					</div>
				)
			}
		}
	}
	// make sort options
	let sortElems = null
	if (sortOptions) {
		sortElems = []
		sortOptions.forEach(function(option) {
			sortElems.push(
				<option value={option}>{option}</option>
			)
		})
	}

	return(
		<div className="inner-form">
			{filterElems}
			<div className="input-group mb-3">
				<div className="input-group-prepend">
					<span className="input-group-text" id="basic-addon1">{"Sort By"}</span>
				</div>
				<select className="custom-select" id="inputGroupSelect02" onChange={sortBy}>
					{sortElems}
				</select>
				<div className="input-group-append ascending">
					<button className="btn btn-outline-secondary" type="button" onClick={() => {orderBy('false')}}>&uarr;</button>
				</div>
				<div className="input-group-append descending">
					<button className="btn btn-outline-secondary" type="button" onClick={() => {orderBy('true')}}>&darr;</button>
				</div>
			</div>
		</div>
	);
}

export default FilterElements;
