import React, {Component} from 'react';
import Grid from './Grid.js';
import '../styles/pagination.css'
import PersonCard from '../model_grid_cards/PersonCard.js'
import first_page from "../img/first_page.svg"
import last_page from "../img/last_page.svg"
import next_page from '../img/next_page.svg'
import prev_page from '../img/prev_page.svg'
import update from 'immutability-helper'
import FilterElements from './FilterElements.js'
import SearchModel from '../search/SearchModel.js'
import SearchResults from '../search/SearchResults.js'


const axios = require('axios')
const INSTANCES_PER_PAGE = 12

class PersonsGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {page : 1,
      num_pages: 5,
      people: [],
      roles: null,
      activity: ['None', 'inactive', 'active'],
      sortOptions: ['None', 'name', 'main role', 'main charity', 'weekly hours', 'compensation'],
      chosenRole: null,
      chosenActivity: null,
      sortBy: null,
      desc: 'false',
      searchTerm: '',
      searchClicked: false,
      showSearchResults: false,
      filterError: false
    };
    this.requestNewPeople = this.requestNewPeople.bind(this)
    this.filterByRole = this.filterByRole.bind(this)
    this.filterByActivity = this.filterByActivity.bind(this)
    this.sortPeople = this.sortPeople.bind(this)
    this.changeOrdering = this.changeOrdering.bind(this)
    this.searchTermChanged = this.searchTermChanged.bind(this)
    this.searchClicked = this.searchClicked.bind(this)
    this.setSearchClicked = this.setSearchClicked.bind(this)
  }

  componentDidMount() {
    this.requestFilteringInformation()
    this.requestNewPeople(1)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.location.state.showSearchResults === false)
      this.setState({showSearchResults:false})
  }

  searchTermChanged(event) {
    this.setState({searchTerm: event.target.value})
  }

  searchClicked(event){
    event.preventDefault()
    this.setState({showSearchResults: true})
    this.setState({searchClicked: true})
  }

  setSearchClicked(clicked){
    this.setState({searchClicked: clicked})
  }

  requestFilteringInformation() {
    // get list of states
    let url = `https://api.thecharitywatch.org/person-roles`
    axios.get(url)
      .then(response => {
        this.setState({roles: response.data['main_roles']})
        this.state.roles.unshift("None")
      })
      .catch(error => {console.log(error)})
  }


  requestNewPeople(newPage) {
    // get information about the charities
    if (newPage > 0 && newPage <= this.state.num_pages) {
      this.setState({page: newPage})
      let url = `https://api.thecharitywatch.org/person?${this.state.chosenRole ? 'role=' + this.state.chosenRole + '&' : ''}${this.state.chosenActivity != null ? 'active=' + this.state.chosenActivity + '&' : ''}${this.state.sortBy ? 'orderby=' + this.state.sortBy + '&desc=' + this.state.desc + '&': ''}start=${newPage}&limit=${INSTANCES_PER_PAGE}`
      axios.get(url)
        .then(response =>{
          this.setState({people: response.data['people']})
          this.setState({num_pages: response.data['num_pages']})
          this.setState({filterError: false})
          window.scrollTo(0,0)
        })
        .catch(error=>{
          console.log(error)
          this.setState({filterError: true})
        })
      }
  }

  filterByRole(event) {
    let roleAbbr = event.target.value
    let keyVal = roleAbbr === 'None' ? null : roleAbbr.replace(' ', '+')
    this.setState({chosenRole: keyVal}, () => {
      this.requestNewPeople(1)
    })
  }

  filterByActivity(event) {
    let actAbbr = event.target.value
    let keyVal = actAbbr === 'None' ? null : actAbbr === 'active'
    this.setState({chosenActivity:  keyVal}, () => {
      this.requestNewPeople(1)
    })
  }

  changeOrdering(orderingVal) {
    this.setState({desc: orderingVal}, () => {
      this.requestNewPeople(1)
    })
  }

  sortPeople(event) {
    let sortByVal = event.target.value
    let sortKey = sortByVal === 'None' ? null : sortByVal.replace(' ', '_')
    this.setState({sortBy: sortKey}, () => {
      this.requestNewPeople(1)
    })
  }


  render() {
    let person_cards = []
    for (let i = 0; i < this.state.people.length; ++i) {
      person_cards.push(
        <PersonCard key={this.state.people[i].id} {...this.state.people[i].person}/>
      );
    }
    let filters = {"Role": {'values': this.state.roles, 'onChange': this.filterByRole}, "Active": {'values': this.state.activity,
      'onChange': this.filterByActivity}}
    return (
      <div>
        <form className="form-inline outer-form" onSubmit={this.searchClicked}>
          <FilterElements filters={filters} sortOptions={this.state.sortOptions} sortBy={this.sortPeople} orderBy={this.changeOrdering}/>
          <SearchModel searchType="people" searchTerm={this.state.searchTerm} searchTermChanged={this.searchTermChanged} searchClicked={this.searchClicked}/>
        </form>
        {
          this.state.filterError ? <h1>No Results Found</h1>
          :
          this.state.showSearchResults ? <SearchResults searchTerm={this.state.searchTerm} searchClicked={this.state.searchClicked} searched={this.setSearchClicked} model="person"/>
          :
          <React.Fragment>
            <Grid>
              {person_cards}
            </Grid>,
            <div className="pagination">
              <button className="page-button" id="first_page" type="button" onClick={() => this.requestNewPeople(1)}><img width="30px" src={first_page} /></button>
              <button className="page-button" id="prev_page" onClick={() => this.requestNewPeople(this.state.page - 1)}><img width="30px" src={prev_page} /></button>
              <div>
                <p className="event_desc">Page {this.state.page} of {this.state.num_pages}</p>
              </div>
              <button className="page-button next_page" id="next_page" onClick={() => this.requestNewPeople(this.state.page + 1)}><img width="30px" src={next_page} /></button>
              <button className="page-button" id="last_page" onClick={() => this.requestNewPeople(this.state.num_pages)}><img width="30px" src={last_page} /></button>
            </div>
          </React.Fragment>
        }


      </div>
    )
  }
}

export default PersonsGrid;
