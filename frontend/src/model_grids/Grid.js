import React, {Component} from 'react';
import { Row } from 'reactstrap';
import '../styles/grid.css'
import { Link, Redirect } from "react-router-dom";



class Grid extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    return(
      <div className="container-fluid">
        <Row>
          {this.props.children}
        </Row>
      </div>
    )
  }
}


export default Grid;
