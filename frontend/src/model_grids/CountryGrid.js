import React, {Component} from 'react';
import Grid from './Grid.js';
import '../styles/pagination.css'
import CountryCard from '../model_grid_cards/CountryCard.js'
import first_page from "../img/first_page.svg"
import last_page from "../img/last_page.svg"
import next_page from '../img/next_page.svg'
import prev_page from '../img/prev_page.svg'
import update from 'immutability-helper'
import FilterElements from './FilterElements.js'
import SearchModel from '../search/SearchModel.js'
import SearchResults from '../search/SearchResults.js'

const axios = require('axios')
const INSTANCES_PER_PAGE = 12

class CountryGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {page : 1,
      num_pages: 5,
      countries: [],
      foreign_invst: ['None', 'negative', 'between 0 and 1', 'greater than 1'],
      gdp: ['None', 'low', 'lower-middle', 'upper-middle', 'high'],
      sortOptions: ['None', 'Name', 'Population', 'GDP per Capita', 'Gender Gap Index', 'Foreign Investment'],
      chosenForeignInvst: null,
      chosenGDP: null,
      sortBy: null,
      desc: 'false',
      searchTerm: '',
      searchClicked: false,
      showSearchResults: false,
      filterError: false,
    };
    this.requestNewCountries = this.requestNewCountries.bind(this)
    this.filterByInvst = this.filterByInvst.bind(this)
    this.filterByGDP = this.filterByGDP.bind(this)
    this.sortCountries = this.sortCountries.bind(this)
    this.changeOrdering = this.changeOrdering.bind(this)
    this.searchTermChanged = this.searchTermChanged.bind(this)
    this.searchClicked = this.searchClicked.bind(this)
    this.setSearchClicked = this.setSearchClicked.bind(this)
  }

  componentDidMount() {
    this.requestNewCountries(1)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.location.state.showSearchResults === false)
      this.setState({showSearchResults:false})
  }


  searchTermChanged(event) {
    this.setState({searchTerm: event.target.value})
  }

  searchClicked(event){
    event.preventDefault()
    this.setState({showSearchResults: true})
    this.setState({searchClicked: true})
  }

  setSearchClicked(clicked){
    this.setState({searchClicked: clicked})
  }

  requestNewCountries(newPage) {
    // get information about the charities
    if (newPage > 0 && newPage <= this.state.num_pages) {
      this.setState({page: newPage})
      let url = `https://api.thecharitywatch.org/country?${this.state.chosenForeignInvst ? 'foreign_investment_pct=' + this.state.chosenForeignInvst + '&' : ''}${this.state.chosenGDP ? 'gdp_per_capita=' + this.state.chosenGDP + '&' : ''}${this.state.sortBy ? 'orderby=' + this.state.sortBy + '&desc=' + this.state.desc + '&': ''}start=${newPage}&limit=${INSTANCES_PER_PAGE}`
      axios.get(url)
        .then(response =>{
          this.setState({countries: response.data['countries']})
          this.setState({num_pages: response.data['num_pages']})
          this.setState({filterError:false})
          window.scrollTo(0,0)
        })
        .catch(error=>{
          console.log(error)
          this.setState({filterError:true})
        })
      }
  }

  filterByInvst(event) {
    let invstValue = event.target.value
    let label = null
    switch (invstValue) {
      case 'negative':
        label = 'NEGATIVE'
        break;
      case 'between 0 and 1':
        label = 'POSITIVE_FRACTION'
        break;
      case 'greater than 1':
        label = 'POSITIVE_LARGE'
        break;
      default:
        break;
    }
    this.setState({chosenForeignInvst: label}, () => {
      this.requestNewCountries(1)
    })
  }

  filterByGDP(event) {
    let gdpValue = event.target.value
    let label = null
    if (gdpValue !== 'None') {
      label = gdpValue.toUpperCase()
    }
    this.setState({chosenGDP: label}, () => {
      this.requestNewCountries(1)
    })
  }

  sortCountries(event) {
    let sortByVal = event.target.value
    let sortKey = null
    switch (sortByVal) {
      case 'Name':
        sortKey = 'name'
        break;
      case 'Population':
        sortKey = 'population'
        break;
      case 'GDP per Capita':
        sortKey = 'gdp_per_capita'
        break;
      case 'Gender Gap Index':
        sortKey = 'gender_gap_index'
        break;
      case 'Foreign Investment':
        sortKey = 'foreign_investment_pct'
        break;
      default:
        sortKey = null
    }
    this.setState({sortBy: sortKey}, () => {
      this.requestNewCountries(1)
    })
  }

  changeOrdering(orderingVal) {
    this.setState({desc: orderingVal}, () => {
      this.requestNewCountries(1)
    })
  }

  render() {
    let country_cards = []
    for (let i = 0; i < this.state.countries.length; i++) {
      country_cards.push(
        <CountryCard key={this.state.countries[i].abbr} {...this.state.countries[i]['country']} />
      );
    }
    let filterOptions = {'Foreign Invsmt': {'values': this.state.foreign_invst, 'onChange':  this.filterByInvst}, 'GDP per Capita': {'values': this.state.gdp, 'onChange': this.filterByGDP}}
    return (
      <div>
        <form className="form-inline outer-form" onSubmit={this.searchClicked}>
          <FilterElements filters={filterOptions} sortOptions={this.state.sortOptions} sortBy={this.sortCountries} orderBy={this.changeOrdering}/>
          <SearchModel searchType="countries" searchTerm={this.state.searchTerm} searchTermChanged={this.searchTermChanged} searchClicked={this.searchClicked}/>
        </form>
        {
          this.state.filterError ? <h1>No Results Found</h1>
          :
          this.state.showSearchResults ? <SearchResults searchTerm={this.state.searchTerm} searchClicked={this.state.searchClicked} searched={this.setSearchClicked} model="country"/>
          :
          <React.Fragment>
            <Grid>
              {country_cards}
            </Grid>,
            <div className="pagination">
              <button className="page-button" id="first_page" type="button" onClick={() => this.requestNewCountries(1)}><img width="30px" src={first_page} /></button>
              <button className="page-button" id="prev_page" onClick={() => this.requestNewCountries(this.state.page - 1)}><img width="30px" src={prev_page} /></button>
              <div>
                <p className="event_desc">Page {this.state.page} of {this.state.num_pages}</p>
              </div>
              <button className="page-button next_page" id="next_page" onClick={() => this.requestNewCountries(this.state.page + 1)}><img width="30px" src={next_page} /></button>
              <button className="page-button" id="last_page" onClick={() => this.requestNewCountries(this.state.num_pages)}><img width="30px" src={last_page} /></button>
            </div>
          </React.Fragment>
        }


      </div>
    )
  }
}

export default CountryGrid;
