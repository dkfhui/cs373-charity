import React, {Component} from 'react';
import Grid from './Grid.js';
import '../styles/pagination.css'
import CharityCard from '../model_grid_cards/CharityCard.js'
import first_page from "../img/first_page.svg"
import last_page from "../img/last_page.svg"
import next_page from '../img/next_page.svg'
import prev_page from '../img/prev_page.svg'
import update from 'immutability-helper'
import FilterElements from './FilterElements.js'
import SearchModel from '../search/SearchModel.js'
import SearchResults from '../search/SearchResults.js'

const axios = require('axios')
const INSTANCES_PER_PAGE = 12

class CharityGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {page : 1,
      num_pages: 5,
      charities: [],
      states: null,
      classifications: null,
      sortOptions: ['None', 'name', 'assets', 'income'],
      chosenState: null,
      chosenClass: null,
      sortBy: null,
      desc: 'false',
      searchTerm: '',
      searchClicked: false,
      showSearchResults: false,
      filterError: false,
    };
    this.requestFilteringInformation = this.requestFilteringInformation.bind(this)
    this.requestNewCharities = this.requestNewCharities.bind(this)
    this.filterByState = this.filterByState.bind(this)
    this.filterByClass = this.filterByClass.bind(this)
    this.sortCharities = this.sortCharities.bind(this)
    this.changeOrdering = this.changeOrdering.bind(this)
    this.searchTermChanged = this.searchTermChanged.bind(this)
    this.searchClicked = this.searchClicked.bind(this)
    this.setSearchClicked = this.setSearchClicked.bind(this)
  }

  componentDidMount() {
    this.requestFilteringInformation()
    this.requestNewCharities(1)
  }

  // Don't know how to us componentDidUpdate yet
  // componentDidUpdate(prevProps, prevState){
  //   console.log("we updated!")
  //   console.log(prevState)
  //   console.log(this.state)
    // if(this.state.showSearchResults === prevState.showSearchResults === true && this.props.location.state.showSearchResults !== true)
    //   this.setState({showSearchResults:false})
  // }

  // Want to get rid of this
  componentWillReceiveProps(nextProps){
    if(nextProps.location.state.showSearchResults === false)
      this.setState({showSearchResults:false})
  }

  searchTermChanged(event) {
    this.setState({searchTerm: event.target.value})
  }

  searchClicked(event){
    event.preventDefault()
    this.setState({showSearchResults: true})
    this.setState({searchClicked: true})
  }

  setSearchClicked(clicked){
    this.setState({searchClicked: clicked})
  }

  requestFilteringInformation() {
    // get list of states
    let url = `https://api.thecharitywatch.org/charity-states`
    axios.get(url)
      .then(response => {
        this.setState({states: response.data['states']})
        this.state.states.unshift("None")
      })
      .catch(error => {console.log(error)})
    // get list of states
    url = `https://api.thecharitywatch.org/charity-classifications`
    axios.get(url)
      .then(response => {
        this.setState({classifications: response.data['irs_class_names']})
        this.state.classifications.unshift("None")
      })
      .catch(error => {console.log(error)})
  }

  requestNewCharities(newPage) {
    // get information about the charities
    if (newPage > 0 && newPage <= this.state.num_pages) {
      this.setState({page: newPage})
      let url = `https://api.thecharitywatch.org/charity?${this.state.chosenState ? 'state=' + this.state.chosenState + '&': ''}${this.state.chosenClass ? 'irs_classification=' + this.state.chosenClass + '&': ''}${this.state.sortBy ? 'orderby=' + this.state.sortBy + '&desc=' + this.state.desc + '&': ''}start=${newPage}&limit=${INSTANCES_PER_PAGE}`
      axios.get(url)
        .then(response =>{
          this.setState({charities: response.data['charities']})
          this.setState({num_pages: response.data['num_pages']})
          this.setState({filterError:false})
          window.scrollTo(0,0)
        })
        .catch(error=>{
          console.log(error)
          this.setState({filterError:true})
        })
      }
  }

  filterByState(event) {
    let stateAbbr = event.target.value
    stateAbbr = stateAbbr === 'None' ? null : stateAbbr
    this.setState({chosenState: stateAbbr}, () => {
      this.requestNewCharities(1)
    })
  }

  filterByClass(event) {
    let classAbbr = event.target.value
    let irs_class = null;
    switch (classAbbr) {
      case 'Charitable Organization':
        irs_class = 'Charitable'
        break;
      case 'Religious Organization':
        irs_class = 'Religious'
        break;
      case 'Educational Organization':
        irs_class = 'Education'
        break;
      case 'Scientific Organization':
        irs_class = 'Scientific'
        break;
      case 'Organization for Public Safety Testing':
        irs_class = 'Public Safety'
        break;
      default:
        break;
    }
    this.setState({chosenClass: irs_class}, () => {
      this.requestNewCharities(1)
    })
  }

  sortCharities(event) {
    let sortByVal = event.target.value
    let sortKey = null
    switch (sortByVal) {
      case 'name':
        sortKey = 'name'
        break;
      case 'income':
        sortKey = 'income_total'
        break;
      case 'assets':
        sortKey = 'asset_total'
        break;
      case 'state':
        sortKey = 'state'
        break;
      default:
        sortKey = null
    }
    this.setState({sortBy: sortKey}, () => {
      this.requestNewCharities(1)
    })
  }

  changeOrdering(orderingVal) {
    this.setState({desc: orderingVal}, () => {
      this.requestNewCharities(1)
    })
  }

  render() {
    let charity_cards = []
    for (let i = 0; i < this.state.charities.length; i++) {
      charity_cards.push(
        <CharityCard key={this.state.charities[i]['ein']} {...this.state.charities[i]['charity']} />
      );
    }
    let filters_dict = {"State": {"values": this.state.states, "onChange": this.filterByState}, "Class": {"values": this.state.classifications, "onChange": this.filterByClass}}
    return (
      <div>
        <form className="form-inline outer-form" onSubmit={this.searchClicked}>
          <FilterElements filters={filters_dict} sortOptions={this.state.sortOptions} sortBy={this.sortCharities} orderBy={this.changeOrdering}/>
          <SearchModel searchType="charities" searchTerm={this.state.searchTerm} searchTermChanged={this.searchTermChanged} searchClicked={this.searchClicked}/> 
        </form>
        {
          this.state.filterError ? <h1>No Results Found</h1>
          :
          this.state.showSearchResults ? <SearchResults searchTerm={this.state.searchTerm} searchClicked={this.state.searchClicked} searched={this.setSearchClicked} model="charity"/>
          :
          <React.Fragment>
            <Grid>
              {charity_cards}
            </Grid>,
            <div className="pagination">
              <button className="page-button" id="first_page" type="button" onClick={() => this.requestNewCharities(1)}><img width="30px" src={first_page} /></button>
              <button className="page-button" id="prev_page" onClick={() => this.requestNewCharities(this.state.page - 1)}><img width="30px" src={prev_page} /></button>
              <div>
                <p className="event_desc">Page {this.state.page} of {this.state.num_pages}</p>
              </div>
              <button className="page-button next_page" id="next_page" onClick={() => this.requestNewCharities(this.state.page + 1)}><img width="30px" src={next_page} /></button>
              <button className="page-button" id="last_page" onClick={() => this.requestNewCharities(this.state.num_pages)}><img width="30px" src={last_page} /></button>
            </div>
          </React.Fragment>
        }
        

      </div>
    )
  }
}

export default CharityGrid;
