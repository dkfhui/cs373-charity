import React, {Component} from 'react'
import ReactTable from 'react-table'
import { Card, Button, CardTitle, CardText,
    CardSubtitle, CardBody, CardGroup} from 'reactstrap';
import update from 'immutability-helper'
import "./styles/react-table.css"
import './styles/about.css'
import './styles/global.css'
import SearchableText from './search/SearchableText';
import arjun from "./img/arjun.jpg"
import darrin from "./img/darrin.jpg"
import jorge from './img/jorge.jpg'
import rodrigo from './img/rodrigo.png'
import ben from './img/ben.jpg'

const axios = require('axios')

const url = 'https://gitlab.com/api/v4/projects/11050555/'
const commitsUrl = url + 'repository/commits?page=1&per_page=100'
const issuesUrl = url + 'issues?page=1&per_page=100'

const darrin_units = 20
const rodrigo_units = 31
const arjun_units = 33
const jorge_units = 12
const ben_units = 0
const total_units = darrin_units + rodrigo_units + arjun_units + jorge_units + ben_units

class About extends Component {
	constructor(props) {
		super(props)
			this.state = {
				Darrin: {commits: 0, issuesClosed: 0, issuesOpened: 0},
				Rodrigo: {commits: 0, issuesClosed: 0, issuesOpened: 0},
				Arjun: {commits: 0, issuesClosed: 0, issuesOpened: 0},
				Jorge: {commits: 0, issuesClosed: 0, issuesOpened: 0},
				Ben: {commits: 0, issuesClosed: 0, issuesOpened: 0},
				Total: {commits: 0, issuesClosed: 0, issuesOpened: 0},
			};
	}

	// When component is mounted get all the members and
	// commits from Gitlab API and set the state of About
	componentDidMount() {
		// Loop throught the commits and call addCommits() for
		// each commit
		axios.get(commitsUrl)
			.then(response =>{
				response.data.forEach(commit => {
					this.addCommits(commit.committer_email)
				})
				for(var i=2;i<=response.headers["x-total-pages"];i++){
					let commitsExtraUrl = url + `repository/commits?page=${i}&per_page=100`
					axios.get(commitsExtraUrl)
						.then(response2 =>{
							response2.data.forEach(commit => {
							this.addCommits(commit.committer_email)
							})
					})
					.catch(error=>console.log(error))
				}
				let newState = update(this.state.Total, {commits: {$set: response.headers["x-total"]}})
				this.setState({Total: newState})
			})
			.catch(error=>console.log(error))

		// Loop through the issues and call addIssues() for
		// every completed issue
		axios.get(issuesUrl)
			.then(response =>{
				for(var i=1;i<=response.headers["x-total-pages"];i++){
					let issuesExtraUrl = url + `issues?page=${i}&per_page=100`
					axios.get(issuesExtraUrl)
						.then(response2 =>{
							response2.data.forEach(issue =>{
								this.addIssuesOpened(issue.author.name)
								if(issue.closed_by != null)
									this.addIssuesClosed(issue.closed_by.name)
							})
						})
					.catch(error=>console.log(error))
				}
			})
			.catch(error=>console.log(error))
	}

	// Check the email address of each commit and add it to
	// each person's commit total stored in the state
	// Returns false if email not recognized
	addCommits(email) {
		var count, newObject, newTotal, newState;
		if(email.search("benolea") >= 0) {
			count = this.state.Ben.commits + 1
			newObject = update(this.state.Ben, {commits: {$set: count}})
			this.setState({Ben: newObject})
		}
		switch(email){
			case "dkfhui@gmail.com":
				count = this.state.Darrin.commits + 1
				newObject = update(this.state.Darrin, {commits: {$set: count}})
				this.setState({Darrin: newObject})
				break;
			case "dkfhui@utexas.edu":
				count = this.state.Darrin.commits + 1
				newObject = update(this.state.Darrin, {commits: {$set: count}})
				this.setState({Darrin: newObject})
				break;
			case "arjunkunjilwar@utexas.edu":
				count = this.state.Arjun.commits + 1
				newObject = update(this.state.Arjun, {commits: {$set: count}})
				this.setState({Arjun: newObject})
				break;
			case "arjun_kunjilwar@homedepot.com":
				count = this.state.Arjun.commits + 1
				newObject = update(this.state.Arjun, {commits: {$set: count}})
				this.setState({Arjun: newObject})
				break;
			case "arjunkun@cs.utexas.edu":
				count = this.state.Arjun.commits + 1
				newObject = update(this.state.Arjun, {commits: {$set: count}})
				this.setState({Arjun: newObject})
				break;
			case "benolea@utexas.edu":
				count = this.state.Ben.commits + 1
				newObject = update(this.state.Ben, {commits: {$set: count}})
				this.setState({Ben: newObject})
				break;
			case "rvn2010@hotmail.es":
				count = this.state.Rodrigo.commits + 1
				newObject = update(this.state.Rodrigo, {commits: {$set: count}})
				this.setState({Rodrigo: newObject})
				break;
			case "jopar219@gmail.com":
				count = this.state.Jorge.commits + 1
				newObject = update(this.state.Jorge, {commits: {$set: count}})
				this.setState({Jorge: newObject})
				break;
			default:
				return false
		}
		return true
	}

	// Check the name of the person who closed the
	// issue and increment that person's issues closed total
	// stored in the state. Returns false if name not recognized
	addIssuesClosed(name) {
		var count, newObject, newTotal, newState
		if(name.search("Darrin") >= 0) {
			count = this.state.Darrin.issuesClosed + 1
			newObject = update(this.state.Darrin, {issuesClosed: {$set: count}})
			this.setState({Darrin: newObject})
		}
		else if(name.search("Rodrigo") >= 0) {
			count = this.state.Rodrigo.issuesClosed + 1
			newObject = update(this.state.Rodrigo, {issuesClosed: {$set: count}})
			this.setState({Rodrigo: newObject})
		}
		else if(name.search("Arjun") >= 0) {
			count = this.state.Arjun.issuesClosed + 1
			newObject = update(this.state.Arjun, {issuesClosed: {$set: count}})
			this.setState({Arjun: newObject})
		}
		else if(name.search("Jorge") >= 0) {
			count = this.state.Jorge.issuesClosed + 1
			newObject = update(this.state.Jorge, {issuesClosed: {$set: count}})
			this.setState({Jorge: newObject})
		}
		else if(name.search("Ben") >= 0){
			count = this.state.Ben.issuesClosed + 1
			newObject = update(this.state.Ben, {issuesClosed: {$set: count}})
			this.setState({Ben: newObject})
		} else {
			return false
		}
		return true
	}

	// Check the name of the issue's author and
	// increment that person's issues opened
	// total stored in the state
	// Returns false if name not recognized
	addIssuesOpened(name) {
		var count, newObject, newTotal, newState
		if(name.search("Darrin") >= 0) {
			count = this.state.Darrin.issuesOpened + 1
			newObject = update(this.state.Darrin, {issuesOpened: {$set: count}})
			this.setState({Darrin: newObject})
		}
		else if(name.search("Rodrigo") >= 0) {
			count = this.state.Rodrigo.issuesOpened + 1
			newObject = update(this.state.Rodrigo, {issuesOpened: {$set: count}})
			this.setState({Rodrigo: newObject})
		}
		else if(name.search("Arjun") >= 0) {
			count = this.state.Arjun.issuesOpened + 1
			newObject = update(this.state.Arjun, {issuesOpened: {$set: count}})
			this.setState({Arjun: newObject})
		}
		else if(name.search("Jorge") >= 0) {
			count = this.state.Jorge.issuesOpened + 1
			newObject = update(this.state.Jorge, {issuesOpened: {$set: count}})
			this.setState({Jorge: newObject})
		}
		else if(name.search("Ben") >= 0){
			count = this.state.Ben.issuesOpened + 1
			newObject = update(this.state.Ben, {issuesOpened: {$set: count}})
			this.setState({Ben: newObject})
		} else {
			return false
		}
		return true
	}

	render() {
		const members = this.state.members;
		const commits = this.state.commits;
		const state = this.state;
		const totalIssuesOpened = state.Darrin.issuesOpened + state.Jorge.issuesOpened + state.Rodrigo.issuesOpened + state.Arjun.issuesOpened + state.Ben.issuesOpened
		const totalIssuesClosed = state.Darrin.issuesClosed + state.Jorge.issuesClosed + state.Rodrigo.issuesClosed + state.Arjun.issuesClosed + state.Ben.issuesClosed
		let search_terms = []

		const data = [
			{
				name: "Darrin",
				commits: this.state.Darrin.commits,
				issuesClosed: this.state.Darrin.issuesClosed,
				issuesOpened: this.state.Darrin.issuesOpened,
				units: darrin_units
			},
			{
				name: "Rodrigo",
				commits: this.state.Rodrigo.commits,
				issuesClosed: this.state.Rodrigo.issuesClosed,
				issuesOpened: this.state.Rodrigo.issuesOpened,
				units: rodrigo_units
			},
			{
				name: "Arjun",
				commits: this.state.Arjun.commits,
				issuesClosed: this.state.Arjun.issuesClosed,
				issuesOpened: this.state.Arjun.issuesOpened,
				units: arjun_units
			},
			{
				name: "Jorge",
				commits: this.state.Jorge.commits,
				issuesClosed: this.state.Jorge.issuesClosed,
				issuesOpened: this.state.Jorge.issuesOpened,
				units: jorge_units
			},
			{
				name: "Ben",
				commits: this.state.Ben.commits,
				issuesClosed: this.state.Ben.issuesClosed,
				issuesOpened: this.state.Ben.issuesOpened,
				units: ben_units
			}, {
				name: "Total",
				commits: this.state.Total.commits,
				issuesClosed: totalIssuesClosed,
				issuesOpened: totalIssuesOpened,
				units: total_units
			}
		]

		const columns = [
			{
				Header: "Name",
				accessor: 'name',
				width: 200
			},
			{
				Header: "Commits",
				accessor: "commits",
				width: 200
			},
			{
				Header: "Issues Opened",
				accessor: "issuesOpened",
				width: 200
			},
			{
				Header: "Issues Closed",
				accessor: "issuesClosed",
				width: 200
			},
			{
				Header: "Unit Tests",
				accessor: "units",
				width: 200
			}
		]

		return(
				<section id="About">
					<div className="info-box">
						<h1 className="display-4"><SearchableText search_terms={search_terms} text="Our Mission"/></h1>
						<p>
							<SearchableText
								search_terms={search_terms}
								text="International relief aid is great and all, but it is important to donate to places that need 
								the most help. We created this website to help the general public find countries in need of help based
								on several development and economic indicators. Then, we also provide charities devoted to international
								aid in order to connect users with the relief effort. We allow the general public to judge charity
								credibility by presenting IRS findings as well as information on their leading members. We just hope
								to instill confidence in donators that their money is actually going to the right place."
							/>
						</p>
					</div>
					<div className="info-box">
						<h1 className="display-4"><SearchableText search_terms={search_terms} text="The Findings"/></h1>
						<p>
							<SearchableText
								search_terms={search_terms}
								text="Through analysis of our data, we see that many charities are devoted to helping developing nations
								like India, Mexico, and Ethiopia. And we also see that charities like the Lions Club and Brothers Brothers
								Foundation cover the largest spread of the globe in international assistance. Unfortunately, we also see
								that charity efforts are not necessarily well-guided as countries as many African countries have the lowest
								GDP per Capita but receive a subpar amount of effort based on the data we analyzed."
							/>
						</p>
					</div>
					<div className="row">
						<div className="col-lg">
							<div className="center-block">
								<CardGroup>
										<Card>
											<img width="100%" src={darrin} alt="<Darrin's picture goes here>" />
											<CardBody>
												<CardTitle><h3><SearchableText search_terms={search_terms} text="Darrin Hui"/></h3></CardTitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Fourth year computer science student"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Front End Developer"/></CardSubtitle>
											</CardBody>
										</Card>
										<Card>
											<img width="100%" src={rodrigo} alt="<Rodrigo's picture goes here>" />
											<CardBody>
												<CardTitle><h3><SearchableText search_terms={search_terms} text="Rodrigo Villarreal"/></h3></CardTitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Third year computer science student"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="API Manager"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Full Stack Developer"/></CardSubtitle>
											</CardBody>
										</Card>
										<Card>
											<img width="100%" src={arjun} alt="<Arjun's picture goes here>" />
											<CardBody>
												<CardTitle><h3><SearchableText search_terms={search_terms} text="Arjun Kunjilwar"/></h3></CardTitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Second year computer science student"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Full Stack Developer"/></CardSubtitle>
											</CardBody>
										</Card>
										<Card>
											<img width="100%" src={jorge} alt="<Jorge's picture goes here>" />
											<CardBody>
												<CardTitle><h3><SearchableText search_terms={search_terms} text="Jorge Paredes"/></h3></CardTitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Fourth year computer science student"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Back End Developer"/></CardSubtitle>
												<CardSubtitle><SearchableText search_terms={search_terms} text="Dropped after Phase 2"/></CardSubtitle>
											</CardBody>
										</Card>
									<Card>
										<img width="100%" src={ben} alt="<Ben's picture goes here>" />
										<CardBody>
											<CardTitle><h3><SearchableText search_terms={search_terms} text="Ben Olea"/></h3></CardTitle>
											<CardSubtitle><SearchableText search_terms={search_terms} text="Third year computer science student"/></CardSubtitle>
											<CardSubtitle><SearchableText search_terms={search_terms} text="Full Stack Developer"/></CardSubtitle>
											<CardSubtitle><SearchableText search_terms={search_terms} text="Dropped after Phase 1"/></CardSubtitle>
										</CardBody>
									</Card>
								</CardGroup>
							</div>
						</div>
					</div>

					<div className="row top-buffer">
						<div className="col-lg">
							<div className="table">
								<ReactTable
									data={data}
									columns={columns}
									showPagination={false}
									pageSize={6}
								/>
							</div>
						</div>
					</div>

					<div className="info-box">
						<h1 className="display-4"><SearchableText search_terms={search_terms} text="Tools"/></h1>
							<ul>
								<li><SearchableText search_terms={search_terms} text="React"/></li>
								<li><SearchableText search_terms={search_terms} text="Bootstrap"/></li>
								<li><SearchableText search_terms={search_terms} text="Flask"/></li>
								<li><SearchableText search_terms={search_terms} text="SQLAlchemy"/></li>
								<li><SearchableText search_terms={search_terms} text="Amazon Web Services"/></li>
								<li><SearchableText search_terms={search_terms} text="Slack"/></li>
								<li><SearchableText search_terms={search_terms} text="Gitlab"/></li>
								<li><SearchableText search_terms={search_terms} text="Postman"/></li>
							</ul>
					</div>
					<div className="info-box">
						<h1 className="display-4"><SearchableText search_terms={search_terms} text="Data Sources"/></h1>
							<ul>
								<li><a href="https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397" target="_blank"><SearchableText search_terms={search_terms} text="Charity Navigator API"/></a><SearchableText search_terms={search_terms} text="- A RESTful API with data about Charity organizations"/></li>
								<li><a href="https://newsapi.org/" target="_blank"><SearchableText search_terms={search_terms} text="Fourth year computer science student"/>News API</a><SearchableText search_terms={search_terms} text="- A RESTful API with recent news articles related to charities and individuals"/></li>
								<li><a href="https://www.mediawiki.org/wiki/REST_API" target="_blank"><SearchableText search_terms={search_terms} text="Wikipedia API"/></a><SearchableText search_terms={search_terms} text="- A RESTful API which gives access to Wikipedia articles about Charities and Countries"/></li>
								<li><a href="https://tcdata360.worldbank.org/" target="_blank"><SearchableText search_terms={search_terms} text="TCData360"/></a><SearchableText search_terms={search_terms} text="- A RESTful API with economic and social indicators for countries"/></li>
								<li><a href="https://s3.amazonaws.com/irs-form-990/index_2018.json" target="_blank"><SearchableText search_terms={search_terms} text="IRS-Form-990"/></a><SearchableText search_terms={search_terms} text="- A JSON document posted by the IRS used to supplement our API data"/></li>
							</ul>
					</div>
					<div className="info-box">
						<h1 className="display-4"><SearchableText search_terms={search_terms} text="For Developers"/></h1>
							<ul>
								<li><a href="https://gitlab.com/dkfhui/cs373-charity" target="_blank"><SearchableText search_terms={search_terms} text="Gitlab Repository"/></a></li>
								<li><a href="https://documenter.getpostman.com/view/6816316/S11NMwxx" target="_blank"><SearchableText search_terms={search_terms} text="Our Postman API Documentation"/></a></li>
							</ul>
					</div>
				</section>
		);
	}
}

export default About;
