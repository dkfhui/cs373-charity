import React, {Component} from 'react';
import '../styles/card.css';
import { Card, Button, CardTitle, CardText,
    CardSubtitle, CardBody} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";

const NewsCard = ({article}) => {
	let jsonDate = article.publishedAt.substring(0,10)

	return (
  	<Card className="grid-row news-card">
			<img width="100%" height="50%" src={article.urlToImage} alt="<News Image goes here>" />
			<CardBody>
				<CardTitle>{article.title}</CardTitle>
				<CardSubtitle>Published: {jsonDate}</CardSubtitle>
				<form action={article.url} method="get" target="_blank">
					<Button className="info-button news-info-button" as="submit">Read the Full Article</Button>
				</form>
			</CardBody>
		</Card>
	)
}

export default NewsCard
