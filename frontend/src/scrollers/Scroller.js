import React, {Component} from 'react';
import '../styles/global.css'
import '../styles/charity.css'
import '../styles/scroller.css'
import Frame from './Frame.js'
import NewsCard from './NewsCard.js'
import portrait1 from '../img/male_portrait.png'
import portrait2 from '../img/male2_portrait.png'
import portrait3 from '../img/male3_potrait.png'
import portrait4 from '../img/female_portrait.png'
import portrait5 from '../img/female2_portrait.png'
import portrait6 from '../img/female3_portrait.png'

const axios = require('axios')


class Scroller extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        data: props.data,
        model: props.model,
        title: props.title
    }
    this.people_images = [portrait1, portrait2, portrait3, portrait4, portrait5, portrait6]

  }

  render() {
    let frames = []
    for (let i = 0; i < this.state.data.length; i++) {
        let frame = null
        if (this.state.model === 'CHARITY') {
            frame = <Frame key={this.state.data[i].ein} data={this.state.data[i]} image={this.state.data[i].img_url} model={this.state.model}/>
        } else if (this.state.model === 'PERSON') {
            let image = this.people_images[Math.floor(Math.random() * 6)]
            frame = <Frame key={this.state.data[i].id} data={this.state.data[i]} image={image} model={this.state.model}/>
        } else if (this.state.model === 'NEWS') {
            frame = <NewsCard key={this.state.data[i].title} article={this.state.data[i]}></NewsCard>
        }
        frames.push(frame)
    }
    return (
        <div className={`data-container ${this.state.model.toLowerCase()}-container`}>
            <h1 className="display-4">{this.state.title}</h1>
            <div className="scrolling-wrapper">
                {frames}
            </div>
      </div>
    )
  }
}

export default Scroller;

