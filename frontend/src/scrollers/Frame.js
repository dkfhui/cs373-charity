import React, {Component} from 'react';
import '../styles/global.css'
import '../styles/charity.css'
import '../styles/person_frame.css'
import history from '../history';

class Frame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: this.props.model,
      data: this.props.data,
      image: this.props.image
    };
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    let path = ''
    if (this.state.model === 'CHARITY') {
      path = `/charity_instance/${this.state.data.ein}`
    } else if (this.state.model === 'PERSON') {
      path = `/person_instance/${this.state.data.id}`
    }
    console.log("PATH IS " + path)
    history.push(path)
  }

  render() {
    let imgStyle = {
      backgroundImage: `url(${this.state.image})`
    }
    let name = this.state.data.name.replace(" ", "\n")
    return (
        <div className="frame">
            <div className="portrait" style={imgStyle} onClick={this.handleClick}/>
            <div onClick={this.handleClick}>
              <p className="lead text">{this.state.data.name}</p>
            </div>
      </div>
    )
  }
}

export default Frame;
