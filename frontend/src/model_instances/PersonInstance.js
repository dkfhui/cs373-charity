import React, {Component} from 'react'
import '../styles/global.css'
import '../styles/charity.css'
import { Link } from "react-router-dom";
import axios from 'axios';
import Scroller from '../scrollers/Scroller.js';
import BasicMap from '../Map.js'


class PersonInstance extends Component {
  /*
    Current News: Headlines, Source, Pictures, People or Organizations involved
    Human actors: Name, about, pic, Related organizations, Related news, Related people, Twitter
  */

	constructor(props) {
		super(props)
		this.state = {
			id: props.match.params.id,
			person: {},
			news: [],
		};
	}

	componentDidMount() {
		let url = `https://api.thecharitywatch.org/person/${this.state.id}`
		axios.get(url).then(response => {
			this.setState(prevState => ({
				person: response.data['person']
			}))
		})
	}

	render() {
		const scroller_model = "Person"
    let person = this.state.person
    //get related charities
		let charities = []
		let charityMuseum
    if (person.charities) {
      for (let index = 0; index < person.charities.length; index++) {
        charities.push(
					person.charities[index]
        )
			}
			charityMuseum = <Scroller data={charities} model="CHARITY" title="Charities Associated With This Person"/>
		}
		else{
			charities = "No charities found"
		}
		let countries = []
		let map = null
		if (person.countries) {
			for (let index = 0; index < person.countries.length; index++) {
				countries.push(
					countries[person.countries[index].abbr] = person.countries[index].name
				)
			}
			map = <BasicMap countries={countries} title="Countries This Person Helps" />
		}
		else{
			countries = "No countries found"
		}

		let imgStyle = {
      backgroundImage: `url(${person.image_url})`
    }

		return (
			
			<section className="instance-section" id={person.name}>
				<div className="row">
					<div className="col">
						<h2 align="center" className="display-1"> {person.name} </h2>
					</div>
				</div>
				<div className="wrapper">
					<div className="image-div" style={imgStyle}></div>
					<div className="irs-wrapper">
						<div className="irs-info">
							<h3 align="center" className="display-4 irsTitle"> About </h3>
							<dl id="list" className="person-dl">
									<dt><p className="lead lead-bold">Main Charity</p></dt>
									<dd><p className="lead">{ person.main_charity ? person.main_charity.name : "" }</p></dd>
									<dt><p className="lead lead-bold">Main Role</p></dt>
									<dd><p className="lead">{ person.main_role }</p></dd>
									<dt><p className="lead lead-bold">Total Compensation</p></dt>
									<dd><p className="lead">{ person.total_compensation }</p></dd>
									<dt><p className="lead lead-bold">Average Total Hours per Week</p></dt>
									<dd><p className="lead">{ person.total_avg_hours }</p></dd>
							</dl>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-lg">
						{map}
					</div>
				</div>
				{charityMuseum}
			</section>
		);
	}
}

export default PersonInstance;
