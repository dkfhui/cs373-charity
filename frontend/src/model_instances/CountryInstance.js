import React from 'react';
import '../styles/global.css'
import '../styles/charity.css'
import axios from 'axios';
import Scroller from '../scrollers/Scroller.js'

const numberWithCommas = (x) => {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


class CountryInstance extends React.Component {
  /*
	Name, wiki blurb, IRS (asset amount, income amount, paid taxes),
	Logo, Related people, Related news,Cause area, Location,
*/

	constructor(props) {
		super(props)
		this.state = {
			abbr: this.props.match.params.abbr,
			country: {},
			news: []
		}
	}

	componentDidMount() {
		let url = `https://api.thecharitywatch.org/country/${this.state.abbr}`
		axios.get(url).then(response => {
			this.setState(prevState => ({
				country: response.data['country'],
				news: response.data.news.articles
			}))
		})
	}


	render() {
		const scroller_model = "Country"
		const country = this.state.country
        //get related people
		let people = []
		let museum = null
    if (country.people && country.people.length > 0) {
			for (let index = 0; index < country.people.length; index++) {
				people.push(country.people[index])
			}
			museum = <Scroller data={people} model="PERSON" title="People Associated With This Country"/>
		}
		//get related orgs
		let orgs = []
		let charityMuseum = null
		if (country.charities) {
			for (let index = 0; index < country.charities.length; index++) {
				orgs.push(
					country.charities[index]
				)
			}
			charityMuseum = <Scroller data={orgs} model="CHARITY" title="Charities Associated With This Country"/>
		}

		//get related news
		let news = []
		let newsMuseum = null
		if (this.state.news && this.state.news.length > 0) {
			for (let index = 0; index < this.state.news.length; index++) {
				news.push(this.state.news[index])
			}
			newsMuseum = <Scroller data={news} model="NEWS" title="Recent News About This Country"/>
		}

		let population = country.population != null ? numberWithCommas(country.population) : "Unknown"
    let gdp = country.gdp_per_capita != null ? numberWithCommas(country.gdp_per_capita.toFixed(2)) : "Unknown"
    let gender_index = country.gender_gap_index != null ? country.gender_gap_index : "Unknown"
    let foreign_investment = country.foreign_investment_pct != null ? country.foreign_investment_pct.toFixed(2) : "Unknown"
		let life_expectancy = country.life_exp_rating != null ? country.life_exp_rating.toFixed(2) : "Unranked"
		let inflation = country.inflation_pct != null ? country.inflation_pct.toFixed(2) : "Unknown"
		let unemployment = country.unemployment_pct != null ? country.unemployment_pct.toFixed(2) : "Unknown"
		let vc = country.vc_availability != null ? country.vc_availability.toFixed(2) : "Unrated"
		let research = country.research_and_development != null ? country.research_and_development.toFixed(2) : "Unknown"
		let imgStyle = {
      backgroundImage: `url(${country.image_url})`
    }

		return (

			<section className="instance-section" id={country.id}>
				<div className="row">
					<div className="col">
						<h2 align="center" className="display-1"> {`${country.name} (${country.abbr})`}</h2>
					</div>
				</div>
				<div className="wrapper">
					<div className="image-div-half" style={imgStyle}></div>
					<div><p className="lead">{country.blurb}</p></div>
				</div>
				<div className="wrapper">
				<div className="irs-wrapper">
						<div className="irs-info">
							<h3 align="center" className="display-4 irsTitle"> Life Quality Indicators</h3>
							<dl id="list" className="person-dl">
								<dt className="economic-dt"><p className="lead lead-bold">Population</p></dt>
								<dd className="economic-dd"><p className="lead">{ population }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">GDP per Capita (USD)</p></dt>
								<dd className="economic-dd"><p className="lead">{ gdp }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Gender Gap Index</p></dt>
								<dd className="economic-dd"><p className="lead">{ gender_index }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Life Expectancy Ranking</p></dt>
								<dd className="economic-dd"><p className="lead">{ life_expectancy }</p></dd>
							</dl>
						</div>
					</div>
					<div className="irs-wrapper">
						<div className="irs-info">
							<h3 align="center" className="display-4 irsTitle"> Economic Indicators </h3>
							<dl id="list" className="person-dl">
								<dt className="economic-dt"><p className="lead lead-bold">Inflation (annual %)</p></dt>
								<dd className="economic-dd"><p className="lead">{ inflation }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Foreign Investment (% of GDP)</p></dt>
								<dd className="economic-dd"><p className="lead">{ foreign_investment }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Unemployment (% of total labor force)</p></dt>
								<dd className="economic-dd"><p className="lead">{ unemployment }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Venture Capital Availability (1-7 Higher is better)</p></dt>
								<dd className="economic-dd"><p className="lead">{ vc }</p></dd>
								<dt className="economic-dt"><p className="lead lead-bold">Research and Development Expenditure (% of GDP)</p></dt>
								<dd className="economic-dd"><p className="lead">{ research }</p></dd>
							</dl>
						</div>
					</div>
				</div>
				{museum}
				{charityMuseum}
				{newsMuseum}
			</section>
		)
  }
}

export default CountryInstance;
