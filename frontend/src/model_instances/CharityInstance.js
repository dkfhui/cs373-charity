import React, {Component} from 'react'
import '../styles/global.css'
import '../styles/charity.css'
import axios from 'axios';
import BasicMap from '../Map.js'
import Scroller from '../scrollers/Scroller.js'

class CharityInstance extends Component {
/*
	Name, wiki blurb, IRS (asset amount, income amount, paid taxes),
	Logo, Related people, Related news,Cause area, Location,
*/

	constructor(props) {
		super(props)
		this.state = {
			ein: this.props.match.params.ein,
			charity: {},
			news: [],
		}
	}

	componentDidMount() {
		let url = `https://api.thecharitywatch.org/charity/${this.state.ein}`
		axios.get(url).then(response => {
			this.setState(prevState => ({
				charity: response.data['charity'],
				news: response.data.news.articles
			}))
		})
	}

	render() {
		const scroller_model = "CHARITY"
		let charity = this.state.charity
		let countries = {}
		let map = null
		if (charity.countries) {
			for (let index = 0; index < charity.countries.length; index++) {
				countries[charity.countries[index].abbr] = charity.countries[index].name
			}
			map = <BasicMap countries={countries} title="Countries This Charity Helps" />
		}
		let people = []
		let museum = null
		if (charity.people && charity.people.length > 0) {
			for (let index = 0; index < charity.people.length; index++) {
				people.push(charity.people[index])
			}
			museum = <Scroller data={people} model="PERSON" title="People Associated With This Charity" />
    }
    //get related news
		let news = []
		let newsMuseum = null
		if (this.state.news && this.state.news.length > 0) {
			for (let index = 0; index < this.state.news.length; index++) {
				news.push(this.state.news[index])
			}
			newsMuseum = <Scroller data={news} model="NEWS" title="Recent News About This Charity"/>
		}

    let imgStyle = {
      backgroundImage: `url(${charity.image_url})`
		}


		const child = { width: `300em`, height: `100%`, backgroundColor: 'aqua'}
		const parent  = { width: `60em`, height: `100%`, backgroundColor: 'red'}


		return (
			<section id={charity.id}>
				<div className="row">
					<div className="col">
						<h2 align="center" className="display-1"> {charity.name} </h2>
						{charity.tag_line && <h1 className="display-4 tagline" align="center"> {`"${charity.tag_line}"`} </h1>}
					</div>
				</div>
				<div className="wrapper">
					<div className="image-div" style={imgStyle}></div>
					{charity.mission_statement && <div className="info">
						<p className="lead"> {charity.mission_statement} </p>
					</div>}
					<div className="irs-wrapper">
						<div className="irs-info">
							<h3 align="center" className="display-4 irsTitle"> IRS Information </h3>
							<dl id="list">
									<dt><p className="lead lead-bold">Classification</p></dt>
									<dd><p className="lead">{ charity.irs_classification }</p></dd>
									<dt><p className="lead lead-bold">Address</p></dt>
									<dd><p className="lead">{ charity.address }</p></dd>
									<dt><p className="lead lead-bold">State</p></dt>
									<dd><p className="lead">{ charity.state }</p></dd>
									<dt><p className="lead lead-bold">Assets</p></dt>
									<dd><p className="lead">{ this.intToMoneyString(charity.asset_total)}</p></dd>
									<dt><p className="lead lead-bold">Income</p></dt>
									<dd><p className="lead">{ this.intToMoneyString(charity.income_total) }</p> </dd>
							</dl>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-lg">
						{map}
					</div>
				</div>
				{museum}
				{newsMuseum}
			</section>
		);
	}

  //helper method for converting numbers to money strings
  intToMoneyString(num) {
      if (num == null) {
          return "not disclosed";
      }
      let str = num.toString();
    	let newStr = "";
    	let count = 0;
    	for (let i = str.length - 1; i >= 0; i--) {
    		count += 1;
    		newStr = str.charAt(i) + newStr;
    		if (count % 3 == 0) {
          if (count != str.length) { //dont add comma as first character
            newStr = "," + newStr;
          }
    		}
  	   }
  	   return "$"+newStr;
  }

}

export default CharityInstance;
