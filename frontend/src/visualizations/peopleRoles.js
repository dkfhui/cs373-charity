import React from 'react';
import * as d3 from 'd3';
import history from '../history';
import axios from 'axios';
import BubbleChart from '@weknow/react-bubble-chart-d3';

class PeopleRoles extends React.Component{

	_isMounted = false;

	constructor(props){
		super(props)
		this.state = {
			hours_dict : {},
			comp_dict : {},
			pop_dict : {},
			hours_list : [],
			comp_list : [],
			pop_list : []
		}
	}

	componentDidMount(){

		this._isMounted = true;

		//populate dicts with accumulated data
		// let hours_dict = {};
		// let comp_dict = {};
		// let pop_dict = {};
		

		let url = 'https://api.thecharitywatch.org/person?start=1&limit=5000';
		axios.get(url)
			.then(response => {
				response.data.people.forEach(p => {
					let person = p.person;
					
					let hours_dict = this.state.hours_dict;
					let comp_dict = this.state.comp_dict;
					let pop_dict = this.state.pop_dict;

					//add to hours_dict
					if (person.main_role in hours_dict) {
						hours_dict[person.main_role] = hours_dict[person.main_role] +
														person.total_avg_hours;
					} else {
						hours_dict[person.main_role] = person.total_avg_hours;
					}

					//add to comp_dict
					if (person.main_role in comp_dict) {
						comp_dict[person.main_role] = comp_dict[person.main_role] +
														person.total_compensation;
					} else {
						comp_dict[person.main_role] = person.total_compensation;
					}

					//add to pop_dict
					if (person.main_role in pop_dict) {
						pop_dict[person.main_role] = pop_dict[person.main_role] + 1;
					} else {
						pop_dict[person.main_role] = 1;
					}

					if (this._isMounted) {
						this.state.hours_dict = hours_dict;
						this.state.comp_dict = comp_dict;
						this.state.pop_dict = pop_dict;
					}
				})

				//prepare lists for charts
				let hours_list = [];
				for(let key in this.state.hours_dict) {
					let popularity = this.state.pop_dict[key];
					let av_hours = this.state.hours_dict[key] / popularity;
					let temp_c = '';
					if (popularity == 1) {
						temp_c = '#D3D3D3';
					}
					let temp = {
									label: key,
									value: av_hours,
									color: temp_c
							   };
				  	hours_list.push(temp);
				}

				let comp_list = [];
				for(let key in this.state.comp_dict) {
					let popularity = this.state.pop_dict[key];
					let comp = this.state.comp_dict[key]  / popularity;
					let temp_c = '';
					if (popularity == 1) {
						temp_c = '#D3D3D3';
					}
					let temp = {
									label: key,
									value: comp,
									color: temp_c
							   };
				  	comp_list.push(temp);
				}

				let comp_b_list = [];
				for(let key in this.state.comp_dict) {
					let popularity = this.state.pop_dict[key];
					let comp = this.state.comp_dict[key]  / popularity;
					let temp_c = '';
					if (comp < 13000) {
						temp_c = '#FFFFFF';
					} else if (comp < 50000) {
		                temp_c = '#cce0ff'
		            } else if (comp < 85000) {
		                temp_c = '#99c2ff'
		            } else if (comp < 150000) {
		                temp_c = '#4d94ff'
		            } else if (comp < 200000) {
		                temp_c = '#005ce6'
		            } else {
		                temp_c = '#003380'
		            }
					let temp = {
									label: key,
									value: comp,
									color: temp_c
							   };
				  	comp_b_list.push(temp);
				}

				let pop_list = [];
				for(let key in this.state.pop_dict) {
					let popularity = this.state.pop_dict[key];
					let temp_c = '';
					if (popularity == 1) {
						temp_c = '#D3D3D3';
					}
					let temp = {
									label: key,
									value: popularity,
									color: temp_c
							   };
				  	pop_list.push(temp);
				}

				if (this._isMounted) {
					this.state.hours_list = hours_list;
					this.state.comp_list = comp_list;
					this.state.comp_b_list = comp_b_list;
					this.state.pop_list = pop_list;
					console.log('success!!!')
				}

			})
			.catch(error=>console.log(error))
	}

	componentWillUnmount() {
		this._isMounted = false;
	}
	bubbleClickHours = (label) => {
		console.log("Hours " + label + " " + this.state.hours_dict[label]);
	}
	bubbleClickComp = (label) => {
		console.log("Comp " + label + " " + this.state.comp_dict[label]);
	}
	bubbleClickPop = (label) => {
		console.log("Pop " + label + " " + this.state.pop_dict[label]);
	}
	legendClick = (label) => {
	}

	render() {

	    return (

	    <div>

	      <div className='container'>
	      	<h3>People Roles by Popularity (number of people with this role)</h3>
	      	<p>Light gray nodes are unique roles (only 1 person has that role).</p>
	      	<BubbleChart
			  graph= {{
			    zoom: 0.7,
			    offsetX: 0,
			    offsetY: 0
			  }}
			  width={800}
  			  height={800}
			  showLegend={false} // optional value, pass false to disable the legend.
			  valueFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  labelFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  bubbleClickFun={this.bubbleClickPop}
			  data={this.state.pop_list} //array of objects
			/>
	      </div>

	      <div className='container'>
	      	<h3>People Roles by Hours Worked</h3>
	      	<p>Light gray nodes are unique roles (only 1 person has that role).</p>
	      	<BubbleChart
			  graph= {{
			    zoom: 0.7,
			    offsetX: 0,
			    offsetY: 0
			  }}
			  width={800}
  			  height={800}
			  showLegend={false} // optional value, pass false to disable the legend.
			  valueFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  labelFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  //Custom bubble/legend click functions such as searching using the label, redirecting to other page
			  bubbleClickFun={this.bubbleClickHours}
			  data={this.state.hours_list} //array of objects
			// {
			//		label: 'label', // Text to display.
			// 		value: 1, // Numeric Value.
			// 		/**
			//   	Color of the circle this value it's optional,
			//   	accept hex (#ff0000) and string (red) name values.
			//   	If you don't provide a value the default behavior
			//   	is assign a color based on a d3.schemeCategory20c
			//   	color schema.
			// 		*/
			// 		color: '#ff00ff'
			// }
			/>
	      </div>

	      <div className='container'>
	      	<h3>People Roles by Monetary Compensation</h3>
	      	<p>Light gray nodes are unique roles (only 1 person has that role).</p>
	      	<BubbleChart
			  graph= {{
			    zoom: 0.7,
			    offsetX: 0,
			    offsetY: 0
			  }}
			  width={800}
  			  height={800}
			  showLegend={false} // optional value, pass false to disable the legend.
			  valueFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  labelFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  bubbleClickFun={this.bubbleClickComp}
			  data={this.state.comp_list} //array of objects
			/>
	      </div>

	      <div className='container'>
	      	<h3>People Roles by Monetary Compensation (US Income Brackets)</h3>
	      	<p>The darker the larger income according to this scale:</p>
	      	<ul>
	      		<li>less than 13000</li>
	      		<li>between 13000 and </li>
	      		<li>between 13000 and 50000</li>
	      		<li>between 50000 and 85000</li>
	      		<li>between 150000 and 200000</li>
	      		<li>more than 200000</li>
	      	</ul>
	      	<BubbleChart
			  graph= {{
			    zoom: 0.7,
			    offsetX: 0,
			    offsetY: 0
			  }}
			  width={800}
  			  height={800}
			  showLegend={false} // optional value, pass false to disable the legend.
			  valueFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  labelFont={{
			        family: 'Arial',
			        size: 0,
			        color: '#fff',
			        weight: 'bold',
			      }}
			  bubbleClickFun={this.bubbleClickComp}
			  data={this.state.comp_b_list} //array of objects
			/>
	      </div>

	    </div>
	    ); 
	}
}

export default PeopleRoles;