// compare annual fuel cost with smog rating for models (scatter plot)

// get all models
import React, { Component } from 'react';
import * as d3 from "d3";
import axios from 'axios';
import '../styles/plot.css'


// Scatterplot visulization for UnbEATable Food
class GreenCarScatterPlot extends Component {
    constructor(props) {
      super(props);
      this.state = {
            data: props.data
      };
      this.drawChart = this.drawChart.bind(this);
    }

    drawChart() {

    	console.log('in drawChart');
			
        var body = d3.select('#scatterplot')
            var margin = { top: 50, right: 50, bottom: 50, left: 50 }
            var h = 500 - margin.top - margin.bottom
            var w = 500 - margin.left - margin.right
            // Scales
        var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
        var xScale = d3.scaleLinear()
            .domain([
            -2, 11
                // d3.min([0, d3.min(data.data,function (d) { return d.avg_smog_rating })]),
                // d3.max([0, d3.max(data.data,function (d) { return d.avg_smog_rating })])
                ])
            .range([0,w])
        var yScale = d3.scaleLinear()
            .domain([
            0, 2500
                // d3.min([0,d3.min(data.data,function (d) { return d.avg_fuelCost })]),
                // d3.max([0,d3.max(data.data,function (d) { return d.avg_fuelCost })])
                ])
            .range([h,0])
            // SVG
            var svg = body.append('svg')
                .attr('height',h + margin.top + margin.bottom)
                .attr('width',w + margin.left + margin.right)
            .append('g')
                .attr('transform','translate(' + margin.left + ',' + margin.top + ')')
            // X-axis
            var xAxis = d3.axisBottom(xScale)
            .ticks(10)
        // Y-axis
            var yAxis = d3.axisLeft(yScale)
            .ticks(10)
        // Circles
        var circles = svg.selectAll('circle')
            .data(this.state.data)
            .enter()
            .append('circle')
            .attr('cx',function (d) { return xScale(d.avg_smog_rating) })
            .attr('cy',function (d) { return yScale(d.avg_fuelCost) })
            .attr('r','3')
            .attr('stroke','black')
            .attr('stroke-width',1)
            .attr('fill',function (d,i) { return colorScale(i) })
            .on('mouseover', function () {
                d3.select(this)
                .transition()
                .duration(500)
                .attr('r',15)
                .attr('stroke-width',3)
            })
            .on('mouseout', function () {
                d3.select(this)
                .transition()
                .duration(500)
                .attr('r',3)
                .attr('stroke-width',1)
            })
            .append('title') // Tooltip
            .text(function (d) { return d.name +
                                    '\nSmog Rating: ' + (d.avg_smog_rating) +
                                    '\nFuel Cost: $' + (d.avg_fuelCost) })
        // X-axis
        svg.append('g')
            .attr('class','axis')
            .attr('transform', 'translate(0,' + h + ')')
            .call(xAxis)
            .append('text') // X-axis Label
            .attr('class','label')
            .attr('y',-17)
            .attr('x',w)
            .attr('dy','.71em')
            .style('fill','black')
            .text('Smog Rating')
        // Y-axis
        svg.append('g')
            .attr('class', 'axis')
            .call(yAxis)
            .append('text') // y-axis Label
            .attr('class','label')
            .attr('transform','rotate(-90)')
            .attr('x',0)
            .attr('y',5)
            .attr('dy','.71em')
            .style('fill','black')
            .text('Annual Fuel Cost')

        svg.append("line")
            .attr("x1", xScale(0))
            .attr("y1", yScale(0))
            .attr("x2", xScale(11))
            .attr("y2", yScale(2500))
            .attr("stroke-width", 2)
            .attr("stroke", "red")
            .attr("stroke-dasharray", "5,5")  //style of svg-line
    }

    componentDidMount() {
        this.drawChart();
    }

    render() {
      return (
        <div>
            <h1 className="display-4">Fuel Cost vs. Smog Rating</h1>
            <div id="scatterplot"></div>
        </div>
      );
    }
  }

export default GreenCarScatterPlot;