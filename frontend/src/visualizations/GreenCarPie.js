import React from 'react';
import * as d3 from 'd3';
import axios from 'axios';
// import carData from './carData.js';

class GreenCarPie extends React.Component {
	constructor(props){
		super(props)
		this.state = {
            data: props.data,
			totalCo2: props.totalCo2
		}
		this.createChart = this.createChart.bind(this)
	}

	componentDidMount() {
        console.log("HERES THE DATA")
        console.log(this.state)
		this.createChart(this.state.data)
	}

	createChart(data) {
		// var data = [
		// 	{name: "USA", value: 60},
		// 	{name: "UK", value: 20},
		// 	{name: "Canada", value: 30},
		// 	{name: "Maxico", value: 15},
		// 	{name: "Japan", value: 10},
		// ];
		var text = "";
		
		var width = 500;
		var height = 500;
		var thickness = 40;
		var duration = 750;
		var padding = 10;
		var opacity = .8;
		var opacityHover = 1;
		var otherOpacityOnHover = .8;
		var tooltipMargin = 13;
        
        var tooltip = d3.select("body")
			.append("div")
			.attr("class", "tooltip")
			.style("opacity", 0);

		var radius = Math.min(width-padding, height-padding) / 2;
		var color = d3.scaleOrdinal(d3.schemeCategory10);
		
		var svg = d3.select("#chart")
		.append('svg')
		.attr('class', 'pie')
		.attr('width', width)
		.attr('height', height);
		
		var g = svg.append('g')
		.attr('transform', 'translate(' + (width/2) + ',' + (height/2) + ')');
		
		var arc = d3.arc()
		.innerRadius(0)
		.outerRadius(radius);
		
		var pie = d3.pie()
		.value(function(d) { return d.value; })
		.sort(null);
		
		var path = g.selectAll('path')
			.data(pie(data))
			.enter()
			.append("g")  
			.append('path')
			.attr('d', arc)
			.attr('fill', (d,i) => color(i))
			.style('opacity', opacity)
			.style('stroke', 'white')
			.on("mouseover", function(d) {
					d3.selectAll('path')
						.style("opacity", otherOpacityOnHover);
					d3.select(this) 
						.style("opacity", opacityHover);
		
					let g = d3.select("svg")
						.style("cursor", "pointer")
						.append("g")
						.attr("class", "tooltip")
						.style("opacity", 0);
		 
					g.append("text")
						.attr("class", "name-text")
						.text(`${d.data.name} (${d.data.value})`)
						.attr('text-anchor', 'middle');
				
					let text = g.select("text");
					let bbox = text.node().getBBox();
					let padding = 2;
					g.insert("rect", "text")
						.attr("x", bbox.x - padding)
						.attr("y", bbox.y - padding)
						.attr("width", bbox.width + (padding*2))
						.attr("height", bbox.height + (padding*2))
						.style("fill", "white")
						.style("opacity", 0.75);
                })
                .on('mouseover.tooltip', function(d) {
                    tooltip.transition()
                        .duration(300)
                        .style("opacity", .8);
                    tooltip.html("Name: " + d.data.name + "<br/>" +
                        '% of Total CO2: ' + d.data.value.toFixed(2) + '%' + "<p/>")
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY + 10) + "px");
                })
                .on("mouseout.tooltip", function() {
                    tooltip.transition()
                        .duration(100)
                        .style("opacity", 0);
                })
			.on("mousemove", function(d) {
						let mousePosition = d3.mouse(this);
						let x = mousePosition[0] + width/2;
						let y = mousePosition[1] + height/2 - tooltipMargin;
				
						let text = d3.select('.tooltip text');
						let bbox = text.node().getBBox();
						if(x - bbox.width/2 < 0) {
							x = bbox.width/2;
						}
						else if(width - x - bbox.width/2 < 0) {
							x = width - bbox.width/2;
						}
				
						if(y - bbox.height/2 < 0) {
							y = bbox.height + tooltipMargin * 2;
						}
						else if(height - y - bbox.height/2 < 0) {
							y = height - bbox.height/2;
						}
				
						d3.select('.tooltip')
							.style("opacity", 1)
							.attr('transform',`translate(${x}, ${y})`);
				})
			.on("mouseout", function(d) {   
					d3.select("svg")
						.style("cursor", "none")  
						.select(".tooltip").remove();
				d3.selectAll('path')
						.style("opacity", opacity);
				})
			.on("touchstart", function(d) {
					d3.select("svg")
						.style("cursor", "none");    
			})
			.each(function(d, i) { this._current = i; });
	
	}

	render(){
		return(
			<div id="chart">
            	<h1 className="display-4">Percentage of CO2 emission by Make</h1>
			</div>
		)
	}
}

export default GreenCarPie;