import React, {Component} from 'react'
import '../styles/visualization_page.css'
import {Tabs, Tab, TabList, TabPanel} from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import CharityCountryBarChart from './CharityCountryBarChart.js';
import CharityCountryMap from './CharityCountryMap.js'
import PeopleRoles from './peopleRoles1.js'
import loading from '../img/loading.gif'

const axios = require('axios')

class VisualizationPage extends Component {
    constructor(props) {
		super(props)
		this.state = {
			charities: {},
			countries: {},
			people: {}
		}
	}
	
	componentDidMount() {
		axios.get('https://api.thecharitywatch.org/countries_per_charity')
			.then(response => {
				this.setState({
					charities: response.data.charities
				})
				console.log(this.state)
			}).catch(error=>{console.log(error)})
		axios.get('https://api.thecharitywatch.org/charities_per_country')
		.then(response =>{
			this.setState({
				countries: response.data['countries']
			})
		}).catch(error=>{console.log(error)})
		axios.get('https://api.thecharitywatch.org/people-visualization')
		.then(response => {
			this.setState({
				people: response.data
			})
		}).catch(error => {console.log(error)})
	}

    render() {
        return (
            <div >
				{Object.keys(this.state.countries).length <= 0 ||
				 Object.keys(this.state.people).length <= 0 ||
				 Object.keys(this.state.charities).length <= 0 ?
					(<img className="loading_animation" src={loading} alt="loading..." />) :
					(
					<div className="visContainer">
						<CharityCountryBarChart charities={this.state.charities}/>
						<div className="spacer"></div>
						<CharityCountryMap countries={this.state.countries}/>
						<div className="spacer"></div>
						<PeopleRoles list={this.state.people.hours_list} title="People Roles Grouped by Average Hours"/>
						<div className="spacer"></div>
						<PeopleRoles list={this.state.people.comp_list} title="People Roles Grouped by Average Compensation"/>
						<div className="spacer"></div>
					</div>
					)
				}
				{/* {Object.keys(this.state.people).length > 0 &&
					<div>
						<PeopleRoles1 list = {this.state.people.pop_list}/>
						<PeopleRoles1 list = {this.state.people.hours_list}/>
						<PeopleRoles1 list = {this.state.people.comp_list}/>
					</div>
				}
				 */}
            </div>
        
        )
    }
}

export default VisualizationPage