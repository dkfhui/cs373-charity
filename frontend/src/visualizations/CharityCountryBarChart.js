import React from 'react';
import * as d3 from 'd3';
import * as d3Chromatic from 'd3-scale-chromatic';
import * as d3Legend from 'd3-legend';
import history from '../history';
import axios from 'axios';
import '../styles/pagination.css';
import first_page from "../img/first_page.svg";
import last_page from "../img/last_page.svg";
import next_page from '../img/next_page.svg';
import prev_page from '../img/prev_page.svg';

let names = []
let charityMap = {}
const barNum = 60

class CharityCountryBarChart extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			page: 0,
			numPages: 4,
			isLoading: true
		}
		this.createGraph = this.createGraph.bind(this)
		this.requestNewPage = this.requestNewPage.bind(this)
	}

	componentDidMount(){

		let url = 'https://api.thecharitywatch.org/countries_per_charity'
		let list = []
		axios.get(url)
			.then(response => {
				if(names.length < 1){
					response.data.charities.some((charity) => {
						names.push(charity.charity.name)
						charityMap[charity.charity.name] = {ein: charity.charity.ein, numCountries: charity.charity.num_countries}
					})
					names.sort((a, b) => {
						const aNum = charityMap[a].numCountries
						const bNum = charityMap[b].numCountries
						if(aNum == bNum) return 0;
						if(aNum > bNum) return -1;
						return 1
					})
				}
				// console.log(names)
				// console.log(charityMap)
				this.createGraph(this.state.page)
				this.setState({isLoading: false})
			})
			.catch(error=>console.log(error))
	}

	createGraph(page){
		var margin = {top: 20, right: 20, bottom: 30, left: 40}
    // width = 960 - margin.left - margin.right,
    // height = 500 - margin.top - margin.bottom;

		let width = 980;
		let height = 400

		var tooltip = d3.select("body")
			.append("div")
			.attr("class", "tooltip")
			.style("opacity", 0);

		// set the ranges
		var x = d3.scaleBand()
							.range([0, width])
							.padding(0.1);
		var y = d3.scaleLinear()
							.range([height, 0]);
							
		// append the svg object to the body of the page
		// append a 'group' element to 'svg'
		// moves the 'group' element to the top left margin
		var svg = d3.select("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
			.append("g")
				.attr("transform", 
							"translate(" + margin.left + "," + margin.top + ")");

			// get the data
			let data = names.slice(page*barNum, page*barNum+(barNum-1))
			console.log(this.state.page)
			console.log(data)

			// format the data
			// data.forEach(function(d) {
			// 	d.sales = +d.sales;
			// });

			// Scale the range of the data in the domains
			x.domain(data.map(function(d) { return d; }));
			y.domain([d3.min(data, (d)=>{
				if(charityMap[d].numCountries-10 >= 0)
					return (charityMap[d].numCountries-10)%10
				else
					return 0
			}), d3.max(data, function(d) { return charityMap[d].numCountries; })]);

			// append the rectangles for the bar chart
			svg.selectAll(".bar")
					.data(data)
				.enter().append("rect")
					.attr("class", "bar")
					.attr("x", function(d) { return x(d); })
					.attr("width", x.bandwidth())
					.attr("y", function(d) { return y(charityMap[d].numCountries); })
					.attr("height", function(d) { return height - y(charityMap[d].numCountries); })
					.on('mouseover.tooltip', function(d) {
						tooltip.transition()
							.duration(300)
							.style("opacity", .8);
						tooltip.html("Name: " + d + "<p/>")
							.style("left", (d3.event.pageX) + "px")
							.style("top", (d3.event.pageY + 10) + "px");
					})
					.on("mouseout.tooltip", function() {
						tooltip.transition()
							.duration(100)
							.style("opacity", 0);
					})
					.on("click", (d) => {
						console.log(d)
						tooltip.transition()
							.duration(100)
							.style("opacity", 0)
						history.push(`/charity_instance/${charityMap[d].ein}`)
					})

			// add the x Axis
			svg.append("g")
				.attr("transform", "translate(0," + height + ")")
				.style("font-size", "0px")
				.call(d3.axisBottom(x));

			// add the y Axis
			svg.append("g")
			.style("font-size", "15px")
				.call(d3.axisLeft(y))
		}

		requestNewPage(newPage){
			if(newPage < 0)
				newPage = 0
			if(newPage > this.state.numPages)
				newPage = this.state.numPages
			this.setState({page:newPage})
			d3.select("#chart").selectAll("*").remove();
			this.createGraph(newPage)
		}

			render(){
				return (
						<div id="visualization-container" className='container'>
							<h1 className="display-4">Countries-Charities</h1>
							<div className='chartContainer'>
								{this.state.isLoading == true ? <h1 className="display-4">Loading...</h1> : null}
								<svg id="chart" className='chart'>
								</svg>
							</div>
							<div className="pagination">
								<button className="page-button" id="first_page" type="button" onClick={() => this.requestNewPage(0)}><img width="30px" src={first_page} /></button>
								<button className="page-button" id="prev_page" onClick={() => this.requestNewPage(this.state.page - 1)}><img width="30px" src={prev_page} /></button>
								<div>
									<p className="event_desc">Page {this.state.page+1} of {this.state.numPages+1}</p>
								</div>
								<button className="page-button next_page" id="next_page" onClick={() => this.requestNewPage(this.state.page + 1)}><img width="30px" src={next_page} /></button>
								<button className="page-button" id="last_page" onClick={() => this.requestNewPage(this.state.numPages)}><img width="30px" src={last_page} /></button>
            </div>
						</div>
				); 
			}
}

export default CharityCountryBarChart;