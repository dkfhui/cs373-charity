import React, {Component} from 'react'
import '../styles/visualization_page.css'
import loading from '../img/loading.gif'
import GreenCarBoxPlot from './GreenCarBoxPlot';
import GreenCarScatterPlot from './GreenCarScatterPlot';
import GreenCarPie from './GreenCarPie';

const axios = require('axios')

class ProviderVisualizationPage extends Component {
    constructor(props) {
		super(props)
		this.state = {
			boxplot_data: [],
			scatterplot_data: {},
            pie_data: [],
            totalCo2: null
		}
	}
	
	componentDidMount() {
        let data = [];
        let url = 'https://cors.io?https://api.greencar.fun/models?page=';
    	let promises = [];
    	for (let pageCounter = 1; pageCounter <= 121; pageCounter++) {
    		promises.push(axios.get(url+pageCounter));
        }
        
        axios.all(promises).then(res => {
			res.forEach(function(response) {
		        data = data.concat(response.data.data);
            });
            this.setUpBarPlot(data)
            this.setState({
                scatterplot_data: data
            })
        })

        let promises2 = [];
        let data2 = []
        let co2 = 0.0
        url = `https://cors.io?https://api.greencar.fun/makes?page=`
    	for (let pageCounter = 1; pageCounter <= 5; pageCounter++) {
    		promises2.push(axios.get(url+pageCounter));
        }

        axios.all(promises2).then(res => {
			res.forEach(function(response) {
                response.data.data.forEach((make) => {
                    co2+= make.avg_co2
                    data2.push({name: make.name, value: make.avg_co2})
                })
            });
            for (let m = 0; m < data2.length; m++) {
                data2[m]['value'] = data2[m]['value']/ co2 * 100
            }
            this.setState({
                totalCo2: co2,
                pie_data: data2
            })
        })
    }
    
    setUpBarPlot(input_data) {
        let data = []
        let num_cars = 0
        let fuel_types = {}
        let bars = []
        for (let i = 0; i < input_data.length; i++) {
            let car = input_data[i]
            data.push({'Fuel Type': car.fuel_primary, 'name': car.name, 'Annual Fuel Cost': car.avg_fuelCost})
            if (!(car.fuel_primary in fuel_types)) {
                fuel_types[car.fuel_primary] = []
            }
            fuel_types[car.fuel_primary].push(car.avg_fuelCost)
        }
        this.setState({
            boxplot_data: data
        })
    }

    render() {
        console.log("RENDERING")
        return (
            <div >
                {this.state.boxplot_data.length <= 0 ||
                 Object.keys(this.state.scatterplot_data).length <= 0 ?
                        (<img className="loading_animation" src={loading} alt="loading..." />) :
                        (
                        <div className="visContainer">
                           <GreenCarBoxPlot data={this.state.boxplot_data}/>
                           <div className="spacer"></div>
                            <GreenCarScatterPlot data={this.state.scatterplot_data}/>
                            <div className="spacer"></div>
                            <GreenCarPie data={this.state.pie_data} totalCo2={this.state.totalCo2}/>
                            <div className="spacer"></div>
                        </div>
                        )
                }
            </div>
        
        )
    }
}

export default ProviderVisualizationPage