import React, {Component} from 'react'
import '../styles/plot.css'
const d3plu = window.d3plus;

// const d3plus = require('d3plus')
// const colorScale = {
//     min: '#005ce6',
//     max: '#096b15',
// };

// const axesProps = {
//     legend: {
//         xAxis: 'Fuel Types',
//         yAxis: 'Average Annual Fuel Cost',
//     },
//     padding: {
//         xAxis: 5,
//         yAxis: 5
//     },
//     translate: {
//         xAxis: 0,
//         yAxis: 0
//     },
//     ticksCount: 6,
// };


class GreenCarBoxPlot extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: props.data
        }
        console.log(this.state.data)
        this.handleBarHover = this.handleBarHover.bind(this);
        this.handleBarClick = this.handleBarClick.bind(this);
    }

    componentDidMount() {
    
          var visualization = d3plu.viz()
            .container("#viz")
            .data(this.state.data)
            .type("box")
            .id("name")
            .x("Fuel Type")
            .y("Annual Fuel Cost")
            .ui([{ 
                "label": "Visualization Type",
                "method": "type", 
                "value": ["box"]
              }])
            .draw()
    }
        
    handleBarHover(item) {
        console.log('hovered', item);
    }
    
    handleBarClick(item) {
        console.log('clicked', item);
    }
    
    render() {
        console.log(this.state)
        return (
            <div>
                <h1 className="display-4">Annual Fuel Cost Grouped by Fuel Type</h1>
                <div id="viz" className="myStyle"></div>
            </div>
        );
    };
    
}

export default GreenCarBoxPlot;