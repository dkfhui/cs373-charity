import React, { Component } from "react"
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from "react-simple-maps"
import worldmap from '../world-50m.json'
import ReactTooltip from 'react-tooltip'
import history from '../history';
import '../styles/vis-map.css'

const wrapperStyles = {
  width: "100%",
  maxWidth: 980,
  margin: "0 auto",
  display: "inline-block"
}

class CharityCountryMap extends Component {
    constructor(props) {
        super(props)
				this.state = {
					countries: props.countries,
					isLoading: true,
				}
    }

    getDefaultColor(code) {
        if (this.state.countries[code] && 'num_charities' in this.state.countries[code]) {
            let num_charities = this.state.countries[code]['num_charities']
            if (num_charities == 0) {
                return '#ffffff'
            } else if (num_charities <= 5) {
                return '#cce0ff'
            } else if (num_charities <= 15) {
                return '#99c2ff'
            } else if (num_charities <= 40) {
                return '#4d94ff'
            } else if (num_charities <= 60) {
                return '#005ce6'
            } else {
                return '#003380'
            }
        }
       return '#ffffff';
    }

    getHighlightColor(code) {
        return '#0f0f0f'
    }

    getTipContent(code) {
        if (this.state.countries[code] != null && 'num_charities' in this.state.countries[code]) {
            let info = this.state.countries[code]
            return info.name + ": " + info.num_charities
        }
    }

    goToCountryPage(code) {
        if (code in this.state.countries) {
            let path = `/country_instance/${code}`
            history.push(path)
        }
    }

    componentDidUpdate(prevProps) {
        if(!(JSON.stringify(this.props.countries) === JSON.stringify(prevProps.countries))) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({
								countries: this.props.countries,
								isLoading: false
						})
				}
    } 

    render() {
        return (
        <div>
            <h1 className="display-4">Charity Density World Map</h1>
            <div className="legend">
                <h5>Legend</h5>
                <div>
                    <div className="color-square white"></div><h6 className="inline-header">No charities</h6>
                </div>
                <div>
                    <div className="color-square first-blue"></div><h6 className="inline-header">1  - 5</h6>
                </div>
                <div>
                    <div className="color-square second-blue"></div><h6 className="inline-header">6  - 15</h6>
                </div>
                <div>
                    <div className="color-square third-blue"></div><h6 className="inline-header">16  - 40</h6>
                </div>
                <div>
                    <div className="color-square fourth-blue"></div><h6 className="inline-header">41  - 60</h6>
                </div>
                <div>
                    <div className="color-square fifth-blue"></div><h6 className="inline-header">More than 60</h6>
                </div>
            </div>
            <div style={wrapperStyles}>
                <ComposableMap
                projectionConfig={{
                    scale: 205,
                    rotation: [-11,0,0],
                }}
                width={980}
                height={551}
                style={{
                    width: "100%",
                    height: "auto",
                }}
                >
                <ZoomableGroup center={[0,20]} disablePanning="true">
                    <Geographies geography={worldmap}>
                    {(geographies, projection) => geographies.map((geography, i) => (
                        <Geography
                        key={ i }
                        data-tip={this.getTipContent(geography.properties.ISO_A3)}
                        geography={ geography }
                        projection={ projection }
                        onClick={() => {this.goToCountryPage(geography.properties.ISO_A3)}}
                        style={{
                            default: {
                            fill: this.getDefaultColor(geography.properties.ISO_A3),
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                            },
                            hover: {
                            fill: this.getHighlightColor(geography.properties.ISO_A3),
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                            },
                            pressed: {
                            fill: this.getHighlightColor(geography.properties.ISO_A3),
                            stroke: "#607D8B",
                            strokeWidth: 0.75,
                            outline: "none",
                            }
                        }}
                        />
                    ))}
                    </Geographies>
                </ZoomableGroup>
                </ComposableMap>
                <ReactTooltip type="info">{}</ReactTooltip>
            </div>
        </div>
    )
  }
}

export default CharityCountryMap;