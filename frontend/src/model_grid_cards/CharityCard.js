import React, {Component} from 'react';
import '../styles/card.css';
import { Card, Button, CardTitle, CardText,
    CardSubtitle, CardBody} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";


class CharityCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      charity: props,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ charity: nextProps });
  }

  //helper method for converting numbers to money strings
  intToMoneyString(num) {
    if (num == null) {
        return "not disclosed";
    }
    let str = num.toString();
    let newStr = "";
    let count = 0;
    for (let i = str.length - 1; i >= 0; i--) {
      count += 1;
      newStr = str.charAt(i) + newStr;
      if (count % 3 == 0) {
        if (count != str.length) { //dont add comma as first character
          newStr = "," + newStr;
        }
      }
    }
    return "$"+newStr;
  }

  render() {
    let imgStyle = {
      backgroundImage: `url(${this.state.charity.image_url})`
    }
    return (
      <Card className="grid-row"> 
        <div className="card-image" style={imgStyle}></div>
        <CardBody>
          <CardTitle className="title"><p className="lead name-title">{this.state.charity.name}</p></CardTitle>
          <CardSubtitle><p className="lead subtitle tag-line">{this.state.charity.tag_line}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Address:  </b>{`${this.state.charity.address}, ${this.state.charity.state}`}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Income Total:  </b>{this.intToMoneyString(this.state.charity.income_total)}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Asset Total:  </b>{this.intToMoneyString(this.state.charity.asset_total)}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>IRS Classification:  </b>{this.state.charity.irs_classification}</p></CardSubtitle>
          <p className="lead subtitle last-subtitle"><br/></p>
          <Button className="info-button" tag={Link} to={{pathname: `/charity_instance/${this.props.ein}`}}>More Info</Button>
        </CardBody>
      </Card>
    )
  }
}

export default CharityCard
