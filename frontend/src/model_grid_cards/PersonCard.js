import React, {Component} from 'react';
import '../styles/card.css';
import { Card, Button, CardTitle, CardText,
    CardSubtitle, CardBody} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";

class PersonCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      person: props,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ person: nextProps });
  }

  render() {
    let imgStyle = {
      backgroundImage: `url(${this.state.person.image_url})`
    }
    return (
      <Card className="grid-row">
        <div className="card-image" style={imgStyle}></div>
        <CardBody>
          <CardTitle className="title"><p className="lead name-title">{this.state.person.name}</p></CardTitle>
          <CardSubtitle><p className="lead subtitle main-role">{this.state.person.main_role}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle main-role-charity "><b>at </b>{this.state.person.main_charity}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Total Compensations:  </b>{this.state.person.total_compensation}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Hours per Week:  </b>{this.state.person.total_avg_hours}</p></CardSubtitle>
          <p className="lead subtitle last-subtitle"><br/></p>
          <Button className="info-button" tag={Link} to={{pathname: `/person_instance/${this.state.person.id}`}}>More Info</Button>
        </CardBody>
      </Card>
    )
  }
}

export default PersonCard
