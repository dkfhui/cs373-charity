import React, {Component} from 'react';
import '../styles/card.css';
import { Card, Button, CardTitle, CardText,
    CardSubtitle, CardBody} from 'reactstrap';
import { Link, Redirect } from "react-router-dom";


class CountryCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      country: props,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ country: nextProps });
  }

  render() {
    const img_url = `url(${this.state.country.image_url})`
    let population = this.state.country.population != null ? this.state.country.population : "Unknown"
    let gdp = this.state.country.gdp_per_capita != null ? this.state.country.gdp_per_capita : "Unknown"
    let gender_index = this.state.country.gender_gap_index != null ? this.state.country.gender_gap_index : "Unknown"
    let foreign_investment = this.state.country.foreign_investment_pct != null ? this.state.country.foreign_investment_pct : "Unknown"
    let life_expectancy = this.state.country.life_exp_rating != null ? this.state.country.life_exp_rating : "Unranked"

    let imgStyle = {
      backgroundImage: img_url
    }

    return (
      <Card className="grid-row">
        <div className="card-image" style={imgStyle}></div>
        <CardBody>
          <CardTitle className="title"><p className="lead name-title">{this.state.country.name}</p></CardTitle>
          <CardSubtitle><p className="lead subtitle main-role">{this.state.country.abbr}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Population:  </b>{population}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>GDP per Capita:  </b>{gdp}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Gender Gap Index:  </b>{gender_index}</p></CardSubtitle>
          <CardSubtitle><p className="lead subtitle"><b>Foreign Investment Amount:  </b>{foreign_investment}</p></CardSubtitle>
          <p className="lead subtitle last-subtitle"><br/></p>
          <Button className="info-button" tag={Link} to={{pathname: `/country_instance/${this.state.country.abbr}`, state: {referrer: this.state.country, img_url: 'https://restcountries.eu/data/' + this.state.country.abbr.toLowerCase() + '.svg'}}}>More Info</Button>
        </CardBody>
      </Card>
    )
  }
}

export default CountryCard
