import React, {Component} from 'react';
import '../styles/pagination.css'
import SearchResult from './SearchResult.js'
import '../styles/search_page.css'
import first_page from "../img/first_page.svg"
import last_page from "../img/last_page.svg"
import next_page from '../img/next_page.svg'
import prev_page from '../img/prev_page.svg'

const axios = require('axios')
const INSTANCES_PER_PAGE = 12

class SearchResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: props.searchTerm,
      model: props.model,
      num_pages: 5,
      page: 1,
      results: []
    }
    this.requestNewSearchResults = this.requestNewSearchResults.bind(this)
  }

  componentDidMount() {
    this.requestNewSearchResults(1)
  }

  // Don't know how to use componentDidUpdate yet
  // componentDidUpdate(prevProps){
  //   if(this.props.searchClicked !== prevProps.searchClicked){
  //     if(this.props.searchClicked){
  //       this.setState({searchTerm: this.props.searchTerm})
  //       this.requestNewSearchResults(1, this.props.searchTerm)
  //     }
  //   }
  // }

  // Want to switch to componentDidUpdate
  componentWillReceiveProps(nextProps){
    if(nextProps.searchTerm !== this.state.searchTerm && nextProps.isGlobal === true){
      this.setState({searchTerm: nextProps.searchTerm})
      this.requestNewSearchResults(1, nextProps.searchTerm)
    }

    if(nextProps.searchClicked){
      this.setState({searchTerm: nextProps.searchTerm})
      this.requestNewSearchResults(1, nextProps.searchTerm)
    }
  }

  requestNewSearchResults(newPage, newSearchTerm=null) {
    // get search results
    this.setState({results:[]})
    if (newPage > 0 && newPage <= this.state.num_pages) {
      let url = "";

      this.setState({page: newPage})
      switch(this.state.model){
        case "charity":
          url = `https://api.thecharitywatch.org/charity-search?q=${newSearchTerm==null ? this.state.searchTerm : newSearchTerm}&start=${newPage}&limit=${INSTANCES_PER_PAGE}`
          break
        case "person":
          url = `https://api.thecharitywatch.org/person-search?q=${newSearchTerm==null ? this.state.searchTerm : newSearchTerm}&start=${newPage}&limit=${INSTANCES_PER_PAGE}`
          break
        case "country":
          url = `https://api.thecharitywatch.org/country-search?q=${newSearchTerm==null ? this.state.searchTerm : newSearchTerm}&start=${newPage}&limit=${INSTANCES_PER_PAGE}`
          break
        default:
          url = ``
          break
      }

      if(url.length > 0){
        axios.get(url)
        .then(response => {
          this.setState({results: response.data['results']})
          this.setState({num_pages: response.data['num_pages']})
          window.scrollTo(0,0)
        })
        .catch(error=>console.log(error))
      }
    }
    if(typeof(this.props.searched) === 'function')
      this.props.searched(false)
  }

  render() {
    let search_results = []
    if (this.state.results && this.state.results.length > 0) {
      for (let i = 0; i < this.state.results.length; i++) {
          const entry = this.state.results[i]
          search_results.push(
            <SearchResult key={i} ein={entry[0]} fields={entry[1]['fields']} model={this.state.model} searchWords={this.state.searchTerm.split(/[ ,]+/).filter(Boolean)}></SearchResult>
          )
      }
    }

    return (
      <div id="searchWrapper">
        <h1 className="display-4 display_header">{`Displaying search results for ${this.state.searchTerm.replace(/ {1,}/g," ")}`}</h1>
        {search_results.length > 0 ? search_results : <h2>No Results Found</h2>}
        <div className="pagination">
          <button className="page-button" id="first_page" type="button" onClick={() => this.requestNewSearchResults(1)}><img width="30px" src={first_page} /></button>
            <button className="page-button" id="prev_page" onClick={() => this.requestNewSearchResults(this.state.page - 1)}><img width="30px" src={prev_page} /></button>
            <div>
              <p className="event_desc">Page {this.state.page} of {this.state.num_pages}</p>
            </div>
            <button className="page-button next_page" id="next_page" onClick={() => this.requestNewSearchResults(this.state.page + 1)}><img width="30px" src={next_page} /></button>
            <button className="page-button" id="last_page" onClick={() => this.requestNewSearchResults(this.state.num_pages)}><img width="30px" src={last_page} /></button>
        </div>
      </div>
    )
  }
}

export default SearchResults;
