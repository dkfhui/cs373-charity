import React, {Component} from 'react';
import '../styles/search_result.css'
import '../styles/pagination.css'
import { Link, Redirect } from "react-router-dom";
import SearchableText from './SearchableText'


class SearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: this.props.fields,
			model: props.model,
			ein: this.props.ein,
      searchWords: this.props.searchWords
    }
  }

  render() {
		let matches = []
    for (const field in this.state.fields) {
      if (field !== 'name') {
        matches.push(
          <div>
            <b>{`${field}:    `}</b>
            <SearchableText key={this.state.ein} search_terms={this.props.searchWords} text={this.state.fields[field]}/>
          </div>
				)
      }
    }
    return (
      <div className="result-wrapper">
        <Link to={`/${this.state.model}_instance/${this.state.ein}`}><h1 className="display-4 instance-title"><SearchableText search_terms={this.props.searchWords} text={this.state.fields.name}/></h1></Link>
        {matches}
      </div>
    )
  }
}

export default SearchResult;
