import React, {Component} from 'react';
import '../styles/search_page.css'
import {Tabs, Tab, TabList, TabPanel} from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import SearchResults from './SearchResults'

const axios = require('axios')
const INSTANCES_PER_PAGE = 12

class SearchGlobalResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: props.match.params.q,
    }
  }

	componentDidUpdate(prevProps){
		if(prevProps.match.params.q != this.props.match.params.q)
			this.setState({searchTerm:this.props.match.params.q})
	}

  render() {
    return (
			<div id="globalSearchWrapper">
				<Tabs>
					<TabList>
						<Tab>Charities</Tab>
						<Tab>Leaders</Tab>
						<Tab>Countries</Tab>
					</TabList>

					<TabPanel>
						<SearchResults searchTerm={this.state.searchTerm} isGlobal={true} model="charity"/>
					</TabPanel>
					<TabPanel>
						<SearchResults searchTerm={this.state.searchTerm} isGlobal={true} model="person"/>
					</TabPanel>
					<TabPanel>
						<SearchResults searchTerm={this.state.searchTerm} isGlobal={true} model="country"/>
					</TabPanel>

				</Tabs>
			</div>
    )
  }
}

export default SearchGlobalResults;
