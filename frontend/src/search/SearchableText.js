import React from 'react';
import Highlighter from 'react-highlight-words';

const SearchableText = ({search_terms, text}) => (
	<Highlighter
		searchWords={search_terms}
		autoEscape={true}
		highlightStyle={{fontWeight:"bold", opacity: ".5", color: "#c31d3b", 'backgroundColor': 'transparent'}}
		textToHighlight={text}
	/>
);

export default SearchableText;
