import React from 'react';

class SearchModel extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			searchType: props.searchType,
			searchTerm: props.searchTerm
		}
	}

	render() {
		return(
			<div className="input-group mb-3">
				<input type="text" className="form-control model-input" placeholder={`Search for ${this.state.searchType}`} aria-label={`Search for ${this.state.searchType}`} aria-describedby="basic-addon2" onChange={this.props.searchTermChanged}/>
				<div className="input-group-append">
					<button className="btn btn-outline-secondary model-btn" type="submit">Search</button>
				</div>
			</div>
		)
	}
}

export default SearchModel;