var articles = [
  {
    source: {
      id: "mashable",
      name: "Mashable"
    },
    author: "Sam Haysom",
    title: "Bill and Melinda Gates get asked whether billionaires should exist",
    description: "Should billionaires actually be a thing? That was one of the first questions Stephen Colbert put to Bill and Melinda Gates during their appearance on The Late Show on Tuesday, where they spoke about philanthropy and taxing the wealthy. \"I think you can make t…",
    url: "https://mashable.com/video/bill-melinda-gates-should-billionaires-exist/",
    urlToImage: "https://mondrian.mashable.com/2019%252F02%252F13%252F72%252F0f15f63b44724749947d644b8d9373b1.abea4.jpg%252F1200x630.jpg?signature=_3oJFVhiOxw75VlmtRlfX7vFwL4=",
    publishedAt: "2019-02-13T11:32:25Z",
    content: "Should billionaires actually be a thing?\r\nThat was one of the first questions Stephen Colbert put to Bill and Melinda Gates during their appearance on The Late Show on Tuesday, where they spoke about philanthropy and taxing the wealthy.\r\n\"I think you can make… [+662 chars]",
    people: [
        {
            name: "Susan Desmond-Hellmann", 
            url: "../../person/num/0"
        }
    ],
    orgs : [
        {
            title: "Bill & Melinda Gates Foundation",
            url: "../../charity/num/0"
        }
    ],
  },
  {
      source: {
          id: "cnn",
          name: "CNN"
      },
      author: "Bill and Melinda Gates",
      title: "Bill and Melinda Gates: 9 things that surprised us",
      description: "As the parents of two teenagers (and one former teenager), we thought we'd seen everything an adolescent could throw our way. But when Bill sat down with a small group of high school students in Chicago last year, we were surprised to discover how much you ca…",
      url: "https://www.cnn.com/2019/02/12/opinions/bill-and-melinda-gates-letter-9-surprises/index.html",
      urlToImage: "https://cdn.cnn.com/cnnnext/dam/assets/190211152840-bill-and-melinda-gates-letter-2019-super-tease.jpg",
      publishedAt: "2019-02-12T13:00:13Z",
      content: "The authors are co-chairs of the Bill &amp; Melinda Gates Foundation. This article is adapted from their 2019 Annual Letter. The views expressed here are those of the authors. View more opinion on CNN. \r\n (CNN)As the parents of two teenagers (and one former t… [+4783 chars]",
      people: [
        {
            name: "Susan Desmond-Hellmann", 
            url: "../../person/num/0"
        }
    ],
    orgs : [
        {
            title: "Bill & Melinda Gates Foundation",
            url: "../../charity/num/0"
        }
    ],
  },
  {
      source: {
          id: "usa-today",
          name: "USA Today"
      },
      author: "USA TODAY, Doug Stanglin, USA TODAY",
      title: "Flu is widespread in US with 15.2 million cases since October, but experts see a 'low-severity' season",
      description: "More people have received flu shots this year than last. The overall hospitalization rate so far this season is 20.1 per 100,000 people compared to 30.5 per 100,000 at this period last year.",
      url: "https://www.usatoday.com/story/news/nation/2019/02/15/flu-season-15-m-cases-widespread-puerto-rico-47-states-cdc-says/2868271002/?utm_source=google&utm_medium=amp&utm_campaign=speakable",
      urlToImage: "https://www.gannett-cdn.com/presto/2019/02/14/USAT/4ecfe4fc-c17f-466d-a725-580f677e395b-AP_Flu_Season-Peak_1.JPG?crop=4773,2714,x0,y528&width=3200&height=1680&fit=bounds",
      publishedAt: "2019-02-15T12:40:47Z",
      content: "In this Feb. 7, 2018 file photo, a nurse prepares a flu shot at the Salvation Army in Atlanta.\r\n (Photo11: David Goldman, AP)\r\nMidway through this year's flu season, as many as 15.2 million people have fallen sick with influenza since October in what the Cent… [+3666 chars]",
      people: [
        {
            name: "Brian Peddle", 
            url: "../../person/num/1"
        }
    ],
    orgs : [
        {
            title: "The Salvation Army",
            url: "../../charity/num/1"
        }
    ],
  },
  {
      source: {
          id: null,
          "name": "Espn.com"
      },
      author: "ESPN",
      title: "Devils celebrate '90s night with Puddy of 'Seinfeld'",
      description: "Patrick Warburton, who played \"David Puddy\" on \"Seinfeld,\" declined an appearance fee Tuesday night, instead asking the Devils to donate $25,000 to St. Jude Children's Research Hospital, which they did.",
      url: "http://www.espn.com/nhl/story/_/id/26034358/devils-give-fans-david-puddy-bobblehead-nod-seinfeld-character",
      urlToImage: "https://a4.espncdn.com/combiner/i?img=%2Fphoto%2F2019%2F0220%2Fr504124_1296x729_16%2D9.jpg",
      publishedAt: "2019-02-20T03:42:06Z",
      content: "The Devils gave the first 9,000 fans in attendance at Tuesday night's home game against the Pittsburgh Penguins a \"David Puddy\" bobblehead.\r\nPuddy was the \"Seinfeld\" character portrayed by Patrick Warburton, who painted his face in Devils colors in an episode… [+648 chars]",
      people: [
        {
            name: "James R. Downing", 
            url: "../../person/num/2"
        }
    ],
    orgs : [
        {
            title: "St. Jude Children's Research Hospital, Inc.",
            url: "../../charity/num/2"
        }
    ],
  }
]

export default articles;
