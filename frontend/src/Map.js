import React, { Component } from "react"
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from "react-simple-maps"
import { scaleLinear } from "d3-scale"
import worldmap from './world-50m.json'
import ReactTooltip from 'react-tooltip'
import history from './history';


const wrapperStyles = {
  width: "100%",
  maxWidth: 980,
  margin: "0 auto",
  borderWidth: 5,
  borderStyle: 'solid',
  borderColor: '#286da8',
  borderRadius: 100,
}

class BasicMap extends Component {
    constructor(props) {
        super(props)
        this.state = {countries: props.countries,
            title: props.title}
    }

    getDefaultColor(code) {
        return (code in this.state.countries) ? '#f19f4d' : '#ffffff'
    }

    getHighlightColor(code) {
        return (code in this.state.countries) ? '#0f0f0f' : '#ffffff'
    }

    getTipContent(code) {
        return (code in this.state.countries) ? this.state.countries[code] : ''
    }

    goToCountryPage(code) {
        if (code in this.state.countries) {
            let path = `/country_instance/${code}`
            history.push(path)
        }
      }

    render() {
        return (
        <div style={wrapperStyles}>
            <h1 className="display-4">{this.state.title}</h1>
            <ComposableMap
            projectionConfig={{
                scale: 205,
                rotation: [-11,0,0],
            }}
            width={980}
            height={551}
            style={{
                width: "100%",
                height: "auto",
            }}
            >
            <ZoomableGroup center={[0,20]} disablePanning="true">
                <Geographies geography={worldmap}>
                {(geographies, projection) => geographies.map((geography, i) => (
                    <Geography
                    key={ i }
                    data-tip={this.getTipContent(geography.properties.ISO_A3)}
                    geography={ geography }
                    projection={ projection }
                    onClick={() => {this.goToCountryPage(geography.properties.ISO_A3)}}
                    style={{
                        default: {
                        fill: this.getDefaultColor(geography.properties.ISO_A3),
                        stroke: "#607D8B",
                        strokeWidth: 0.75,
                        outline: "none",
                        },
                        hover: {
                        fill: this.getHighlightColor(geography.properties.ISO_A3),
                        stroke: "#607D8B",
                        strokeWidth: 0.75,
                        outline: "none",
                        },
                        pressed: {
                        fill: this.getHighlightColor(geography.properties.ISO_A3),
                        stroke: "#607D8B",
                        strokeWidth: 0.75,
                        outline: "none",
                        }
                    }}
                    />
                ))}
                </Geographies>
            </ZoomableGroup>
            </ComposableMap>
            <ReactTooltip type="info">{}</ReactTooltip>
        </div>
    )
  }
}

export default BasicMap
