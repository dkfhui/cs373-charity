import React from 'react'
import { Link, NavLink, Route } from "react-router-dom";
import './styles/global.css';
import history from './history';

class NavBar extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      searchTerm: ''
    }
    this.searchTermChanged = this.searchTermChanged.bind(this)
    this.searchClicked = this.searchClicked.bind(this)
  }

  searchTermChanged(event){
    this.setState({searchTerm: event.target.value})
  }

  searchClicked(event){
    event.preventDefault()
    let value = document.getElementById("global_search_box").value
    let path = `/global_search/${value}`;
    history.push(path);
  }

  render(props) {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark">
        <img className="logo navbar-brand" src={require('./img/charity-watch-logo.png')} />
        <NavLink className="navbar-brand dispay-3" to="/home">The Charity Watch</NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            <li key="charities_list" className="nav-item">
              <NavLink className="nav-link link" to={{pathname: "/charities_list", state:{showSearchResults:false}}}>Charities</NavLink>
            </li>
            <li key="persons_list" className="nav-item">
              <NavLink className="nav-link link" to={{pathname: "/persons_list", state:{showSearchResults:false}}}>Leaders</NavLink>
            </li>
            <li key="countries_list" className="nav-item">
              <NavLink className="nav-link link" to={{pathname: "/countries_list", state:{showSearchResults:false}}}>Countries</NavLink>
            </li>
            <li key="visualizations" className="nav-item">
              <div class="dropdown show">
                <NavLink className="nav-link link dropdown-toggle clear-border" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" to="/about">Visualizations</NavLink>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <NavLink className="dropdown-item" to="/our-visualizations">Our Visualizations</NavLink>
                  <NavLink className="dropdown-item" to="/provider-visualizations">Provider Visualizations</NavLink>
                </div>
              </div>
            </li>
            <li key="about" className="nav-item">
              <NavLink className="nav-link link" to="/about">About</NavLink>
            </li>
          </ul>
          <form className="input-group col-md-3">
            <input id="global_search_box" type="text" className="form-control global-input" placeholder="Search site" aria-label="Recipient's username" aria-describedby="basic-addon2" onChange={this.searchTermChanged}/>
            <div className="input-group-append">
              <button id="globalSearchButton" className="btn btn-outline-secondary global-btn" onClick={this.searchClicked}>Search</button>
            </div>
          </form>
        </div>
      </nav>
    );
  }
}

export default NavBar;
