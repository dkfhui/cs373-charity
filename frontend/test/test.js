// var assert = require('assert');
import React from 'react';
import Home from '../src/Home.js';
import About from '../src/About.js';
import BasicMap from '../src/Map.js';
import SearchResults from '../src/search/SearchResults.js';
import SearchModel from '../src/search/SearchModel.js';
import CharityCard from '../src/model_grid_cards/CharityCard.js';
import CountryCard from '../src/model_grid_cards/CountryCard.js';
import PersonCard from '../src/model_grid_cards/PersonCard.js';
import Grid from '../src/model_grids/Grid.js';
import Frame from '../src/scrollers/Frame.js';
import Scroller from '../src/scrollers/Scroller.js'
import { mount, shallow, configure } from 'enzyme';
import {expect} from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import jsdom from 'jsdom'
const { JSDOM } = jsdom;

const { document } = (new JSDOM('')).window;
const doc = global.document || document;
// const doc = jsdom.jsdom('<!doctype html><html><body></body></html>')
global.document = doc
global.window = doc.defaultView

configure({ adapter: new Adapter() });

// describe('Array', function() {
//   describe('#indexOf()', function() {
//     it('should return -1 when the value is not present', function() {
//       assert.equal([1, 2, 3].indexOf(4), -1);
//     });
//   });
// });

// Home.js
describe('Home', function() {
	const wrapper = shallow(<Home/>)
    it("rendered the carousel", function() {
        expect(wrapper.find('ol')).to.have.lengthOf(1)
    })

    it("rendered a previous and next button for the carousel", function() {
        expect(wrapper.find('a')).to.have.lengthOf(2)
    })
});

// About.js
describe('About', function() {
    const wrapper = shallow(<About/>)
	const instance = wrapper.instance()
	const emails = ["dkfhui@gmail.com", "dkfhui@utexas.edu", "arjunkunjilwar@utexas.edu", "arjun_kunjilwar@homedepot.com", "arjunkun@cs.utexas.edu", "rvn2010@hotmail.es", "jopar219@gmail.com", "benolea@utexas.edu"]
    const names = ["Darrin", "Arjun", "Rodrigo", "Jorge", "Ben"]
    
    describe('#constructor()', function() {
        it("created the state correctly", function() {
            expect(wrapper.state().Darrin).to.not.equal(undefined)
            expect(wrapper.state().Arjun).to.not.equal(undefined)
            expect(wrapper.state().Rodrigo).to.not.equal(undefined)
            expect(wrapper.state().Jorge).to.not.equal(undefined)
            expect(wrapper.state().Ben).to.not.equal(undefined)
            expect(wrapper.state().Total).to.not.equal(undefined)
        })
    })

    // describe('#componentDidMount()', function() {
    //     var mountedWrapper
    //     before(function() {
    //         moxios.install()
    //         mountedWrapper = mount(<About/>)
    //     })
    //     after(function() {
    //         moxios.uninstall()
    //         mountedWrapper.unmount()
    //     })
            
    //     it("is able to retrieve commits and set them correctly", function(done) {
            // moxios.wait(function() {
            //     let request = moxios.requests.at(0)
            //     console.log(request)
            //     let promise = request.respondWith({
            //         status: 201,
            //         headers: {
            //             "x-total-pages": 1
            //         },
            //         response: [
            //             {"committer_email": "dkfhui@utexas.edu", },
            //             {"id":"33fe830d25952011758879d3632b5996bb9ab9de","short_id":"33fe830d","created_at":"2019-03-28T23:56:09.000Z","parent_ids":["7e9ce558afd5ead1c1d8416a55ee1bfbd3a0cc92"],"title":"Adding fixed mocha stuff","message":"Adding fixed mocha stuff\n","author_name":"Darrin","author_email":"dkfhui@gmail.com","authored_date":"2019-03-28T23:56:09.000Z","committer_name":"Darrin","committer_email":"dkfhui@gmail.com","committed_date":"2019-03-28T23:56:09.000Z"}
            //         ]
            //     })

            //     promise.then(function() {
            //         // console.log(mountedWrapper.state().Darrin)
            //         expect(mountedWrapper.state().Darrin.commits).to.be.above(0)
            //         expect(mountedWrapper.state().Total.commits).to.be.above(0)
            //         done()
            //     })
            //     .catch(console.log)
            // })
    //         moxios.stubRequest('https://gitlab.com/api/v4/projects/11050555/repository/commits?page=1&per_page=100', {
    //             status: 200,
    //             headers: {
    //                 "x-total-pages": 1
    //             },
    //             response: [
    //                 {"id":"33fe830d25952011758879d3632b5996bb9ab9de","short_id":"33fe830d","created_at":"2019-03-28T23:56:09.000Z","parent_ids":["7e9ce558afd5ead1c1d8416a55ee1bfbd3a0cc92"],"title":"Adding fixed mocha stuff","message":"Adding fixed mocha stuff\n","author_name":"Darrin","author_email":"dkfhui@gmail.com","authored_date":"2019-03-28T23:56:09.000Z","committer_name":"Darrin","committer_email":"dkfhui@gmail.com","committed_date":"2019-03-28T23:56:09.000Z"}
    //             ]
    //         })

    //         moxios.wait(() => {
    //             // console.log(moxios.requests.at(0))
    //             expect(mountedWrapper.instance().state.Darrin.commits).to.equal(1)
    //             // expect(mountedWrapper.state().Total.commits).to.equal(1)
    //             done()
    //         })
    //     })
    // })

    describe('#addCommits()', function() {
        it("adds commits correctly", function() {
            emails.forEach(email => {
                expect(instance.addCommits(email)).to.equal(true)
            })
            expect(instance.addCommits("downing@cs.utexas.edu")).to.equal(false)
        })
    })
    
    describe('#addIssuesClosed()', function() {
        it("adds closed issues correctly", function() {
            names.forEach(name => {
                expect(instance.addIssuesClosed(name)).to.equal(true)
            })
            expect(instance.addIssuesClosed("Beto")).to.equal(false)
        })
    })

    describe('#addIssuesOpened()', function() {
        it("adds opened issues correctly", function() {
            names.forEach(name => {
                expect(instance.addIssuesOpened(name)).to.equal(true)
            })
            expect(instance.addIssuesOpened("Mohammed Lee")).to.equal(false)
        })
    })

    describe('#render()', function() {
        it("rendered 5 developer cards with images", function() {
            expect(wrapper.find('Card')).to.have.lengthOf(5)
            expect(wrapper.find('img')).to.have.lengthOf(5)
        })
        
        it("created the ReactTable", function() {
            expect(wrapper.find('ReactTable')).to.have.lengthOf(1)
        })

        it("rendered 3 unordered lists of information we want to display", function() {
            expect(wrapper.find('ul')).to.have.lengthOf(3)
        })

        it("rendered 5 headers that mark each section of the About page", function() {
            expect(wrapper.find('h1')).to.have.lengthOf(5)
        })
    })
});

// Map.js
describe('BasicMap', function() {
    const wrapper = shallow(<BasicMap/>)

    it("rendered correctly", function() {
        expect(wrapper.find('ComposableMap')).to.have.lengthOf(1)
        expect(wrapper.find('ZoomableGroup')).to.have.lengthOf(1)
        expect(wrapper.find('Geographies')).to.have.lengthOf(1)
        expect(wrapper.find('ReactTooltip')).to.have.lengthOf(1)
    })
});

// SearchResults.js
//describe('SearchResults', function() {
   // const wrapper = shallow(<SearchResults searchTerm="acadia" searchClicked={true} model="charity"/>)

  //  it("created the state correctly", function() {
        // expect(wrapper.state().Darrin).to.not.equal(undefined)
    //    expect(wrapper.state().searchTerm).to.equal("acadia")
       // expect(wrapper.state().model).to.equal("charity")
     //   expect(wrapper.state().results).to.have.lengthOf(0)
      //  console.log(wrapper.state().result)
   // })

   // it("displays 'No results found'", function() {
    //    expect(wrapper.find('h2')).to.have.lengthOf(1)
 //   })
//});

// SearchModel.js
describe('SearchModel', function() {
    const wrapper = shallow(<SearchModel searchType="charities" searchTerm="acadia"/>)

    it("created the state correctly", function() {
        expect(wrapper.state().searchType).to.equal("charities")
        expect(wrapper.state().searchTerm).to.equal("acadia")
    })

    it("rendered correctly", function() {
        expect(wrapper.find('div')).to.have.lengthOf(2)
        expect(wrapper.find('input')).to.have.lengthOf(1)
        expect(wrapper.find('button')).to.have.lengthOf(1)
    })
});

// CharityCard.js
describe('CharityCard', function() {
    const wrapper = shallow(<CharityCard/>)

    it("rendered correctly", function() {
        expect(wrapper.find('Card')).to.have.lengthOf(1)
        expect(wrapper.find('CardBody')).to.have.lengthOf(1)
        expect(wrapper.find('CardTitle')).to.have.lengthOf(1)
        expect(wrapper.find('CardSubtitle')).to.have.lengthOf(5)
        expect(wrapper.find('Button')).to.have.lengthOf(1)
    })
});

// CountryCard.js
describe('Country', function() {
    const wrapper = shallow(<CountryCard abbr='ABC'/>)

    it("rendered correctly", function() {
        expect(wrapper.find('Card')).to.have.lengthOf(1)
        expect(wrapper.find('CardBody')).to.have.lengthOf(1)
        expect(wrapper.find('CardTitle')).to.have.lengthOf(1)
        expect(wrapper.find('CardSubtitle')).to.have.lengthOf(5)
        expect(wrapper.find('Button')).to.have.lengthOf(1)
    })
});

// PersonCard.js
describe('PersonCard', function() {
    const wrapper = shallow(<PersonCard/>)

    it("rendered correctly", function() {
        expect(wrapper.find('Card')).to.have.lengthOf(1)
        expect(wrapper.find('CardBody')).to.have.lengthOf(1)
        expect(wrapper.find('CardTitle')).to.have.lengthOf(1)
        expect(wrapper.find('CardSubtitle')).to.have.lengthOf(4)
        expect(wrapper.find('Button')).to.have.lengthOf(1)
    })
});

// Grid.js
describe('Grid', function() {
    const wrapper = shallow(<Grid/>)

    it("rendered correctly", function() {
        expect(wrapper.find('Row')).to.have.lengthOf(1)
    })
});

// NewsScroller.js
describe('NewsScroller', function() {
    const wrapper = shallow(<Scroller data={{}} model="NEWS"/>)

    it("rendered correctly", function() {
        expect(wrapper.find('div')).to.have.lengthOf(2)
        expect(wrapper.find('h1')).to.have.lengthOf(1)
    })
});

// PersonScroller.js
describe('PersonScroller', function() {
    const wrapper = shallow(<Scroller data={{}} model="PERSON"/>)

    it("rendered correctly", function() {
        expect(wrapper.find('div')).to.have.lengthOf(2)
        expect(wrapper.find('h1')).to.have.lengthOf(1)
    })
})

// PersonFrame.js
describe('PersonFrame', function() {
    const wrapper = shallow(<Frame data={{name: "Arjun Kunjilwar"}} model="PERSON"/>)

    it("rendered correctly", function() {
        expect(wrapper.find('div')).to.have.lengthOf(3)
        expect(wrapper.find('p')).to.have.lengthOf(1)
    })
})