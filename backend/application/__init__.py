from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask_cors import CORS

application = Flask(__name__, static_folder="../frontend/build")
application.config.from_object("config")
CORS(application)
db = SQLAlchemy(application)
engine = create_engine(
    "postgres://akruj19:Charity20!9@charitywatchdb.cmdjdp6ayomp.us-east-2.rds.amazonaws.com:5432/charityinfodb"
)
