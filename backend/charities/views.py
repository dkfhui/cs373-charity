import os, json
from flask import Flask, send_from_directory, request, abort
from application import engine
from sqlalchemy import create_engine, func, text
from sqlalchemy.sql import union
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import column
from .models import Charity
from werkzeug.exceptions import HTTPException
from helpers import *


def get_charity(ein):
    from news import news_helper

    # make sure numeric ein passed
    if ein.isdigit() == False:
        abort(400)
    # filter for charity with ein ein
    res = Charity.query.filter(Charity.ein == ein)
    if res.count() < 1:
        abort(404)  # no charity found
    elif res.count() > 1:
        abort(500)  # should not occur
    else:
        result = res.first().get_complete_attributes()
        result["news"] = news_helper(result["charity"]["name"])
        return json.dumps(result)


# possible arguments:
# name, state, irs_classification, start, limit, sort, distance_to_person
# Sort values are: 'name', 'income_total' and 'asset_total'.
def get_charities():
    # Defaults
    page = 1
    num_instances = 9
    # get start parameter
    startArg = request.args.get("start")
    if startArg != None:
        page = int(startArg)
        if page <= 0:  # pages are 1 indexed
            abort(400)
    # get limit parameter
    limitArg = request.args.get("limit")
    if limitArg != None:
        num_instances = int(limitArg)
        if num_instances < 0 or num_instances > 50:
            abort(400)
    try:
        # Do base query
        charities_list = Charity.query

        # Apply any filters
        name = request.args.get("name")
        if name != None:
            if name == "":
                abort(400)
            # check if the passed in name is contained in any parameters
            charities_list = charities_list.filter(
                func.lower(Charity.name).contains(name.lower())
            )

        state = request.args.get("state")
        if state != None:
            if state == "":
                abort(400)
            charities_list = charities_list.filter(
                func.lower(Charity.state) == state.lower()
            )

        irs_classification = request.args.get("irs_classification")
        if irs_classification != None:
            if irs_classification == "":
                abort(400)
            charities_list = charities_list.filter(
                func.lower(Charity.irs_classification).contains(
                    irs_classification.lower()
                )
            )

        # Apply sorting if necessary
        orderby = request.args.get("orderby")
        desc = request.args.get("desc")
        if desc != None:
            if desc == "true":
                desc = True
        if orderby != None:
            if orderby == "name":
                if desc == True:
                    charities_list = charities_list.order_by(Charity.name.desc())
                else:
                    charities_list = charities_list.order_by(Charity.name)
            elif orderby == "income_total":
                if desc == True:
                    charities_list = charities_list.order_by(
                        Charity.income_total.desc()
                    )
                else:
                    charities_list = charities_list.order_by(Charity.income_total)
            elif orderby == "asset_total":
                if desc == True:
                    charities_list = charities_list.order_by(Charity.asset_total.desc())
                else:
                    charities_list = charities_list.order_by(Charity.asset_total)
            elif orderby == "state":
                if desc == True:
                    charities_list = charities_list.order_by(Charity.state.desc())
                else:
                    charities_list = charities_list.order_by(Charity.state)
            else:
                abort(400)
        else:
            charities_list = charities_list.order_by(Charity.ein)

        # Google like search

        # Paginate with sqlalchemy
        charities_list = charities_list.paginate(page, per_page=num_instances)
        if len(charities_list.items) < 1:
            abort(404)  # no results
        result = {
            "charities": [charity.get_attributes() for charity in charities_list.items]
        }
        result["num_pages"] = charities_list.pages
        return json.dumps(result)

    except HTTPException:
        print("http")
        raise
    except Exception as e:
        # abort(500)
        print(e)
        return ""


# parameter: search term, start page ,limit
def search_charities():
    # Defaults
    page = 1
    num_instances = 9
    # get start parameter
    startArg = request.args.get("start")
    if startArg != None:
        page = int(startArg)
        if page <= 0:  # pages are 1 indexed
            abort(400)
    # get limit parameter
    limitArg = request.args.get("limit")
    if limitArg != None:
        num_instances = int(limitArg)
        if num_instances < 0 or num_instances > 50:
            abort(400)
    # start search algorithm
    searchTerm = request.args.get("q")
    if searchTerm == None:
        abort(400)
    elif searchTerm == "":
        abort(404)
    else:
        terms = searchTerm.split()
        results_dict = {}
        charities_list = Charity.query
        priorities = {"name": [10, 4], "tag_line": [5, 2], "mission_statement": [3, 1]}
        # handle the whole search term
        if len(terms) > 1:
            terms.insert(0, searchTerm)
        index = 0
        for term in terms:
            # do name first
            sub_list = charities_list.filter(Charity.name.ilike("%" + term + "%"))
            for charity in sub_list:
                if charity.ein in results_dict:
                    results_dict[charity.ein]["priority"] += priorities["name"][
                        index != 0
                    ]
                    results_dict[charity.ein]["fields"]["name"] = charity.name
                else:
                    results_dict[charity.ein] = {
                        "priority": priorities["name"][index != 0],
                        "fields": {"name": charity.name},
                    }
                results_dict[charity.ein]["fields"]["name"] = charity.name
            # do tag line next
            sub_list = charities_list.filter(Charity.tag_line.ilike("%" + term + "%"))
            for charity in sub_list:
                if charity.ein in results_dict:
                    results_dict[charity.ein]["priority"] += priorities["tag_line"][
                        index != 0
                    ]
                    results_dict[charity.ein]["fields"]["tag_line"] = charity.tag_line
                else:
                    results_dict[charity.ein] = {
                        "priority": priorities["tag_line"][index != 0],
                        "fields": {"tag_line": charity.tag_line},
                    }
                results_dict[charity.ein]["fields"]["name"] = charity.name
            # do tag line next
            sub_list = charities_list.filter(
                Charity.mission_statement.ilike("%" + term + "%")
            )
            for charity in sub_list:
                # only get a portion of the mission statement, include up to 10 words before and after
                statement = getTextPortion(charity.mission_statement, term)
                if charity.ein in results_dict:
                    results_dict[charity.ein]["priority"] += priorities[
                        "mission_statement"
                    ][index != 0]
                    results_dict[charity.ein]["fields"]["mission_statement"] = statement
                else:
                    results_dict[charity.ein] = {
                        "priority": priorities["mission_statement"][index != 0],
                        "fields": {"mission_statement": statement},
                    }
                results_dict[charity.ein]["fields"]["name"] = charity.name
        sorted_charities = sorted(
            results_dict.items(), key=lambda kv: kv[1]["priority"], reverse=True
        )
        # now create and return response
        num_pages = (len(sorted_charities) // num_instances) + (
            (len(sorted_charities) % num_instances) != 0
        )
        if page > num_pages:
            abort(404)
        else:
            results = sorted_charities[
                (page - 1) * num_instances : page * num_instances
            ]
            json_res = {"results": results, "num_pages": num_pages, "page": page}
            return json.dumps(json_res)


def get_all_states():
    try:
        state_names = Charity.query.distinct(Charity.state)
        states = {}
        states["states"] = [entry.state for entry in state_names]
        return json.dumps(states)
    except:
        abort(500)


def get_all_classifications():
    try:
        irs_class_names = Charity.query.distinct(Charity.irs_classification)
        class_names = {}
        class_names["irs_class_names"] = [
            entry.irs_classification for entry in irs_class_names
        ]
        return json.dumps(class_names)
    except:
        abort(500)

def get_countries_per_charity():
    try:
        charities = Charity.query
        char_list = []
        for charity in charities:
            info = {}
            info['charity'] = {'ein': charity.ein, 'name': charity.name, 'num_countries': len(charity.countries)}
            char_list.append(info)
        result = {'charities': char_list}
        return json.dumps(result)
    except:
        abort(500)