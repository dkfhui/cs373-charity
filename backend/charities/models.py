from application import db
from people.models import CharitiesPeopleAssociation

charities_countries = db.Table(
    "charities_countries_real",
    db.Model.metadata,
    db.Column("charity_id", db.Integer, db.ForeignKey("charities_real.id")),
    db.Column("country_id", db.Integer, db.ForeignKey("countries_real.id")),
)


class Charity(db.Model):
    __tablename__ = "charities_real"
    id = db.Column(db.Integer, primary_key=True)
    ein = db.Column(db.Integer)
    name = db.Column(db.String(128), unique=True)
    tag_line = db.Column(db.String(128))
    mission_statement = db.Column(db.Text)
    address = db.Column(db.String(128))
    state = db.Column(db.String(128))
    income_total = db.Column(db.String(15))
    asset_total = db.Column(db.String(15))
    irs_classification = db.Column(db.String(255))
    image_url = db.Column(db.Text)

    people_assoc = db.relationship(CharitiesPeopleAssociation)

    countries = db.relationship(
        "Country", secondary="charities_countries_real", back_populates="charities"
    )
    people = db.relationship(
        "Person", secondary="charities_people_real", back_populates="charities"
    )

    def __init__(self, args):
        self.ein = int(args["ein"])
        self.name = args["name"]
        self.tag_line = args["tag_line"] if "tag_line" in args else None
        self.mission_statement = args["mission"] if "mission" in args else None
        self.address = args["address"]
        self.state = args["state"]
        self.income_total = args["income"] if "income" in args else None
        self.asset_total = args["assets"] if "assets" in args else None
        self.irs_classification = args["class"] if "class" in args else None
        self.image_url = args["image_url"] if "image_url" in args else None

    def __repr__(self):
        return "<Data %r>" % self.name

    def get_attributes(self):
        attr = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        return {"charity": attr}

    def get_complete_attributes(self):
        attributes = self.get_attributes()
        related = {
            "countries": [
                {"name": country.name, "abbr": country.abbr}
                for country in self.countries
            ],
            "people": [
                {"name": person.name, "id": person.id} for person in self.people
            ],
        }
        attributes["charity"] = {**attributes["charity"], **related}
        return attributes
