from itertools import repeat


def UnionType(*args):
    return args


charity_schema_simple = {
    "charity": {
        "ein": int,
        "name": str,
        "tag_line": UnionType(type(None), str),
        "mission_statement": UnionType(type(None), str),
        "address": str,
        "state": str,
        "income_total": UnionType(type(None), str),
        "asset_total": UnionType(type(None), str),
        "irs_classification": str,
        "image_url": str,
    }
}
charity_schema_complete = {
    "charity": {
        "ein": int,
        "name": str,
        "tag_line": UnionType(type(None), str),
        "mission_statement": UnionType(type(None), str),
        "address": str,
        "state": str,
        "income_total": UnionType(type(None), str),
        "asset_total": UnionType(type(None), str),
        "irs_classification": str,
        "image_url": str,
        "countries": repeat({"name": str, "abbr": str}),
        "people": repeat({"id": int, "name": str}),
    }
}
person_schema_simple = {
    "person": {
        "name": str,
        "total_compensation": int,
        "total_avg_hours": int,
        "main_charity": str,
        "main_role": str,
        "image_url": str,
    }
}
person_schema_complete = {
    "person": {
        "name": str,
        "total_compensation": int,
        "total_avg_hours": int,
        "main_charity": {"ein": int, "name": str},
        "main_role": str,
        "image_url": str,
        "countries": repeat({"name": str, "abbr": str}),
        "charities": repeat({"ein": int, "name": str}),
    }
}
country_schema_simple = {
    "country": {
        "abbr": str,
        "name": str,
        "population": UnionType(type(None), int),
        "gdp_per_capita": UnionType(type(None), float),
        "gender_gap_index": UnionType(type(None), float),
        "foreign_investment_pct": UnionType(type(None), float),
    }
}
country_schema_complete = {
    "country": {
        "abbr": str,
        "name": str,
        "population": UnionType(type(None), int),
        "gdp_per_capita": UnionType(type(None), float),
        "gender_gap_index": UnionType(type(None), float),
        "foreign_investment_pct": UnionType(type(None), float),
        "charities": repeat({"ein": int, "name": str}),
        "people": repeat({"id": int, "name": str}),
    }
}
