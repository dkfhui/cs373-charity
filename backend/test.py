import unittest
from charities import *
from countries import *
from people import *
from people.models import *
from news import *
from sqlalchemy import create_engine
from application import db, application

from werkzeug.exceptions import BadRequest

from itertools import repeat
import json
from schemas import *


class SchemaTests(unittest.TestCase):
    @classmethod
    def setUp(cls):
        info = {
            "ein": -1,
            "name": "Helping people",
            "tag_line": "We help all people",
            "mission_statement": "To get all people help",
            "address": "711-2880 Nulla St. Mankato Mississippi 96522",
            "state": "NY",
            "income": "123456",
            "assets": "1234567",
            "class": "Charitable Organization",
            "image_url": "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4REQ0NzJGMDNEOEExMUUzQTg5NjlERDAzNDkzOTZCQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4REQ0NzJGMTNEOEExMUUzQTg5NjlERDAzNDkzOTZCQyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhERDQ3MkVFM0Q4QTExRTNBODk2OUREMDM0OTM5NkJDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhERDQ3MkVGM0Q4QTExRTNBODk2OUREMDM0OTM5NkJDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgBEwETAwERAAIRAQMRAf/EAHsAAQACAwACAwAAAAAAAAAAAAAHCAYJCgEFAgMEAQEAAAAAAAAAAAAAAAAAAAAAEAABBAMAAQEDBwsDBQEAAAAAAgQFBgEDBwgREhMJIRTWV5cYGfAiFdV2d7c4WCk5krUXMUEyQiMWEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDuQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMGuvT+a810s3HReh0agt5Ba9bDfdbbAVXS+2a8Yzs1s9s7IMEOV68Z9VYRlWcf8AcCPPvWeLv9SXA/th559IgH3rPF3+pLgf2w88+kQD71ni7/UlwP7YeefSIB96zxd/qS4H9sPPPpEA+9Z4u/1JcD+2Hnn0iAfes8Xf6kuB/bDzz6RAZBWPIDg92mG9epnbOR26fd4VlrB1jpFNn5hzhHp7eW8ZFTLt7uwj2sevsoz6eoEuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHMjq5Nr85PiOd/oXULdY4mNrMz06OincCpit4zheZ2vTTq9Ds0SbV2yaMstFe+24Tqzle5S1f+exS8hc5/wDBm8eYpnvkJPsnUI1g1R7x0+fuqQzZttfrhPvN7lxXdenSj2lYx6qVjHrkAw+DN48yrPRIRnZOoSTB0j3jV8wdUh4zc6/XKfeaHLeu7NO5HtJzj1SrOPXAH6/wV+EfWt1v/VTvo0A/BX4R9a3W/wDVTvo0A/BX4R9a3W/9VO+jQD8FfhH1rdb/ANVO+jQFJvO74f1B8QuV1HqVB6BfJmZf9NhqllvPKhdSGaHdctdi0yrB3Cx0Y6bP2bqsISnPqrHpsyrHs5Tj1Dfz4w3Cc6B468SulmdZfWKycxp0pNv1evvJCU3wjT57IbvXOf8A7vnCVbdnp8ntrz6Yxj0wBOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAADnu8Jv8AKt5QftD5J/xbbAVa+KN2+99B8mbpzqUkpJpROWvI6DrNV99t0x3zxUIweydkdtEqToeScq8f7PcuFJytDH3WtOfTCsqD9fwse3Xyi+TVN5nHS0k7oXT1zcRYKrsdONsXqfN4CRmI6yMWKlqbNJZi6ikI270JwrYzXtQrOfzcpDqrA8ZzhOMqVnCUpxnKlZzjGMYxj1znOc/JjGMAVp5T5d8F7T0e8cr59dmsxbKLuyhxqzhGlhZW2hKEyUlTH3vVa7HGw71WW7nbqxj0Wn3iMLbr17lhZcDUX8Z/+V2h/v8AKv8Aw86kBdfws/lL8dv3SUv/AGduBZ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAABz3eE3+Vbyg/aHyT/i22Auz5ofDYqflHaU9Nq9v/AOOek7WLRhPOXMQubgLdpjW+plFbpJvqfsHkXKsWGlDfDrVnenY31a9atPqnC8B9vhb8N2p+LNl29Kstuz0Tpn6PeRkM70RGIavVNnJasN5FcU13u379/MPGvt6FPNi9CUtt2zUnRj2lLUGzMDno+JB8RLdYd9h8eeETKtNcb7HUL0roMW49Nlj268qbyFRq7zQv8yva1YVqfvEK9X+cK06s/NcLU6DSxUrbZaHZYS406bkK5aK5IaJSFmovflu9YPW+fVG3UvHqlaFpzlGzWvCte3WpSFpUhSk5Dqw8EvO2teVNaRWLOuPrnbq5Hp2WCv61YbsrUyb4Rr2Wuqa9i8qW2WrOMvGeMq2Mdiv/AG0qQvIQz8Z/+V2h/v8AKv8Aw86kBdfws/lL8dv3SUv/AGduBZ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAABz3eE3+Vbyg/aHyT/i22A3odS6lRuMUad6L0WdbV6rV5tne8eb8+3vc71+qWkZGNE59/Iy0jv9NTdvqxnZt2ZxjGPT1zgOcOa+LP2px5FNunw7b5tyKO99At+Oud+vDOTqe91q2OX8q/1a9vu7059wjdqe68L1s1JxoSnY3zuTvCWPOL4ozTo9Oa8x8b3k3Dw9pgmu/oF0etd8NPatMm0Rsd0OG1ZV71mpunbnRKPdS1J3ZwrS2WrTlW3aGkQABkVSttlodlhLjTpuQrlorkholIWai9+W71g9b59UbdS8eqVoWnOUbNa8K17dalIWlSFKTkOgv4pFhkrd4F+PFrmV6tkxZ77yWwyuzRqS30bJKa410GSfL06EfmadSnTleUox8icfJgDYt4Wfyl+O37pKX/ALO3As6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA57vCb/Kt5QftD5J/wAW2wFUPidd16J0jyQuvOJ6WzrovKJxUJUqyx95ojde/Me02PZ+R1Z2L/SE89U5UjO9fyadGMa9SUYyvKw1vgAAAAB0I/Eh/wAc3ix+0PD/AOBd5A2ZeFn8pfjt+6Sl/wCztwLOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOafjvWaJ4z/E08lLZ22Wc0mvPrV3BrpkNsLOTSsKtt7b2et7lsK5HS8nltLwvsbtWxOlScJ2oyr2cZ9cBZi/X34MPULjYL/epX9OW60v8AMnOy3zHysjPnz1WrXpzv+YQ7OPjGvrr0px7OnTrR8n/T19QMP/sa/l974B/Y1/L73wD+xr+X3vgH9jX8vvfAP7Gv5fe+Awb4jPlZ4udc8bOa8i4Dd12JxS+h1N62hcVjoETriafWKFc6w2wqUusDGZer0LlmmpOM797jZj1WvOfRSgNzviDEyUH4teP0XLs90fIteSUbDpk5ThDhsrdAs3CNW/X65Vp3Y1bU+0hXotCvVKsYVjOMBYwAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVvuHhh44eQ8y3svUOdtpSzttGlp/wDpImUma3NO2bdOUaGkm7gn7DEtqbozhOrLpO5elGMJ1qSn5AIK/Cn8LPq9sP2g3T9cgPwp/Cz6vbD9oN0/XID8Kfws+r2w/aDdP1yA/Cn8LPq9sP2g3T9cgPwp/Cz6vbD9oN0/XID8Kfws+r2w/aDdP1yBk9O+Gj4bUqwMLKw5T+l38ZsxvZNrRZrPY4dDlCkq1uHEJKSu+KkFavZz7KHOrdqxnPr7HtYTnAXxxjCcYSnGEpTjGEpxjGMYxjHpjGMY+TGMYA8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARxza5Wi5tbbvtXOJzm2+v3uxVWHaTsjGyW21V+Hy0/Rl3j9kWpehvFz2HC8atKlLWhWlX5y05SrISOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFMviDWm10vxG6pZaRYZirWiOec1TFTsFLycFJs1P+s0SNda9UrDOWcm20PmLza3cY1bE52N9q0Z9UqzjIRh1Lx36HzrmF07fFeTfd3Xa6HUZno791I3je65XYpOqRL6yP6q45ZhjprTSjPdmnfobNNOlC2ydiV+0tWF4WGQX7uN86LAeHtE59Nf8cWjy6rW25y10YtND15SqXXucwt/uLest5HW/b67LIa5/Qzj929O5DfOVbFeqk4VgPRdlrdw8Oqb/wA/VDtPY79WqbM1hHU+e9gvO/oMdbafP2SJrsm8rLyYZfO6dbY3fLa3TdbPboYKTrVrVo9jPsKDYyBpvufWeqQ/iN53XCI6DbdVupHmzcaxSZxzZJne8rVdadt5ZFMqxEO1u9jmLrGiNkd7XDFvlDZLbfs1pRhK1YyEq+RtS6p4s8dl/JCqeQPWrtf6Htrr+6V7otl/TnLb41sE9F12cYtuft2bKJpaNDmb+cRyor5sptr041LVtzn3mAyzrfGencr4rcO0Q/kf2aR7NzWlS/SplxN3NzI8stT+qROyyWSu7uVJYtKhH1mU0MHLdnrZtGjlvjZryrdn2MgfKwdPvHkX0fifGqXcbBx+t3Lx2hvJvp0/U9rZrenlassjHwVaolTsG5u/xW96pjfv2v3upHzj3GpCdW1HqpOwI88o+WdH8e+bsrdyDvXbcwEj0bmMJf69eOjzlwcfo2c6PXMJnKhaJXeq11uWdzOxDSSb6HmWcnHyDjG3VjOM5WEz2N5d/InyF6lxeP6PceT8z4FGc93WtfNZNVbv3Qrd0eAcWeK1YuKWu95AVGvw+lKdmllnXvdutisbNmUJRjWGC2rmk9wPv/i5Yl9f670rndk6BY+ca6d0q+PLK/rt1t9Csy4a2MJfYhtKWCM1R0M5abmcmp2hqpx77UtOxfsgR/Z+/wDPepdl7BBda8qZLx9oXKLfIcyqfPaV0HXza22yagdbZNpv1oszBGLGuK2S+xbKNZ6NrfRhDVWxXtZUv3gSf4xdtjX/AHy1cNp/ec+RnN9vLc9QqlrlpeKsNxo8hE2qKq9jpFhska2ZbbK0fZsTR+wcu9fzrTr95pUteEJXkNigAAAAAAAAAAAAAAAAAAAAAAABW/y449Zu9+P155RT30FG2KzO6RvYPbK5kGcLpRWug1W2PsO3EXGTD7WrbHQe5Gr2G2zCt6kYV7KcqWkJN67UpG/co6fRIfeybS9155dalFOZPZv0xzeRsdbkodjvkNzVs8c6mWpy8SrarXp27E68ZylCs+ichXJ94w2V1xfxrhYi3xdQ7r401GmsafeWLR3O1nM3E0SNptxgn7JwmFkZWjXNqzVocY9lq6zpxr2eylSc68h6qd4z5Jd4XB1XyLmuMV/ksVNwdistQ4+u8TMj095XZFvLRkJZJO5MYXTAVLEqz0udzRvqe79ytSUZ34xjCsBeUDW9aPD7pc348eVfJWk5Rddj7n5N2LtFSeuJOfRCR1Wl+n0O6tmFic66ztftJ9EVV3Gteps2dt8OF6043qRlS0BZHy649Zu++O3SeR059BRtkuLSv6Ix7ZXMgzhNC4q2wE84y/cxcZMv9SVs4rYlHu223Odqk4z6JzlWAkPtdLlOkca63zuDcR7WavvMb7S4d1K7XGiLbSlpqsrBx7iS3s2r53pj9Lt8hW5erRu2J14zlKFq9E5CtD3xq6XXIfx8vHK7TToTu3FuQ1/kFg1WLXNPeadLqDeHhtExWJx3GtmdmZso+xReZGJe6m+dmndnOdrZXt+msI/67xHzA8lqvDxXQJLjPL4ysW2mW+NpdSmrbYVWqYr9qhXLzZcrW8runEdEsK/qkN0cxYs3Hv5HY2y53ITq9pATJe+SzT7sVk6h47dcqlJ7JrgKrCdapVji293qVxhW3zxxRV3ivxs1EWmoSWtnh0hjItd2rY5aIXrSjKcLXgIOS26xXPKLjUv5dbarcI+ekJ2u8BleXfpCJ5xROku6+9cPUWek2Bq+srqzWCCY79UXKbJaQbtFe2lOnSped2sJnkuN905ff75cvG+wczeVrqth2XK6cw64iyxsNFXhywZsZe2U20U5jKyTPdZ8R+rZINHbNxqU4wrbrWn19jATTyiJ7e1zYZbtdto8q/ltkYiCqXOoF9H1aoNWOjel6rVOz+3bZ7JITTjelW7Y5xo0ak6E406Ue2v1CYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKvdN8dJaw9GR2bknVJnjHU3EExq1ikm1dibpTbtXo3e4cRzW40qU3RqZORi1OVIaPdL5q5b6s5RhWU+z7IetqnjdbXfQ6v07vHaJHtE9z9b9zzuBY0uH5xQarKybJce8siqxGSM87nbMhnu26mzp4+2Iao2qzr1Y2YRsSFswAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//Z",
        }
        cls.charity = Charity(info)

        info = {
            "name": "John Doe",
            "image_url": "person_url goes here",
            "main_role": "Trustee",
            "main_charity_name": "Brothers Brothers Foundation",
            "total_avg_hours": 2,
            "total_comp": 5000,
        }

        cls.person = Person(info)
        cls.person.image_url = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4REQ0NzJGMDNEOEExMUUzQTg5NjlERDAzNDkzOTZCQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4REQ0NzJGMTNEOEExMUUzQTg5NjlERDAzNDkzOTZCQyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhERDQ3MkVFM0Q4QTExRTNBODk2OUREMDM0OTM5NkJDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhERDQ3MkVGM0Q4QTExRTNBODk2OUREMDM0OTM5NkJDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgBEwETAwERAAIRAQMRAf/EAHsAAQACAwACAwAAAAAAAAAAAAAHCAYJCgEFAgMEAQEAAAAAAAAAAAAAAAAAAAAAEAABBAMAAQEDBwsDBQEAAAAAAgQFBgEDBwgREhMJIRTWV5cYGfAiFdV2d7c4WCk5krUXMUEyQiMWEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDuQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMGuvT+a810s3HReh0agt5Ba9bDfdbbAVXS+2a8Yzs1s9s7IMEOV68Z9VYRlWcf8AcCPPvWeLv9SXA/th559IgH3rPF3+pLgf2w88+kQD71ni7/UlwP7YeefSIB96zxd/qS4H9sPPPpEA+9Z4u/1JcD+2Hnn0iAfes8Xf6kuB/bDzz6RAZBWPIDg92mG9epnbOR26fd4VlrB1jpFNn5hzhHp7eW8ZFTLt7uwj2sevsoz6eoEuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHMjq5Nr85PiOd/oXULdY4mNrMz06OincCpit4zheZ2vTTq9Ds0SbV2yaMstFe+24Tqzle5S1f+exS8hc5/wDBm8eYpnvkJPsnUI1g1R7x0+fuqQzZttfrhPvN7lxXdenSj2lYx6qVjHrkAw+DN48yrPRIRnZOoSTB0j3jV8wdUh4zc6/XKfeaHLeu7NO5HtJzj1SrOPXAH6/wV+EfWt1v/VTvo0A/BX4R9a3W/wDVTvo0A/BX4R9a3W/9VO+jQD8FfhH1rdb/ANVO+jQFJvO74f1B8QuV1HqVB6BfJmZf9NhqllvPKhdSGaHdctdi0yrB3Cx0Y6bP2bqsISnPqrHpsyrHs5Tj1Dfz4w3Cc6B468SulmdZfWKycxp0pNv1evvJCU3wjT57IbvXOf8A7vnCVbdnp8ntrz6Yxj0wBOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAADnu8Jv8AKt5QftD5J/xbbAVa+KN2+99B8mbpzqUkpJpROWvI6DrNV99t0x3zxUIweydkdtEqToeScq8f7PcuFJytDH3WtOfTCsqD9fwse3Xyi+TVN5nHS0k7oXT1zcRYKrsdONsXqfN4CRmI6yMWKlqbNJZi6ikI270JwrYzXtQrOfzcpDqrA8ZzhOMqVnCUpxnKlZzjGMYxj1znOc/JjGMAVp5T5d8F7T0e8cr59dmsxbKLuyhxqzhGlhZW2hKEyUlTH3vVa7HGw71WW7nbqxj0Wn3iMLbr17lhZcDUX8Z/+V2h/v8AKv8Aw86kBdfws/lL8dv3SUv/AGduBZ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAABz3eE3+Vbyg/aHyT/i22Auz5ofDYqflHaU9Nq9v/AOOek7WLRhPOXMQubgLdpjW+plFbpJvqfsHkXKsWGlDfDrVnenY31a9atPqnC8B9vhb8N2p+LNl29Kstuz0Tpn6PeRkM70RGIavVNnJasN5FcU13u379/MPGvt6FPNi9CUtt2zUnRj2lLUGzMDno+JB8RLdYd9h8eeETKtNcb7HUL0roMW49Nlj268qbyFRq7zQv8yva1YVqfvEK9X+cK06s/NcLU6DSxUrbZaHZYS406bkK5aK5IaJSFmovflu9YPW+fVG3UvHqlaFpzlGzWvCte3WpSFpUhSk5Dqw8EvO2teVNaRWLOuPrnbq5Hp2WCv61YbsrUyb4Rr2Wuqa9i8qW2WrOMvGeMq2Mdiv/AG0qQvIQz8Z/+V2h/v8AKv8Aw86kBdfws/lL8dv3SUv/AGduBZ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAABz3eE3+Vbyg/aHyT/i22A3odS6lRuMUad6L0WdbV6rV5tne8eb8+3vc71+qWkZGNE59/Iy0jv9NTdvqxnZt2ZxjGPT1zgOcOa+LP2px5FNunw7b5tyKO99At+Oud+vDOTqe91q2OX8q/1a9vu7059wjdqe68L1s1JxoSnY3zuTvCWPOL4ozTo9Oa8x8b3k3Dw9pgmu/oF0etd8NPatMm0Rsd0OG1ZV71mpunbnRKPdS1J3ZwrS2WrTlW3aGkQABkVSttlodlhLjTpuQrlorkholIWai9+W71g9b59UbdS8eqVoWnOUbNa8K17dalIWlSFKTkOgv4pFhkrd4F+PFrmV6tkxZ77yWwyuzRqS30bJKa410GSfL06EfmadSnTleUox8icfJgDYt4Wfyl+O37pKX/ALO3As6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA57vCb/Kt5QftD5J/wAW2wFUPidd16J0jyQuvOJ6WzrovKJxUJUqyx95ojde/Me02PZ+R1Z2L/SE89U5UjO9fyadGMa9SUYyvKw1vgAAAAB0I/Eh/wAc3ix+0PD/AOBd5A2ZeFn8pfjt+6Sl/wCztwLOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOafjvWaJ4z/E08lLZ22Wc0mvPrV3BrpkNsLOTSsKtt7b2et7lsK5HS8nltLwvsbtWxOlScJ2oyr2cZ9cBZi/X34MPULjYL/epX9OW60v8AMnOy3zHysjPnz1WrXpzv+YQ7OPjGvrr0px7OnTrR8n/T19QMP/sa/l974B/Y1/L73wD+xr+X3vgH9jX8vvfAP7Gv5fe+Awb4jPlZ4udc8bOa8i4Dd12JxS+h1N62hcVjoETriafWKFc6w2wqUusDGZer0LlmmpOM797jZj1WvOfRSgNzviDEyUH4teP0XLs90fIteSUbDpk5ThDhsrdAs3CNW/X65Vp3Y1bU+0hXotCvVKsYVjOMBYwAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVvuHhh44eQ8y3svUOdtpSzttGlp/wDpImUma3NO2bdOUaGkm7gn7DEtqbozhOrLpO5elGMJ1qSn5AIK/Cn8LPq9sP2g3T9cgPwp/Cz6vbD9oN0/XID8Kfws+r2w/aDdP1yA/Cn8LPq9sP2g3T9cgPwp/Cz6vbD9oN0/XID8Kfws+r2w/aDdP1yBk9O+Gj4bUqwMLKw5T+l38ZsxvZNrRZrPY4dDlCkq1uHEJKSu+KkFavZz7KHOrdqxnPr7HtYTnAXxxjCcYSnGEpTjGEpxjGMYxjHpjGMY+TGMYA8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARxza5Wi5tbbvtXOJzm2+v3uxVWHaTsjGyW21V+Hy0/Rl3j9kWpehvFz2HC8atKlLWhWlX5y05SrISOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFMviDWm10vxG6pZaRYZirWiOec1TFTsFLycFJs1P+s0SNda9UrDOWcm20PmLza3cY1bE52N9q0Z9UqzjIRh1Lx36HzrmF07fFeTfd3Xa6HUZno791I3je65XYpOqRL6yP6q45ZhjprTSjPdmnfobNNOlC2ydiV+0tWF4WGQX7uN86LAeHtE59Nf8cWjy6rW25y10YtND15SqXXucwt/uLest5HW/b67LIa5/Qzj929O5DfOVbFeqk4VgPRdlrdw8Oqb/wA/VDtPY79WqbM1hHU+e9gvO/oMdbafP2SJrsm8rLyYZfO6dbY3fLa3TdbPboYKTrVrVo9jPsKDYyBpvufWeqQ/iN53XCI6DbdVupHmzcaxSZxzZJne8rVdadt5ZFMqxEO1u9jmLrGiNkd7XDFvlDZLbfs1pRhK1YyEq+RtS6p4s8dl/JCqeQPWrtf6Htrr+6V7otl/TnLb41sE9F12cYtuft2bKJpaNDmb+cRyor5sptr041LVtzn3mAyzrfGencr4rcO0Q/kf2aR7NzWlS/SplxN3NzI8stT+qROyyWSu7uVJYtKhH1mU0MHLdnrZtGjlvjZryrdn2MgfKwdPvHkX0fifGqXcbBx+t3Lx2hvJvp0/U9rZrenlassjHwVaolTsG5u/xW96pjfv2v3upHzj3GpCdW1HqpOwI88o+WdH8e+bsrdyDvXbcwEj0bmMJf69eOjzlwcfo2c6PXMJnKhaJXeq11uWdzOxDSSb6HmWcnHyDjG3VjOM5WEz2N5d/InyF6lxeP6PceT8z4FGc93WtfNZNVbv3Qrd0eAcWeK1YuKWu95AVGvw+lKdmllnXvdutisbNmUJRjWGC2rmk9wPv/i5Yl9f670rndk6BY+ca6d0q+PLK/rt1t9Csy4a2MJfYhtKWCM1R0M5abmcmp2hqpx77UtOxfsgR/Z+/wDPepdl7BBda8qZLx9oXKLfIcyqfPaV0HXza22yagdbZNpv1oszBGLGuK2S+xbKNZ6NrfRhDVWxXtZUv3gSf4xdtjX/AHy1cNp/ec+RnN9vLc9QqlrlpeKsNxo8hE2qKq9jpFhska2ZbbK0fZsTR+wcu9fzrTr95pUteEJXkNigAAAAAAAAAAAAAAAAAAAAAAABW/y449Zu9+P155RT30FG2KzO6RvYPbK5kGcLpRWug1W2PsO3EXGTD7WrbHQe5Gr2G2zCt6kYV7KcqWkJN67UpG/co6fRIfeybS9155dalFOZPZv0xzeRsdbkodjvkNzVs8c6mWpy8SrarXp27E68ZylCs+ichXJ94w2V1xfxrhYi3xdQ7r401GmsafeWLR3O1nM3E0SNptxgn7JwmFkZWjXNqzVocY9lq6zpxr2eylSc68h6qd4z5Jd4XB1XyLmuMV/ksVNwdistQ4+u8TMj095XZFvLRkJZJO5MYXTAVLEqz0udzRvqe79ytSUZ34xjCsBeUDW9aPD7pc348eVfJWk5Rddj7n5N2LtFSeuJOfRCR1Wl+n0O6tmFic66ztftJ9EVV3Gteps2dt8OF6043qRlS0BZHy649Zu++O3SeR059BRtkuLSv6Ix7ZXMgzhNC4q2wE84y/cxcZMv9SVs4rYlHu223Odqk4z6JzlWAkPtdLlOkca63zuDcR7WavvMb7S4d1K7XGiLbSlpqsrBx7iS3s2r53pj9Lt8hW5erRu2J14zlKFq9E5CtD3xq6XXIfx8vHK7TToTu3FuQ1/kFg1WLXNPeadLqDeHhtExWJx3GtmdmZso+xReZGJe6m+dmndnOdrZXt+msI/67xHzA8lqvDxXQJLjPL4ysW2mW+NpdSmrbYVWqYr9qhXLzZcrW8runEdEsK/qkN0cxYs3Hv5HY2y53ITq9pATJe+SzT7sVk6h47dcqlJ7JrgKrCdapVji293qVxhW3zxxRV3ivxs1EWmoSWtnh0hjItd2rY5aIXrSjKcLXgIOS26xXPKLjUv5dbarcI+ekJ2u8BleXfpCJ5xROku6+9cPUWek2Bq+srqzWCCY79UXKbJaQbtFe2lOnSped2sJnkuN905ff75cvG+wczeVrqth2XK6cw64iyxsNFXhywZsZe2U20U5jKyTPdZ8R+rZINHbNxqU4wrbrWn19jATTyiJ7e1zYZbtdto8q/ltkYiCqXOoF9H1aoNWOjel6rVOz+3bZ7JITTjelW7Y5xo0ak6E406Ue2v1CYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKvdN8dJaw9GR2bknVJnjHU3EExq1ikm1dibpTbtXo3e4cRzW40qU3RqZORi1OVIaPdL5q5b6s5RhWU+z7IetqnjdbXfQ6v07vHaJHtE9z9b9zzuBY0uH5xQarKybJce8siqxGSM87nbMhnu26mzp4+2Iao2qzr1Y2YRsSFswAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//Z"

        info = {
            "abbr": "USA",
            "name": "Murica!",
            "population": 1,
            "gdp_per_capita": 20000.0,
            "foreign_investment_pct": 1.0,
            "research_and_development": 1.0,
            "vc_availability": 1.0,
            "inflation_pct": 1.0,
            "unemployment_pct": 1.0,
            "gender_gap_index": 0.0,
            "life_exp_rating": 1.0,
        }
        cls.country = Country(info)
        cls.country.charities.append(cls.charity)

        charity_people = CharitiesPeopleAssociation(
            cls.person, cls.charity, "Associate", 100, 200, 300, 40
        )
        cls.person.charities_assoc.append(charity_people)
        cls.charity.countries.append(cls.country)

        cls.test_client = application.test_client()

    def check_types_deep(self, expected, obj):
        for key, val in expected.items():
            # print("Checking if object has key %s" %(key))
            self.assertIn(key, obj, msg="%s not in object" % key)
            if type(val) == type or (type(val) == tuple and type(val[0]) == type):
                # print("Checking if %s is of type %s" %(key, val))
                self.assertTrue(
                    isinstance(obj[key], val),
                    msg="%s is of type %s but %s was expected"
                    % (key, type(obj[key]), val),
                )
            elif type(val) == dict:
                # print("Checking if %s is of type %s" %(key, dict))
                self.assertTrue(
                    isinstance(obj[key], dict),
                    msg="%s is of type %s but dict was expected"
                    % (key, type(obj[key])),
                )
                self.check_types_deep(val, obj[key])
            elif hasattr(val, "__iter__"):
                # print("Checking if %s is iterable" %(key))
                self.assertTrue(
                    hasattr(obj[key], "__iter__"), msg="%s is not iterable" % key
                )
                for i, j in zip(val, obj[key]):
                    self.check_types_deep(i, j)

    def test_charity_get_attributes(self):
        attributes = SchemaTests.charity.get_attributes()
        self.check_types_deep(charity_schema_simple, attributes)

    def test_charity_get_complete_attributes(self):
        attributes = SchemaTests.charity.get_complete_attributes()
        self.check_types_deep(charity_schema_complete, attributes)

    def test_country_get_attributes(self):
        attributes = SchemaTests.country.get_attributes()
        self.check_types_deep(country_schema_simple, attributes)

    def test_country_get_complete_attributes(self):
        attributes = SchemaTests.country.get_complete_attributes()
        self.check_types_deep(country_schema_complete, attributes)

    def test_people_get_attributes(self):
        attributes = SchemaTests.person.get_attributes()
        self.check_types_deep(person_schema_simple, attributes)

    def test_people_get_complete_attributes(self):
        attributes = SchemaTests.person.get_complete_attributes()
        self.check_types_deep(person_schema_complete, attributes)

    def test_get_person(self):
        id = str(db.session.query(Person.id).first()[0])
        self.check_types_deep(person_schema_complete, json.loads(get_person(id)))

    def test_person_id_endpoint(self):
        result = get_person("6805")
        result = json.loads(result)
        self.assertTrue("person" in result)
        self.assertTrue(
            "name" in result["person"] and result["person"]["name"] == "Boris Lushniak"
        )
        self.assertTrue("countries" in result["person"])
        self.assertTrue("charities" in result["person"])
        self.assertTrue("news" not in result)

    def test_get_people(self):
        expected = {"people": repeat(person_schema_simple), "num_pages": int}
        with application.test_request_context():
            response = get_people()

            self.check_types_deep(expected, json.loads(response))

    def test_get_charity(self):
        ein = str(db.session.query(Charity.ein).first()[0])
        self.check_types_deep(charity_schema_complete, json.loads(get_charity(ein)))

    def test_charity_ein_endpoint(self):
        result = get_charity("10518193")
        result = json.loads(result)
        self.assertTrue("charity" in result)
        self.assertTrue(
            "name" in result["charity"] and result["charity"]["name"] == "Acadia Center"
        )
        self.assertTrue("countries" in result["charity"])
        self.assertTrue("people" in result["charity"])
        self.assertTrue("news" in result)

    def test_get_charities(self):
        expected = {"charities": repeat(charity_schema_simple), "num_pages": int}
        with application.test_request_context():
            self.check_types_deep(expected, json.loads(get_charities()))

    def test_get_country_by_abbr(self):
        abbr = db.session.query(Country.abbr).first()[0]
        self.check_types_deep(
            country_schema_complete, json.loads(get_country_by_abbr(abbr))
        )

    def test_get_country_by_id(self):
        result = get_country_by_id(1)
        result = json.loads(result)
        self.assertTrue("country" in result)
        self.assertTrue(
            "name" in result["country"] and result["country"]["name"] == "Afghanistan"
        )
        self.assertTrue("charities" in result["country"])
        self.assertTrue("people" in result["country"])
        self.assertTrue("news" in result)

    def test_get_countries(self):
        expected = {"countries": repeat(country_schema_simple), "num_pages": int}
        with application.test_request_context():
            self.check_types_deep(expected, json.loads(get_countries()))

    def test_news_endpoint(self):
        result = news_helper("hello")
        # check status and that articles
        self.assertEqual(result["status"], "ok")
        self.assertTrue(type(result["articles"]), "list")

    def test_get_text_portion(self):
        blurb = "hi it is monday and i am very very very sleepy, but I love swe so it is all good right?"
        expected = (
            "...and i am very very very sleepy, but I love swe so it is all good right?"
        )
        self.assertEqual(expected, getTextPortion(blurb, "swe"))

    def test_get_charity_states(self):
        expected = '{"states": ["AZ", "CA", "CO", "CT", "DC", "FL", "GA", "IA", "ID", "IL", "IN", "KS", "KY", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "NE", "NH", "NJ", "NY", "OH", "OK", "OR", "PA", "SC", "TN", "TX", "UT", "VA", "VT", "WA", "WI"]}'
        self.assertEqual(expected, get_all_states())

    def test_get_charity_classifications(self):
        expected = '{"irs_class_names": ["Charitable Organization", "Educational Organization", "Organization for Public Safety Testing", "Religious Organization", "Scientific Organization"]}'
        self.assertEqual(expected, get_all_classifications())

    def test_get_people_roles(self):
        expected = '{"main_roles": ["Director", "Board Member", "Trustee", "Treasurer", "Secretary", "Member", "President", "Vice President", "Chairman", "Vice Chairman", "Chair", "Executive Trustee", "Chief Financial Officer", "Executive Director", "Chief Executive Officer", "Unique"]}'


if __name__ == "__main__":
    unittest.main()
