from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from charities.models import Charity
from application import db
from flask_script import Command


class CharityImageScraper(Command):
    def run(self):
        charities_list = Charity.query.order_by(Charity.id.asc())
        browser = webdriver.Chrome()
        for charity in charities_list:
            if charity.image_url != "https://www.gstatic.com/kpui/social/fb_32x32.png":
                continue
            name = charity.name.replace(" ", "+")
            url = (
                "https://www.google.com/search?q=" + name + "+logo&source=lnms&tbm=isch"
            )
            browser.get(url)
            try:
                images = browser.find_elements_by_tag_name("img")
                header = False
                for image in images:
                    if (
                        image.get_attribute("src") is not None
                        and image.get_attribute("src")[0:4] == "data"
                    ):
                        charity.image_url = image.get_attribute("src")
                        print(image.get_attribute("src"))
                        db.session.commit()
                        break
                    # if image.get_attribute('src') is None:
                    #     header = True
                    # if header == True and image.get_attribute('src') is not None:
                    #     charity.image_url = image.get_attribute('src')
                    #     db.session.commit()
                    #     break
            except TimeoutException:
                print("probelms")
