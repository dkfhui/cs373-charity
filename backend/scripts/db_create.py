from application import db
from charities.models import Charity
from countries.models import Country
from people.models import Person
from flask_script import Command


class DBTableCreate(Command):
    def run(self):
        db.create_all()
        print("DB created.")
