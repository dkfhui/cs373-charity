from flask_script import Command
from application import db
from charities.models import Charity
from people.models import Person
from people.models import CharitiesPeopleAssociation
from lxml import html
import json
import requests
import xml.etree.ElementTree as ET

from sqlalchemy import func

import codecs


class PeopleScraper(Command):
    def run(self):
        print("Getting IRS-form-990 json...")
        URL = "https://s3.amazonaws.com/irs-form-990/index_2018.json"
        res = requests.get(URL)

        index = res.json()["Filings2018"]  # May fail

        # Each element of index is a dict made up from the following keys:
        # ['EIN', 'TaxPeriod', 'DLN', 'FormType', 'URL', 'OrganizationName', 'SubmittedOn', 'ObjectId', 'LastUpdated']
        # The values are all strings

        charities_count = db.session.query(func.count(Charity.id)).scalar()
        print("%d charities in table" % charities_count)

        print("Generating form index...")
        progress_str = "Validating charities [%%d/%d]" % charities_count
        forms = []
        count = 0
        for i, ein in enumerate(x[0] for x in db.session.query(Charity.ein).all()):
            print(progress_str % i)
            print("Valid charities: %d" % count)

            for j, obj in enumerate(index):
                if int(obj["EIN"]) == ein and obj["FormType"] == "990":
                    # 'FormType' is either '990', '990EZ', or '990PF'
                    # Only know how to parse 990
                    forms.append(j)
                    count += 1

        def index_with(indices, what):
            for i in indices:
                yield what[i]

        def follow_link(form):
            req = requests.get(form["URL"])
            req.encoding = "utf-8-sig"

            # print(req.text)

            return ET.fromstring(req.text)

        irs_namespace = {"irs": "http://www.irs.gov/efile"}

        def extract_data(part_vii_section_as):
            nonlocal irs_namespace

            def safe_tostring(element, default=None):
                if element == None:
                    return default
                return ET.tostring(element, method="text", encoding="unicode")

            for xml in part_vii_section_as:
                name = safe_tostring(
                    xml.find("irs:PersonNm", irs_namespace), "ANONYMOUS"
                ).strip()
                role = safe_tostring(
                    xml.find("irs:TitleTxt", irs_namespace), "UNDEFINED"
                ).strip()
                average_hours = safe_tostring(
                    xml.find("irs:AverageHoursPerWeekRt", irs_namespace), 0
                )
                org_comp = safe_tostring(
                    xml.find("irs:ReportableCompFromOrgAmt", irs_namespace), 0
                )
                rltd_org_comp = safe_tostring(
                    xml.find("irs:ReportableCompFromRltdOrgAmt", irs_namespace), 0
                )
                other_comp = safe_tostring(
                    xml.find("irs:OtherCompensationAmt", irs_namespace), 0
                )
                yield (name, role, average_hours, org_comp, rltd_org_comp, other_comp)

        progress_str = "Getting form %%s [%%d/%d]" % len(forms)
        for i, form in enumerate(index_with(forms, index)):
            tree = follow_link(form)
            print(progress_str % (form["URL"], i))
            part_vii_section_as = tree.findall(
                "./irs:ReturnData/irs:IRS990/irs:Form990PartVIISectionAGrp",
                irs_namespace,
            )

            print(len(part_vii_section_as))

            for (
                name,
                role,
                average_hours,
                org_comp,
                rltd_org_comp,
                other_comp,
            ) in extract_data(part_vii_section_as):

                person = (
                    db.session.query(Person).filter(Person.name == name).one_or_none()
                )

                if not person:
                    person = Person(name)

                ca = CharitiesPeopleAssociation(
                    person,
                    db.session.query(Charity)
                    .filter(Charity.ein == int(form["EIN"]))
                    .one(),
                    role,
                    int(float(average_hours) * 100),
                    int(org_comp),
                    int(rltd_org_comp),
                    int(other_comp),
                )

                """
                def __init__(self, person, charity, role, from_org_comp, from_rltd_org_comp, other_comp, avg_hours):
                    self.person = person
                    self.charity = charity
                    self.role = role
                    self.from_org_comp = from_org_comp
                    self.from_rltd_org_comp = from_rltd_org_comp
                    self.other_comp = other_comp
                    self.avg_hours = avg_hours
                """
                person.charities_assoc.append(ca)

                print("Commiting...")
                db.session.add(person)
                db.session.commit()
