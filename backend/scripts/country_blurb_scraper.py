from countries.models import Country
from application import db
from flask_script import Command
import requests, json


class CountryBlurbScraper(Command):
    def run(self):
        countries = Country.query.all()
        for country in countries:
            searchTerm = country.name.replace(" ", "_")
            url = "https://en.wikipedia.org/api/rest_v1/page/summary/" + searchTerm
            page = requests.get(url)
            try:
                json_res = json.loads(page.text)
                country.blurb = json_res["extract"]
                print(country.blurb)
                db.session.commit()
            except:
                print("No info for", country.name)
        db.session.close()
