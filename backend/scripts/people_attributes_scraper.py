from people.models import Person
from flask_script import Command
from application import db


class PeopleAttributesScraper(Command):
    def run(self):
        person_list = Person.query
        for person in person_list:
            print(person.name)
            person.main_charity_name = person.mai_charity.name
            # person.total_avg_hours = person.tota_avg_hours
            # person.total_comp = person.tota_compensation
            # #change names to  camel case
            # name_portions = person.name.split()
            # new_name = ''
            # for portion in name_portions:
            #     new_name += portion.capitalize()  + ' '
            # new_name = new_name[0: len(new_name) - 1]
            # person.name = (new_name)
        db.session.commit()
