from people.models import Person, CharitiesPeopleAssociation
from application import db
from flask_script import Command


class PeopleImageScraper(Command):
    def run(self):
        assoc_list = CharitiesPeopleAssociation.query.order_by(
            CharitiesPeopleAssociation.id.asc()
        )
        for assoc in assoc_list:
            role = assoc.role.lower()
            print(role)
            url = ""
            if "president" in role:
                url = "https://i.pinimg.com/originals/2e/b6/11/2eb61101218ca44796814bcb2cd8fd90.jpg"
            elif "director" in role:
                url = "http://jarvisbuckman.com/wp-content/uploads/jarvisbuckman-com/sites/1141/Jarvis-Buckman-Leader-vs-Manager-927x675.jpg"
            elif "treasur" in role:
                url = (
                    "http://www.parkcounty.org/uploads/pics/departments/35/Money-3.jpg"
                )
            elif "ceo" in role or "chief" in role:
                url = "https://banner2.kisspng.com/20180707/rfb/kisspng-logo-brand-organization-ceo-5b414bb073faa5.5476068115310058724751.jpg"
            elif "board" in role:
                url = "https://static1.squarespace.com/static/5306da2de4b0ba3094b60642/t/54529e7ae4b0fd4cc64bab75/1414700667235/meeting"
            elif "secretary" in role:
                url = "http://www.animatedimages.org/data/media/1817/animated-secretary-image-0050.gif"
            elif "v" in role:
                url = (
                    "http://hrringleader.com/wp-content/uploads/2010/07/leader-logo.jpg"
                )
            else:
                url = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX///8AAAD19fX8/Pzt7e3h4eHb29v29vZYWFje3t75+fnY2Njy8vLo6OhhYWHT09O8vLysrKxLS0vGxsbMzMxnZ2ebm5uJiYl3d3eAgIC0tLQuLi4JCQmTk5MRERFTU1MdHR2fn59tbW0yMjI5OTkmJiZFRUUxMTF8fHwaGhpwDrDiAAAKKUlEQVR4nO2daZerKBCGMWgWo4lZzZ6Y7Xb//z84KriCAhm7C/vwfJm5xs7x7QJqobARMhgMBoPBYDAYDAaDwWAwGAwGzcAIz7fhexNGQ5z86w8SPC3K42xDP8wPMLpbZc7eXzOjf7SqPNbQj9Qt+GIxbKEfqlNcbzBZzKoKT39wMvrbssIr9ON0T7y0eLtC4aLyyd8hOFGBFzpI7XDb/+GKi/9i5FC3uCfXhskiO5733Y7eMGaCExEYDcapQj/9xKZeZNZn5+EFs39EhU2sOBpnJsT4nU/Lnd1TM9rhLRex9Mg1/zszYVhaW4/9NOOi4gLfXjpS0dT6Sg02r3rIdf+suDhVJVhPN7mM0YqY8G3VJfaL1c1ieJfNFDEfj+CeVp1V3UB0QSn8gsf+Bnagj6wCno65+hIRZCrGOkPOpy7wg0sRC5hvmvQVVsR2fZJaySDuBc6OfXTWitgN2I8O0M8uJH52X6AvJqJ3O/Ws8ZV9zzCIcbQM5fBZqM/K0or4t8E34Zwqf04BlfAZRTL6rCJz8paVy+TipLhwxlpZEQccByiwol0eqHT0lmsBX4B66riHZ12GjBWdV37pMkivzLn3wbNe1jUIIBEaRuu6mKqnOQ4ARZVYNzp4scSseHMhmUctGreGkLoynHv96ZUkUuuH5MvqwcIcUBjF/0xfjEO+gI5Tkm7UTahBWdX5VF/s76gvSM3WYEINbHj9XGHmHtLwjRh0ytwzAdSWwoyqGhtOfJ3zJN8xSu4j/8uG7D60zxdMwj1atX1MK6UnOhgxa8KlC6wQP9oVrtt/B1ThJTZhKmTG3HEmd/hzB0ih3zYIYx8XZ7T2t0ihnRWhhuwdJPhexMHdcgWiUDAN0wVy0fw5URhlE5I14TU1LR3pIM6/5ekTyFLPr9jEHJNPMRpbxDwcE65RKSZ46qfwSMouk3fDWCbefBI/esMsJDtw+WIFUQJoV3jO7xsG4fLFfE6Kh2dqQnYhpc4wHwPL3xfYqvBec9butCYzIJcfN+IQWBOSFbbIPiDcf7PCO39d8IbBfnwsLIzjryCDj7NoOekNpWkMsJw2Kby2Fug957Af08KpdzwSp86GMyTOKZmwNOx/DU5FMBUo/w1nmvlyTEiyjXJqDaCwwR8eV1Nf9gvodjdrwnt6vRL1ASh0WmOa5XIZlZVOmRjTe9Gn5iykSf0C40p1BGBzymNdQN2cRVl3Ylmv+3ZlEy+ZTEP7RkyIOSYkyquBO0Rw2hivUE50sCVk1eLTJlxMPYy9Qy6EY8J08OJK+rkBECjKnhIW1IpVe5+u17S8Slwca8IkpMUamFAUmKbQQgQ3UyQ7hmxEeknmL66a8AEhsOKtmniR1XLEs3cqhBPOkLJNdfDCFKWkClHEPWLMStxxhCScyBJc/QGgopTUXkXmx5idKbL8s/VkYsLarw9os+2fjEIaZOO6RJIssGHDjTiUqglDEH1Si2mCTyVWGjDTccebhSSQq5kQqnLK6zfgcCQKq+s/2TvjmBBxYvEjkEB0kFOYNZLgUrBOswvWF3JNGDU+wg8jKgnnHLKFYkV2RS8R+TcnnCkV+wvAuonk9y3oPMJocAjD8JD1IrImjHilxT2UQISkFR493o+zJrxwldMc5fcdBkaCqneJMe8LmmZhTTk1IUhvn8LmEyeBZafxi1vUsNOLNshYlYm9MwLmp9n+G25pkUxNtAVZUFUUWsxUZFLoJZlp+8rFK91LfYLsXbRun9U5MktF3Z8SMw8qs/tBl5kAJkO0VRQWHaQjYg5cC/uoCZEd7PMJvssOZDxf3OX4h8Hy7iIly/hHR9rv5B2tUjmrPAxHziTGyV39wpr9prIMjOoH7wTQdrxYGE0W7NJUbOvycoHSC16ZrBXa5RQrzJZWL8sPr63pwxIsvZDMLnJI7SlpEb7QuiJaBUGwmrTHK/HqCjINkaK7sLKplsazrH9sJHEfY6AsX8ld5ApTL9Ew7aL9qropMCCNc1BN0mrugoxSTFz6P+73JTn/abk7r9eO7/vz9XZDSyVQ/RiemsDv1DxuugLfyDcUYy/+v0WptnU6nUottiD7+OlTcY4xt0BKi7TOerlet4t1aXYFLX24cJ38as2JZ87PXA+JYb31ubU2CbSSonqULCL1aZ90NO7gDidsxU9XkJRI8UcdjYB9mDJ7FznpZFJ1oQlg6wziVssaeaj+QI5CdNA5Ku4iiIeoJ1/aKfiG7MIcyWcXyeE0R7RvzAWsIJyA5deNOHVatTdsNvCAPZko7y5c9+sTfSDdUGWkDqyl3NTinxzo14Q0dEZ1CPSZBE7ra7eAn4Cun5Tsmhv4208Gnzg4BTQ4HvzB0TUFQGqINRTLbYrocEhfKbtQRYMx+lmuIMtGizPrHyULcnzr8WaeH3SImrzRBSud41YBqhGKYSJ+1o946vPGGm8RXscZs5bjamro896I2tsdulp5FlqsMjwExxJlAQ+4G8CY9Y9HP1J96QJv018b2N2oZJPJiRSrpFAbMVxKv+zBnLNlSt954W8VtsQhy4cs7n1GeXA15NuB8qss/MsUKgh6MF+S95XY6fXyHSR4OVR+2EU6CXlDn8OvgkXHn/JyruwgPY60EigutmXnSWUj9O+Jbo5CtEc6oM8byQm8gL/uo46o1pa3zkqmINoJFPbrZymQ3M7vaajbEEVYFJBlmw5SxY6TFm+GqiI0Tebv2cMxLPotMki8frxpGiuTcbz1SXkLRO+oyZMgieM1+3qmqQXCCgbx97wTXDVeegXbOcLdw2n8W7hH4pg0hGsJagMj0abnt4/juPUu6KF5brVKB8sImy+T/duXFbQE3Qfbt3XYnGhAuIn/lcajTtR8h47rZ4ErEpjsrMSuvm2d0eT9lg2I9/DnSRP6u62tSG+Fws3Rm50EPcu2fnetFYq38N/izF5rheJg+i5+PYjWCsV13kgcmeusUGKDYo2iPiuU6PfyxX0MGisU5r5JqZT3JwJ6o1Ci//kt4TE1VihxqOs8FOe92irEA4kXm2wkXu+irULlM139Uyj+UxY9V+h2s5etscLOer00VShRWeq7QsVzh/1TiKI/r1DuLV89VthhK5umClWP4fdNIVboG+mnQrVjlb1U2FXEpqtC7Apfr9tvhbjbw1waKuz4DImOCtv+LMffUNjtERIdFXZ7lEtDhR0fONRQYccnufRT6H50or5PCrs+5qSfwg7TCi0VYrdTZ6ihQuTL/0XVnirsfJjqpxCjofoZn14pTDgovhCyfwrRaNvZdNRUYZxgRB1p1FRh0urqRZ1szmiqMAGjQRdjVWOFKcH/Xld1V4iQE/4/Q+qvMB6u6/vHIp97jbtny3jr8IPX1CzDKUY9URgzmCw2CnXU5+6gbW83H/L3DJ3Fl3jpuVzDgz9izvH3Bnc0DM67GddXPmbhduXrdoBSkeLp7YkTLHJWzsSr32EwGAwGg8FgMBgMBoPBYDAYDAaDwWAw9IX/AAL/hN8gBQ43AAAAAElFTkSuQmCC"
            assoc.person.image_url = url
        db.session.commit()
