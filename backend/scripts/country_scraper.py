from flask_script import Command
from application import db
from countries.models import Country
import json
import requests


class CountryScraper(Command):
    def run(self):
        indicators = {
            "352": "population",
            "944": "gdp_per_capita",
            "27962": "gender_gap_index",
            "626": "life_exp_rating",
            "1541": "foreign_investment_pct",
            "2013": "research_and_development",
            "3385": "vc_availability",
            "24785": "transparency_index",
            "1035": "inflation_pct",
            "3294": "unemployment_pct",
        }
        # indicators = {'3294':'unemployment_pct'}
        page = requests.get("https://tcdata360-backend.worldbank.org/api/v1/countries/")
        json_res = json.loads(page.text)
        countries_dict = {}
        for obj in json_res:
            data = {}
            data["name"] = obj["name"]  # .encode('ascii', 'replace')
            data["abbr"] = obj["iso3"]  # .encode('ascii', 'replace')
            countries_dict[data["abbr"]] = data

        # print("---------------------------------------")
        # print('initial dict')
        # print(countries_dict)

        # now get the indicators for each country
        for indicator, desc in indicators.items():
            url = (
                "https://tcdata360-backend.worldbank.org/api/v1/data?indicators="
                + indicator
            )
            indicator_page = requests.get(url)
            json_res = json.loads(indicator_page.text)

            # self.get_indicator_data2(json_res['data'], desc)

            if indicator == "626":
                # life expectancy has to be treated differently
                self.get_life_expectancies(countries_dict, json_res["data"], desc)
            else:
                self.get_indicator_data(countries_dict, json_res["data"], desc)

        # print("---------------------------------------")
        # print('final dict')
        # for country in countries_dict:
        #     print(countries_dict[country])
        #     print("")
        #     print("---------------------------------------------------------------------------------------------------------------------")
        # insert countries into database
        # try:
        for country in countries_dict:
            print(countries_dict[country])
            country_obj = Country(countries_dict[country])
            print(country_obj)
            db.session.add(country_obj)
            print("added", country)
        db.session.commit()
        db.session.close()
        # except:
        #     db.session.rollback()

    def get_life_expectancies(self, countries_dict, json_data, desc):
        for country in json_data:
            id = country["id"]
            if id in countries_dict:
                # pull the most recent available value since 2018
                year = 2018
                prev_year = 2017
                values = country["indicators"][0]["values"]
                while prev_year >= 2000:
                    year_str = str(prev_year) + "-" + str(year)
                    if year_str in values:
                        countries_dict[id][desc] = values[year_str]
                        break
                    year -= 1
                    prev_year -= 1
                else:
                    print("Not able to find data")

    def get_indicator_data(self, countries_dict, json_data, desc):
        print("----------------called for " + desc)
        for country in json_data:
            id = country["id"]
            print(id + " ", end="")
            if id in countries_dict:
                # pull the most recent available value since 2018
                year = 2018
                values = country["indicators"][0]["values"]
                while year >= 2000:
                    year_str = "" + str(year)
                    if year_str in values:
                        countries_dict[id][desc] = values[year_str]
                        print(values[year_str], end="\n")
                        break
                    year -= 1
                else:
                    print("Not able to find data")

    def get_indicator_data2(self, json_data, desc):
        print("----------------called for " + desc)
        for country in json_data:
            id = country["id"]
            print(id + " ", end="")

            try:
                res = Country.query.filter(Country.abbr == id).first()
                # print(res)
                # print(type(res))

                year = 2018
                values = country["indicators"][0]["values"]
                while year >= 2000:
                    year_str = "" + str(year)
                    if year_str in values:
                        setattr(res, desc, values[year_str])
                        # res[desc] = values[year_str]
                        db.session.add(res)
                        print(values[year_str], end="\n")
                        break
                    year -= 1
                else:
                    print("Not able to find data")
            except:
                db.session.rollback()
