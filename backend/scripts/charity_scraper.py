from flask_script import Command
from application import db
from charities.models import Charity
from countries.models import Country
from lxml import html
import json
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class CharityScraper(Command):
    def run(self):
        country_page = requests.get(
            "https://www.charitynavigator.org/index.cfm?bay=search.map#Afghanistan"
        )
        tree = html.fromstring(country_page.content)
        countries = tree.xpath('//div[@class="country-item"]/a/text()')
        # iterate over each country and get add to map of charities
        charities_map = {}
        for country in countries:
            path = (
                '//div[@id="'
                + country
                + '"]/div[@class="display-items"]/div[@class="org-item"]/a/text()'
            )
            charities_map[country] = tree.xpath(path)
        # now visit each charity and collect info
        visited_charities = set()
        charities_info = []
        for country in charities_map.keys():
            country_obj = Country.query.filter(Country.name == country).first()
            if country_obj is None:
                continue
            print(country_obj)
            for charity in charities_map[country]:
                # only pull data for the charity once
                charity_obj = Charity.query.filter(Charity.name == charity).first()
                if charity_obj is None:
                    visited_charities.add(charity)
                    url = (
                        "https://api.data.charitynavigator.org/v2/Organizations/?app_id=5562f0c9&app_key=03823a48dfdb6947eadac6dcd00c63f7&searchType=NAME_ONLY&search="
                        + charity
                    )
                    response = requests.get(url)
                    data = json.loads(response.text)
                    for entry in data:
                        # get the charity with the exact name
                        if entry["charityName"] == charity:
                            info = {}
                            info["ein"] = entry["ein"]
                            info["name"] = entry["charityName"]
                            info["tag_line"] = (
                                entry["tagLine"] if "tagLine" in entry else None
                            )
                            info["mission"] = entry["mission"]
                            info["address"] = (
                                entry["mailingAddress"]["streetAddress1"]
                                + ", "
                                + entry["mailingAddress"]["city"]
                            )
                            info["state"] = entry["mailingAddress"]["stateOrProvince"]
                            info["income"] = entry["irsClassification"]["incomeAmount"]
                            info["assets"] = entry["irsClassification"]["assetAmount"]
                            info["class"] = entry["irsClassification"]["classification"]
                            info["image_url"] = "blank right now"
                            # try:
                            print(info)
                            charity_obj2 = Charity(info)
                            db.session.add(charity_obj2)
                            charity_obj2.countries.append(country_obj)
                            db.session.commit()
                            print("COMMITED")
                            break
                else:
                    print(charity)
                    charity_obj.countries.append(country_obj)

        # add each charity to the database
        try:
            db.session.commit()
            print("COMMITED")
            db.session.close()
        except:
            db.session.rollback()
