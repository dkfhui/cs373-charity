import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
import sys


class SearchText(unittest.TestCase):
    def setUp(self):
        # create a new Firefox session
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("localhost:8080")

    def test_navbar_links(self):
        navigation_links = [
            "/charities_list",
            "/persons_list",
            "/countries_list",
            "/about",
        ]
        links = self.driver.find_elements_by_xpath('//li[@class="nav-item"]/a')
        index = 0
        for link in links:
            href = link.get_attribute("href")
            url = "http://localhost:8080" + navigation_links[index]
            self.assertEqual(url, href)
            index += 1

    def test_about_link(self):
        self.driver.get("localhost:8080")
        # should be second link in navbar
        link = self.driver.find_elements_by_xpath('//li[@class="nav-item"]/a')[3]
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:8080/about")
        picture_cards = self.driver.find_elements_by_xpath('//div[@class="card"]')
        self.assertEqual(len(picture_cards), 5)

    def test_charity_instance_link(self):
        self.driver.get("localhost:8080")
        # should be third link in navbar
        link = self.driver.find_elements_by_xpath('//li[@class="nav-item"]/a')[0]
        link.click()
        self.assertEqual(
            self.driver.current_url, "http://localhost:8080/charities_list"
        )
        self.driver.implicitly_wait(30)
        firstCard = self.driver.find_elements_by_xpath('//div[@class="card-body"]/a')[0]
        firstCard.click()
        self.assertTrue(
            "http://localhost:8080/charity_instance/" in self.driver.current_url
        )

    def test_people_instance_link(self):
        self.driver.get("localhost:8080")
        # should be third link in navbar
        link = self.driver.find_elements_by_xpath('//li[@class="nav-item"]/a')[1]
        link.click()
        self.assertEqual(self.driver.current_url, "http://localhost:8080/persons_list")
        self.driver.implicitly_wait(30)
        firstCard = self.driver.find_elements_by_xpath('//div[@class="card-body"]/a')[0]
        firstCard.click()
        self.assertTrue(
            "http://localhost:8080/person_instance/" in self.driver.current_url
        )

    def test_country_instance_link(self):
        self.driver.get("localhost:8080")
        # should be fourth link in navbar
        link = self.driver.find_elements_by_xpath('//li[@class="nav-item"]/a')[2]
        link.click()
        self.assertEqual(
            self.driver.current_url, "http://localhost:8080/countries_list"
        )
        self.driver.implicitly_wait(30)
        firstCard = self.driver.find_elements_by_xpath('//div[@class="card-body"]/a')[0]
        firstCard.click()
        self.assertTrue(
            "http://localhost:8080/country_instance/" in self.driver.current_url
        )

    def test_charity_instance_content(self):
        # page for Acadia Center
        self.driver.get("localhost:8080/charity_instance/10518193")
        # wait for content to load
        time.sleep(2)
        name_text = self.driver.find_elements_by_tag_name("h2")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(name_text, " Acadia Center ")
        tag_text = self.driver.find_elements_by_tag_name("h1")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(tag_text, ' "Advancing the Clean Energy Future" ')
        irs_text = self.driver.find_elements_by_tag_name("h3")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(irs_text, " IRS Information ")
        irs_labels = self.driver.find_elements_by_xpath("//dt/p")
        required_labels = ["Classification", "Address", "State", "Assets", "Income"]
        index = 0
        for label in irs_labels:
            self.assertEqual(label.get_attribute("innerHTML"), required_labels[index])
            index += 1

    def test_charity_instace_links(self):
        # check that links exist to country and people pages
        # Test persons
        self.driver.get("localhost:8080/charity_instance/10518193")
        frames = self.driver.find_elements_by_xpath('//div[@class="portrait"]')
        frames[0].click()
        self.assertTrue(
            "http://localhost:8080/person_instance" in self.driver.current_url
        )
        # Test country
        self.driver.get("localhost:8080/charity_instance/10518193")
        paths = self.driver.find_elements_by_tag_name("path")
        for path in paths:
            # only Canada should exist
            if path.get_attribute("data-tip"):
                path.click()
                self.assertEqual(
                    self.driver.current_url,
                    "http://localhost:8080/country_instance/CAN",
                )
                break

    def test_country_instance_content(self):
        # page for AFGHANISTAN
        self.driver.get("http://localhost:8080/country_instance/AFG")
        time.sleep(4)
        name_text = self.driver.find_elements_by_tag_name("h2")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(name_text, " Afghanistan (AFG)")
        indicator_headers = self.driver.find_elements_by_tag_name("h3")
        self.assertEqual(
            indicator_headers[0].get_attribute("innerHTML"), " Life Quality Indicators"
        )
        self.assertEqual(
            indicator_headers[1].get_attribute("innerHTML"), " Economic Indicators "
        )
        labels = self.driver.find_elements_by_xpath("//dt/p")
        required_labels = [
            "Population",
            "GDP per Capita (USD)",
            "Gender Gap Index",
            "Life Expectancy Ranking",
            "Inflation (annual %)",
            "Foreign Investment (% of GDP)",
            "Unemployment (% of total labor force)",
            "Venture Capital Availability (1-7 Higher is better)",
            "Research and Development Expenditure (% of GDP)",
        ]
        index = 0
        for label in labels:
            self.assertEqual(label.get_attribute("innerHTML"), required_labels[index])
            index += 1

    def test_country_instace_links(self):
        # check that links exist to country and people pages
        # Test persons
        self.driver.get("http://localhost:8080/country_instance/AFG")
        frames = self.driver.find_elements_by_xpath('//div[@class="portrait"]')
        frames[0].click()
        self.assertTrue(
            "http://localhost:8080/person_instance" in self.driver.current_url
        )

    def test_person_instance_content(self):
        # page for Aaron Bennink
        self.driver.get("http://localhost:8080/person_instance/4038")
        time.sleep(2)
        name_text = self.driver.find_elements_by_tag_name("h2")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(name_text, " Aaron Bennink ")
        about_text = self.driver.find_elements_by_tag_name("h3")[0].get_attribute(
            "innerHTML"
        )
        self.assertEqual(about_text, " About ")
        labels = self.driver.find_elements_by_xpath("//dt/p")
        required_labels = [
            "Main Charity",
            "Main Role",
            "Total Compensation",
            "Average Total Hours per Week",
        ]
        index = 0
        for label in labels:
            self.assertEqual(label.get_attribute("innerHTML"), required_labels[index])
            index += 1

    def test_person_instace_links(self):
        # check that links exist to country and people pages
        # Test country
        self.driver.get("localhost:8080/person_instance/4038")
        paths = self.driver.find_elements_by_tag_name("path")
        for path in paths:
            # only Canada should exist
            if path.get_attribute("data-tip") == "China":
                path.click()
                self.assertEqual(
                    self.driver.current_url,
                    "http://localhost:8080/country_instance/CHN",
                )
                break

    def test_filter_by_state(self):
        # filter by California
        self.driver.get("localhost:8080/charities_list")
        el = self.driver.find_elements_by_class_name("state-filter")
        options = el[0].find_elements_by_tag_name("option")
        # California is the second link
        options[2].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[1]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            self.assertTrue("CA" in card_paragraph)

    def test_filter_by_class(self):
        # filter by Educational Org
        self.driver.get("localhost:8080/charities_list")
        time.sleep(2)
        el = self.driver.find_elements_by_tag_name("select")
        options = el[1].find_elements_by_tag_name("option")
        # California is the second link
        options[2].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[4]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            self.assertTrue("Educational Organization" in card_paragraph)

    def test_sort_by_name(self):
        # sort by name
        self.driver.get("localhost:8080/charities_list")
        time.sleep(2)
        el = self.driver.find_elements_by_tag_name("select")
        options = el[2].find_elements_by_tag_name("option")
        # name is the  first option
        options[1].click()
        time.sleep(2)
        titles = self.driver.find_elements_by_class_name("name-title")
        prev_name = ""
        curr_name = ""
        for title in titles:
            curr_name = title.get_attribute("innerHTML")
            self.assertTrue(curr_name > prev_name)

    def test_sort_by_assets_descending(self):
        # sort by assets descending
        self.driver.get("localhost:8080/charities_list")
        time.sleep(2)
        el = self.driver.find_elements_by_tag_name("select")
        options = el[2].find_elements_by_tag_name("option")
        # assets is the  first option
        options[2].click()
        time.sleep(2)
        descending_div = self.driver.find_elements_by_class_name("descending")[0]
        descending_div.find_elements_by_tag_name("button")[0].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        prev = None
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[3]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            money = (
                card_paragraph.replace("<b>Asset Total:  </b>", "")
                .replace(",", "")
                .replace("$", "")
            )
            if money != "not disclosed":
                if prev == None:
                    prev = money
                else:
                    self.assertTrue(money < prev)
                    prev = money

    def test_filter_by_foreign_invst(self):
        # filter by foreign invst
        self.driver.get("localhost:8080/countries_list")
        el = self.driver.find_elements_by_tag_name("select")
        options = el[0].find_elements_by_tag_name("option")
        # California is the second link
        options[1].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[4]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            # make sure negative sign present
            self.assertTrue("-" in card_paragraph)

    #
    def test_filter_foreign_invst_sort_population(self):
        # filter by foreign invst
        self.driver.get("localhost:8080/countries_list")
        el = self.driver.find_elements_by_tag_name("select")
        options = el[0].find_elements_by_tag_name("option")
        # California is the second link
        options[1].click()
        time.sleep(2)
        options = el[2].find_elements_by_tag_name("option")
        # assets is the  first option
        options[2].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        prev = None
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[4]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            # make sure negative sign present
            self.assertTrue("-" in card_paragraph)
            population = (
                card_subtitle[1]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            population = population.replace("<b>Population:  </b>", "")
            if population != "Unknown":
                if prev == None:
                    prev = int(population)
                else:
                    self.assertTrue(int(population) >= prev)
                    prev = int(population)

    def test_filter_by_role(self):
        # filter by foreign invst
        self.driver.get("localhost:8080/persons_list")
        time.sleep(2)
        el = self.driver.find_elements_by_class_name("state-filter")
        options = el[0].find_elements_by_tag_name("option")
        # Secretary is the sixth link
        time.sleep(2)
        options[5].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[0]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            # make sure negative sign present
            self.assertTrue("Secretary" in card_paragraph)

    def test_filter_by_role_filter_by_active(self):
        # filter by foreign invst
        self.driver.get("localhost:8080/persons_list")
        time.sleep(2)
        el = self.driver.find_elements_by_tag_name("select")
        options = el[0].find_elements_by_tag_name("option")
        # Secretary is the sixth link
        time.sleep(2)
        options[5].click()
        time.sleep(2)
        options = el[1].find_elements_by_tag_name("option")
        time.sleep(2)
        options[2].click()
        time.sleep(2)
        cards = self.driver.find_elements_by_class_name("card-body")
        for card in cards:
            card_subtitle = card.find_elements_by_class_name("card-subtitle")
            card_paragraph = (
                card_subtitle[0]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            # make sure negative sign present
            self.assertTrue("Secretary" in card_paragraph)
            hours = (
                card_subtitle[3]
                .find_elements_by_class_name("lead")[0]
                .get_attribute("innerHTML")
            )
            hours = hours.replace("<b>Hours per Week:  </b>", "")
            self.assertTrue(int(hours) > 0)

    def test_search_charities(self):
        self.driver.get("localhost:8080/charities_list")
        time.sleep(2)
        el = self.driver.find_elements_by_class_name("model-input")
        el[0].send_keys("children")
        el[0].send_keys(Keys.ENTER)
        time.sleep(2)
        heading = self.driver.find_elements_by_tag_name("h1")[0]
        self.assertEqual(
            heading.get_attribute("innerHTML"), "Displaying search results for children"
        )
        results = self.driver.find_elements_by_class_name("result-wrapper")
        for result in results:
            self.assertTrue("children" in result.get_attribute("innerHTML").lower())

    def test_search_countries(self):
        self.driver.get("localhost:8080/countries_list")
        time.sleep(2)
        el = self.driver.find_elements_by_class_name("model-input")
        el[0].send_keys("repu")
        el[0].send_keys(Keys.ENTER)
        time.sleep(2)
        heading = self.driver.find_elements_by_tag_name("h1")[0]
        self.assertEqual(
            heading.get_attribute("innerHTML"), "Displaying search results for repu"
        )
        results = self.driver.find_elements_by_class_name("result-wrapper")
        for result in results:
            self.assertTrue("repu" in result.get_attribute("innerHTML").lower())

    def test_search_people(self):
        self.driver.get("localhost:8080/persons_list")
        time.sleep(2)
        el = self.driver.find_elements_by_class_name("model-input")
        el[0].send_keys("adam")
        el[0].send_keys(Keys.ENTER)
        time.sleep(2)
        heading = self.driver.find_elements_by_tag_name("h1")[0]
        self.assertEqual(
            heading.get_attribute("innerHTML"), "Displaying search results for adam"
        )
        results = self.driver.find_elements_by_class_name("result-wrapper")
        for result in results:
            self.assertTrue("adam" in result.get_attribute("innerHTML").lower())

    def test_search_global(self):
        self.driver.get("localhost:8080")
        time.sleep(2)
        el = self.driver.find_elements_by_class_name("global-input")[0]
        el.send_keys("chil")
        el.send_keys(Keys.ENTER)
        time.sleep(4)
        tabs = self.driver.find_elements_by_class_name("react-tabs__tab")
        tab_names = ["Charities", "Leaders", "Countries"]
        index = 0
        for tab in tabs:
            self.assertTrue(tab_names[index] in tab.get_attribute("innerHTML"))
            index += 1
            tab.click()
            time.sleep(2)
            heading = self.driver.find_elements_by_tag_name("h1")[0]
            self.assertEqual(
                heading.get_attribute("innerHTML"), "Displaying search results for chil"
            )
            results = self.driver.find_elements_by_class_name("result-wrapper")
            for result in results:
                self.assertTrue("chil" in result.get_attribute("innerHTML").lower())

    def test_pagination_content_one(self):
        self.driver.get("http://localhost:8080/persons_list")
        time.sleep(2)
        page_num = self.driver.find_elements_by_class_name("event_desc")[0]
        self.assertTrue("Page 1 of " in page_num.get_attribute("innerHTML"))
        first_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        next_page = self.driver.find_element_by_id("next_page")
        next_page.click()
        time.sleep(2)
        self.assertTrue("Page 2 of " in page_num.get_attribute("innerHTML"))
        new_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        self.assertNotEqual(new_title, first_title)
        prev_page = self.driver.find_element_by_id("prev_page")
        prev_page.click()
        time.sleep(2)
        self.assertTrue("Page 1 of " in page_num.get_attribute("innerHTML"))
        new_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        self.assertEqual(new_title, first_title)

    def test_pagination_content_two(self):
        self.driver.get("http://localhost:8080/charities_list")
        time.sleep(2)
        page_num = self.driver.find_elements_by_class_name("event_desc")[0]
        self.assertTrue("Page 1 of " in page_num.get_attribute("innerHTML"))
        first_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        last_page = self.driver.find_element_by_id("last_page")
        last_page.click()
        time.sleep(2)
        # make sure page number is last page number
        page_string = page_num.get_attribute("innerHTML")
        words = page_string.split(" ")
        self.assertTrue(words[1] == words[3])
        new_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        self.assertNotEqual(new_title, first_title)
        time.sleep(2)
        first_page = self.driver.find_element_by_id("first_page")
        first_page.click()
        time.sleep(2)
        self.assertTrue("Page 1 of " in page_num.get_attribute("innerHTML"))
        new_title = self.driver.find_elements_by_class_name("name-title")[
            0
        ].get_attribute("innerHTML")
        self.assertEqual(new_title, first_title)

    def tearDown(self):
        # close the browser window
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
