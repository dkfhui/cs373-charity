from application import db
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import func, select
import enum
from sqlalchemy.ext.associationproxy import association_proxy


class CharitiesPeopleAssociation(db.Model):
    __tablename__ = "charities_people_real"
    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("people_real_3.id"))
    charity_id = db.Column(db.Integer, db.ForeignKey("charities_real.id"))

    person = db.relationship("Person", back_populates="charities_assoc")
    charity = db.relationship("Charity", back_populates="people_assoc")

    role = db.Column(db.String(128))

    from_org_comp = db.Column(db.Integer)
    from_rltd_org_comp = db.Column(db.Integer)
    other_comp = db.Column(db.Integer)

    avg_hours = db.Column(db.Integer)

    @hybrid_property
    def total_comp(self):
        return self.from_org_comp + self.from_rltd_org_comp + self.other_comp

    @total_comp.expression
    def total_comp(cls):
        return func.sum(cls.from_org_comp + cls.from_rltd_org_comp + cls.other_comp)

    def __init__(
        self,
        person,
        charity,
        role,
        from_org_comp,
        from_rltd_org_comp,
        other_comp,
        avg_hours,
    ):
        self.person = person
        self.charity = charity
        self.role = role
        self.from_org_comp = from_org_comp
        self.from_rltd_org_comp = from_rltd_org_comp
        self.other_comp = other_comp
        self.avg_hours = avg_hours


class Person(db.Model):
    __tablename__ = "people_real_3"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    image_url = db.Column(db.Text)
    main_role = db.Column(db.String(128))
    main_charity_name = db.Column(db.String(128))
    total_avg_hours = db.Column(db.Integer)
    total_comp = db.Column(db.Integer)

    # @hybrid_property #creates a hybrid_property with name total_compensation and getter as defined below
    # def tota_compensation(self):
    # 	total = 0
    # 	for assoc in self.charities_assoc:
    # 		total += assoc.total_comp
    # 	return total
    #
    # @tota_compensation.expression
    # def tota_compensation(cls):
    # 	return (select([func.sum(CharitiesPeopleAssociation.from_org_comp +  CharitiesPeopleAssociation.from_rltd_org_comp +  CharitiesPeopleAssociation.other_comp)]).
    #             where(CharitiesPeopleAssociation.person_id == cls.id).
    #             label("total_compensation")
    #             )
    #
    # @hybrid_property
    # def tota_avg_hours(self):
    # 	return sum(assoc.avg_hours for assoc in self.charities_assoc)
    #
    # @tota_avg_hours.expression
    # def tota_avg_hours(cls):
    # 	return (select([func.sum(CharitiesPeopleAssociation.avg_hours)]).
    #             where(CharitiesPeopleAssociation.person_id == cls.id).
    #             label("total_avg_hours")
    #             )
    #
    @hybrid_property
    def main_charity_assoc(self):

        iterator = iter(self.charities_assoc)
        charity_most_hours = next(iterator)
        max_hours = charity_most_hours.avg_hours

        for charity in iterator:
            if max_hours < charity.avg_hours:
                max_hours = charity.avg_hours
                charity_most_hours = charity

        return charity_most_hours

    #
    @hybrid_property
    def main_charity(self):
        return self.main_charity_assoc.charity

    #
    # @mai_charity.expression
    # def mai_charity(cls):
    # 	return (select([CharitiesPeopleAssociation.charity]).
    #             where(CharitiesPeopleAssociation.person_id == cls.id).
    # 			order_by(CharitiesPeopleAssociation.avg_hours.desc()).limit(1).
    #             label("main_charity")
    #             )
    #
    # @hybrid_property
    # def mai_role(self):
    # 	return self.mai_charity_assoc.role
    #
    # @mai_role.expression
    # def mai_role(cls):
    # 	return (select([CharitiesPeopleAssociation.role]).
    #             where(CharitiesPeopleAssociation.person_id == cls.id).
    # 			order_by(CharitiesPeopleAssociation.avg_hours.desc()).limit(1).
    #             label("main_role")
    #             )

    charities_assoc = db.relationship("CharitiesPeopleAssociation")

    # Will also populate Charity
    charities = db.relationship(
        "Charity", secondary="charities_people_real", back_populates="people"
    )
    countries = association_proxy("charities", "countries")

    def __init__(self, args):
        self.name = args["name"]
        self.image_url = args["image_url"] if "image_url" in args else None
        self.main_role = args["main_role"] if "main_role" in args else None
        self.main_charity_name = (
            args["main_charity_name"] if "main_charity_name" in args else None
        )
        self.total_avg_hours = (
            args["total_avg_hours"] if "total_avg_hours" in args else None
        )
        self.total_comp = args["total_comp"] if "total_comp" in args else None

    def __repr__(self):
        return "<Person %d>" % (self.id)

    def get_attributes(self):
        attr = {
            "id": self.id,
            "name": self.name,
            "total_compensation": self.total_comp,
            "total_avg_hours": self.total_avg_hours,
            "main_charity": self.main_charity_name,
            "main_role": self.main_role,
            "image_url": self.image_url,
        }
        return {"person": attr}

    def get_complete_attributes(self):
        attributes = {
            "person": {
                "id": self.id,
                "name": self.name,
                "total_compensation": self.total_comp,
                "total_avg_hours": self.total_avg_hours,
                "main_charity": {
                    "ein": self.main_charity.ein,
                    "name": self.main_charity_name,
                },
                "main_role": self.main_role,
                "image_url": self.image_url,
            }
        }
        related = {
            "charities": [
                {
                    "name": charity_ass.charity.name,
                    "ein": charity_ass.charity.ein,
                    "img_url": charity_ass.charity.image_url,
                    "org_comp": charity_ass.from_org_comp,
                    "rlt_org_comp": charity_ass.from_rltd_org_comp,
                    "other_comp": charity_ass.other_comp,
                    "avg_hours": charity_ass.avg_hours,
                    "role": charity_ass.role,
                }
                for charity_ass in self.charities_assoc
            ],
            "countries": [
                {"name": country.name, "abbr": country.abbr}
                for country_list in self.countries
                for country in country_list
            ],
        }
        attributes["person"] = {**attributes["person"], **related}
        return attributes
