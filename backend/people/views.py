import os, json
from flask import Flask, send_from_directory, request, abort
from application import engine, db
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker, load_only
from .models import Person, CharitiesPeopleAssociation
from werkzeug.exceptions import HTTPException


def get_person(id):
    # make sure numeric id passed
    if id.isdigit() == False:
        abort(400)

    res = Person.query.get(id)

    if res == None:
        abort(404)

    return json.dumps(res.get_complete_attributes())


def get_people():
    # Defaults
    start = 1
    num_instances = 9

    startArg = request.args.get("start")
    if startArg != None:
        start = int(startArg)

    limitArg = request.args.get("limit")
    if startArg != None:
        num_instances = int(limitArg)

    try:
        persons_list = Person.query

        # Apply filters
        name = request.args.get("name")
        if name != None:
            if name == "":
                abort(400)
            persons_list = persons_list.filter(
                func.lower(Person.name).contains(name.lower())
            )

        role = request.args.get("role")
        non_unique = {
            "Director",
            "Board Member",
            "Trustee",
            "Treasurer",
            "Secretary",
            "Member",
            "President",
            "Vice President",
            "Chairman",
            "Vice Chairman",
            "Chair",
            "Executive Trustee",
            "Chief Financial Officer",
            "Executive Director",
            "Chief Executive Officer",
        }
        if role != None:
            if role == "":
                abort(400)
            elif role == "unique":
                persons_list = persons_list.filter(~Person.main_role.in_(non_unique))
            else:
                # get the objects from charities_people table who match the role
                persons_list = persons_list.filter(
                    func.lower(Person.main_role) == role.lower()
                )

        active = request.args.get("active")
        if active != None:
            if active == "":
                abort(400)
            elif active == "true":
                persons_list = persons_list.filter(Person.total_avg_hours > 0)
                for person in persons_list:
                    print(person.name, person.total_avg_hours)
            elif active == "false":
                persons_list = persons_list.filter(Person.total_avg_hours == 0)
            else:
                abort(400)

        # Apply sorting if necessary
        orderby = request.args.get("orderby")
        desc = request.args.get("desc")
        if desc != None:
            if desc == "true":
                desc = True
        if orderby != None:
            if orderby == "name":
                if desc == True:
                    persons_list = persons_list.order_by(Person.name.desc())
                else:
                    persons_list = persons_list.order_by(Person.name)
            elif orderby == "main_charity":
                if desc == True:
                    persons_list = persons_list.order_by(
                        Person.main_charity_name.desc()
                    )
                else:
                    persons_list = persons_list.order_by(Person.main_charity_name)
            elif orderby == "main_role":
                if desc == True:
                    persons_list = persons_list.order_by(Person.main_role.desc())
                else:
                    persons_list = persons_list.order_by(Person.main_role)
            elif orderby == "weekly_hours":
                if desc == True:
                    persons_list = persons_list.order_by(Person.total_avg_hours.desc())
                else:
                    persons_list = persons_list.order_by(Person.total_avg_hours)
            elif orderby == "compensation":
                if desc == True:
                    persons_list = persons_list.order_by(Person.total_comp.desc())
                else:
                    persons_list = persons_list.order_by(Person.total_comp)
            else:
                abort(400)
        else:
            persons_list = persons_list.order_by(
                Person.id.desc()
            )  # order by name default

        # # Paginate
        pagination = persons_list.paginate(start, per_page=num_instances)
        if len(pagination.items) < 1:
            abort(404)
        result = {"people": [person.get_attributes() for person in pagination.items]}
        result["num_pages"] = pagination.pages
        return json.dumps(result)

    except HTTPException:
        raise
    except Exception as e:
        # print(e)
        abort(500)
        # return ""
        # raise


# parameter: search term, start page ,limit
def search_people():
    from charities import Charity

    # Defaults
    page = 1
    num_instances = 9
    q = Charity.query
    # get start parameter
    startArg = request.args.get("start")
    if startArg != None:
        page = int(startArg)
        if page <= 0:  # pages are 1 indexed
            abort(400)
    # get limit parameter
    limitArg = request.args.get("limit")
    if limitArg != None:
        num_instances = int(limitArg)
        if num_instances < 0 or num_instances > 50:
            abort(400)
    # start search algorithm
    searchTerm = request.args.get("q")
    if searchTerm == None:
        abort(400)
    elif searchTerm == "":
        abort(404)
    else:
        terms = searchTerm.split()
        results_dict = {}
        people_list = Person.query
        priorities = {"name": [10, 6], "main_role": [4, 2], "main_charity": [4, 2]}
        # handle the whole search term
        if len(terms) > 1:
            terms.insert(0, searchTerm)
        index = 0
        for term in terms:
            # do name first
            sub_list = people_list.filter(Person.name.ilike("%" + term + "%"))
            for person in sub_list:
                if person.id in results_dict:
                    results_dict[person.id]["priority"] += priorities["name"][
                        index != 0
                    ]
                    results_dict[person.id]["fields"]["name"] = person.name
                else:
                    results_dict[person.id] = {
                        "priority": priorities["name"][index != 0],
                        "fields": {"name": person.name},
                    }
                results_dict[person.id]["fields"]["name"] = person.name
            # do tag line next
            sub_list = people_list.filter(Person.main_role.ilike("%" + term + "%"))
            for person in sub_list:
                if person.id in results_dict:
                    results_dict[person.id]["priority"] += priorities["main_role"][
                        index != 0
                    ]
                    results_dict[person.id]["fields"]["main_role"] = person.main_role
                else:
                    results_dict[person.id] = {
                        "priority": priorities["main_role"][index != 0],
                        "fields": {"main_role": person.main_role},
                    }
                results_dict[person.id]["fields"]["name"] = person.name
            # do tag line next
            sub_list = people_list.filter(
                Person.main_charity_name.ilike("%" + term + "%")
            )
            for person in sub_list:
                if person.id in results_dict:
                    results_dict[person.id]["priority"] += priorities["main_charity"][
                        index != 0
                    ]
                    results_dict[person.id]["fields"][
                        "main_charity"
                    ] = person.main_charity_name
                else:
                    results_dict[person.id] = {
                        "priority": priorities["main_charity"][index != 0],
                        "fields": {"main_charity": person.main_charity_name},
                    }
                results_dict[person.id]["fields"]["name"] = person.name
        sorted_people = sorted(
            results_dict.items(), key=lambda kv: kv[1]["priority"], reverse=True
        )
        # now create and return response
        num_pages = (len(sorted_people) // num_instances) + (
            (len(sorted_people) % num_instances) != 0
        )
        if page > num_pages:
            abort(404)
        else:
            results = sorted_people[(page - 1) * num_instances : page * num_instances]
            json_res = {"results": results, "num_pages": num_pages, "page": page}
            return json.dumps(json_res)


def get_all_roles():
    main_roles = {}
    main_roles["main_roles"] = [
        "Director",
        "Board Member",
        "Trustee",
        "Treasurer",
        "Secretary",
        "Member",
        "President",
        "Vice President",
        "Chairman",
        "Vice Chairman",
        "Chair",
        "Executive Trustee",
        "Chief Financial Officer",
        "Executive Director",
        "Chief Executive Officer",
        "Unique",
    ]
    return json.dumps(main_roles)
