import os, json
from flask import Flask, send_from_directory, request, abort
from application import engine
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from .models import Country
from werkzeug.exceptions import HTTPException
from news import news_helper
from helpers import *


def get_country_by_id(id):
    if id < 0:
        abort(400)

    res = Country.query.filter(Country.id == id)

    if res.count() < 1:
        abort(404)
    elif res.count() > 1:
        abort(500)
    else:
        result = res.first().get_complete_attributes()
        result["news"] = news_helper(result["country"]["name"])
        return json.dumps(result)


def get_country_by_abbr(abbr):

    abbr = abbr.upper()
    if len(abbr) != 3 or not abbr.isalpha():
        abort(400)

    res = Country.query.filter(Country.abbr == abbr)

    if res.count() < 1:
        abort(404)
    elif res.count() > 1:
        abort(500)
    else:
        result = res.first().get_complete_attributes()
        result["news"] = news_helper(result["country"]["name"])
        return json.dumps(result)


# possible arguments:
# name, associated-person, start, limit, sort, distance-to-person
def get_countries():

    # Defaults
    page = 1
    num_instances = 9

    startArg = request.args.get("start")
    if startArg != None:
        page = int(startArg)

    limitArg = request.args.get("limit")
    if startArg != None:
        num_instances = int(limitArg)

    try:
        # Do base query
        countries_list = Country.query

        # Apply any filters
        abbr = request.args.get("abbr")
        if abbr != None:
            if abbr == "":
                abort(400)
            countries_list = countries_list.filter(
                func.lower(Country.abbr).contains(abbr.lower())
            )

        name = request.args.get("name")
        if name != None:
            if abbr == "":
                abort(400)
            countries_list = countries_list.filter(
                func.lower(Country.name).contains(name.lower())
            )

        foreign_invst_filter = request.args.get("foreign_investment_pct")
        if foreign_invst_filter != None:
            if foreign_invst_filter == "":
                abort(400)
            elif foreign_invst_filter == "NEGATIVE":
                countries_list = countries_list.filter(
                    Country.foreign_investment_pct < 0
                )
            elif foreign_invst_filter == "POSITIVE_FRACTION":
                countries_list = countries_list.filter(
                    Country.foreign_investment_pct >= 0
                ).filter(Country.foreign_investment_pct <= 1)
            elif foreign_invst_filter == "POSITIVE_LARGE":
                countries_list = countries_list.filter(
                    Country.foreign_investment_pct > 1
                )
            else:
                abort(400)

        gdp = request.args.get("gdp_per_capita")
        if gdp != None:
            if gdp == "":
                abort(400)
            elif gdp == "LOW":
                countries_list = countries_list.filter(Country.gdp_per_capita <= 1005)
            elif gdp == "LOWER-MIDDLE":
                countries_list = countries_list.filter(
                    Country.gdp_per_capita > 1005
                ).filter(Country.gdp_per_capita <= 3955)
            elif gdp == "UPPER-MIDDLE":
                countries_list = countries_list.filter(
                    Country.gdp_per_capita > 3955
                ).filter(Country.gdp_per_capita <= 12235)
            elif gdp == "HIGH":
                countries_list = countries_list.filter(Country.gdp_per_capita > 12235)
            else:
                abort(400)

        # Apply sorting if necessary
        orderby = request.args.get("orderby")
        desc = request.args.get("desc")
        if desc != None and desc == "true":
            desc = True
        if orderby != None:
            if orderby == "name":
                if desc == True:
                    countries_list = countries_list.order_by(Country.name.desc())
                else:
                    countries_list = countries_list.order_by(Country.name)
            elif orderby == "population":
                if desc == True:
                    countries_list = countries_list.order_by(Country.population.desc())
                else:
                    countries_list = countries_list.order_by(Country.population)
            elif orderby == "gdp_per_capita":
                if desc == True:
                    countries_list = countries_list.order_by(
                        Country.gdp_per_capita.desc()
                    )
                else:
                    countries_list = countries_list.order_by(Country.gdp_per_capita)
            elif orderby == "gender_gap_index":
                if desc == True:
                    countries_list = countries_list.order_by(
                        Country.gender_gap_index.desc()
                    )
                else:
                    countries_list = countries_list.order_by(Country.gender_gap_index)
            elif orderby == "foreign_investment_pct":
                if desc == True:
                    countries_list = countries_list.order_by(
                        Country.foreign_investment_pct.desc()
                    )
                else:
                    countries_list = countries_list.order_by(
                        Country.foreign_investment_pct
                    )
            else:
                abort(400)

        countries_list = countries_list.paginate(page, per_page=num_instances)

        if len(countries_list.items) < 1:
            abort(404)

        result = {
            "countries": [country.get_attributes() for country in countries_list.items]
        }

        result["num_pages"] = countries_list.pages
        return json.dumps(result)

    except HTTPException:
        raise
    except Exception as e:
        abort(500)
        # print(e)
        # return ""

# returns a list of all countries with a list of all charities per country
def get_charities_by_country():
    try:
        # Do base query
        countries_list = Country.query
        result = {
            "countries": [country.get_charities_only() for country in countries_list]
        }
        print('HEY')
        return json.dumps(result)

    except HTTPException:
        raise
    except Exception as e:
        abort(500)


# parameter: search term, start page ,limit
def search_countries():
    # Defaults
    page = 1
    num_instances = 9
    # get start parameter
    startArg = request.args.get("start")
    if startArg != None:
        page = int(startArg)
        if page <= 0:  # pages are 1 indexed
            abort(400)
    # get limit parameter
    limitArg = request.args.get("limit")
    if limitArg != None:
        num_instances = int(limitArg)
        if num_instances < 0 or num_instances > 50:
            abort(400)
    # start search algorithm
    searchTerm = request.args.get("q")
    if searchTerm == None:
        abort(400)
    elif searchTerm == "":
        abort(404)
    else:
        terms = searchTerm.split()
        results_dict = {}
        countries_list = Country.query
        priorities = {"name": [10, 6], "abbr": [8, 5], "blurb": [4, 2]}
        # handle the whole search term
        if len(terms) > 1:
            terms.insert(0, searchTerm)
        index = 0
        for term in terms:
            # do name first
            sub_list = countries_list.filter(Country.name.ilike("%" + term + "%"))
            for country in sub_list:
                if country.abbr in results_dict:
                    results_dict[country.abbr]["priority"] += priorities["name"][
                        index != 0
                    ]
                    results_dict[country.abbr]["fields"]["name"] = country.name
                else:
                    results_dict[country.abbr] = {
                        "priority": priorities["name"][index != 0],
                        "fields": {"name": country.name},
                    }
                results_dict[country.abbr]["fields"]["name"] = country.name
            # do tag line next
            sub_list = countries_list.filter(Country.abbr.ilike("%" + term + "%"))
            for country in sub_list:
                if country.abbr in results_dict:
                    results_dict[country.abbr]["priority"] += priorities["abbr"][
                        index != 0
                    ]
                    results_dict[country.abbr]["fields"]["abbr"] = country.abbr
                else:
                    results_dict[country.abbr] = {
                        "priority": priorities["abbr"][index != 0],
                        "fields": {"abbr": country.abbr},
                    }
                results_dict[country.abbr]["fields"]["name"] = country.name
            # do tag line next
            sub_list = countries_list.filter(Country.blurb.ilike("%" + term + "%"))
            for country in sub_list:
                # only get a portion of the mission statement, include up to 10 words before and after
                statement = getTextPortion(country.blurb, term)
                if country.abbr in results_dict:
                    results_dict[country.abbr]["priority"] += priorities["blurb"][
                        index != 0
                    ]
                    results_dict[country.abbr]["fields"]["blurb"] = statement
                else:
                    results_dict[country.abbr] = {
                        "priority": priorities["blurb"][index != 0],
                        "fields": {"blurb": statement},
                    }
                results_dict[country.abbr]["fields"]["name"] = country.name
        sorted_countries = sorted(
            results_dict.items(), key=lambda kv: kv[1]["priority"], reverse=True
        )
        # now create and return response
        num_pages = (len(sorted_countries) // num_instances) + (
            (len(sorted_countries) % num_instances) != 0
        )
        if page > num_pages:
            abort(404)
        else:
            results = sorted_countries[
                (page - 1) * num_instances : page * num_instances
            ]
            json_res = {"results": results, "num_pages": num_pages, "page": page}
            return json.dumps(json_res)

def num_charities_per_country():
    countries_list = Country.query
    result = {'countries': {}}
    for country in countries_list:
        result['countries'][country.abbr] = {"name": country.name, "num_charities": len(country.charities)}
    return json.dumps(result)
