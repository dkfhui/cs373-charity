from application import db
from sqlalchemy.ext.associationproxy import association_proxy


class Country(db.Model):
    __tablename__ = "countries_real"
    id = db.Column(db.Integer, primary_key=True)
    abbr = db.Column(db.String(3), unique=True)
    name = db.Column(db.String(30), unique=True)
    population = db.Column(db.Integer)
    gdp_per_capita = db.Column(db.Float)
    gender_gap_index = db.Column(db.Float)
    life_exp_rating = db.Column(db.Float)
    foreign_investment_pct = db.Column(db.Float)  # (% of GDP)
    research_and_development = db.Column(db.Float)  # expenditure (% of GDP)
    vc_availability = db.Column(db.Float)  # venture capital 1-7 (best)
    inflation_pct = db.Column(db.Float)  # annual %
    unemployment_pct = db.Column(db.Float)  # % of total labor force
    blurb = db.Column(db.Text)

    charities = db.relationship(
        "Charity", secondary="charities_countries_real", back_populates="countries"
    )
    people = association_proxy("charities", "people")

    def __init__(self, args):
        self.abbr = args["abbr"] if "abbr" in args else ""
        self.name = args["name"]
        self.population = args["population"] if "population" in args else None
        self.gdp_per_capita = (
            args["gdp_per_capita"] if "gdp_per_capita" in args else None
        )
        self.foreign_investment_pct = (
            args["foreign_investment_pct"] if "foreign_investment_pct" in args else None
        )
        self.research_and_development = (
            args["research_and_development"]
            if "research_and_development" in args
            else None
        )
        self.vc_availability = (
            args["vc_availability"] if "vc_availability" in args else None
        )
        self.inflation_pct = args["inflation_pct"] if "inflation_pct" in args else None
        self.unemployment_pct = (
            args["unemployment_pct"] if "unemployment_pct" in args else None
        )
        self.gender_gap_index = (
            args["gender_gap_index"] if "gender_gap_index" in args else None
        )
        self.life_exp_rating = (
            args["life_exp_rating"] if "life_exp_rating" in args else None
        )
        self.blurb = args["blurb"] if "blurb" in args else None

    def __repr__(self):
        return "<Data %r>" % self.name

    def get_attributes(self):
        attr = {
            "abbr": self.abbr,
            "name": self.name,
            "population": self.population,
            "gdp_per_capita": self.gdp_per_capita,
            "foreign_investment_pct": self.foreign_investment_pct,
            "gender_gap_index": self.gender_gap_index,
            "image_url": "https://restcountries.eu/data/" + self.abbr.lower() + ".svg",
        }
        return {"country": attr}

    def get_complete_attributes(self):
        attributes = {
            "country": {c.name: getattr(self, c.name) for c in self.__table__.columns}
        }
        attributes["country"]["image_url"] = (
            "https://restcountries.eu/data/" + self.abbr.lower() + ".svg"
        )
        related = {
            "charities": [
                {"name": charity.name, "ein": charity.ein, "img_url": charity.image_url}
                for charity in self.charities
            ],
            "people": [
                {"name": person.name, "id": person.id}
                for people_list in self.people
                for person in people_list
            ],
        }
        attributes["country"] = {**attributes["country"], **related}
        return attributes

    def get_charities_only(self):
        attributes = {
            "country": { 'abbr':self.abbr, 'name':self.name }
        }
        related = {
            "charities": [
                {"name": charity.name, "ein": charity.ein}
                for charity in self.charities
            ]
        }
        attributes["country"] = {**attributes["country"], **related}
        return attributes

