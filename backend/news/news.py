# import os, json
from flask import Flask, send_from_directory, request, abort

# from application import engine
# from sqlalchemy import create_engine, func
# from sqlalchemy.orm import sessionmaker
# from .models import Person
# from werkzeug.exceptions import HTTPException
import json
import requests


def get_news(name):
    return json.dumps(news_helper(name))


def news_helper(name):
    try:
        url = (
            "https://newsapi.org/v2/everything?language=en&apiKey=f8c3fb0b4448425bb2db27c81772c19b&q="
            + name
            + "&sortBy=relevance"
        )
        contents = requests.get(url)
        json_res = json.loads(contents.text)
        return json_res
    except Exception as e:
        abort(500)
        # print(e)
        # return ""
