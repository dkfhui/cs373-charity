import re


def getTextPortion(originalText, matchTerm):
    m = re.search(
        "(\S+\s+){0,10}\S*" + matchTerm + "\s*(\S+\s+){0,9}\S+",
        originalText,
        re.IGNORECASE,
    )
    statement = m.group(0).replace("\s{2, }", " ")
    # add elipses depending on whether this match is in the middle of the string
    if m.start(0) != 0:
        statement = "..." + statement
    if m.end(0) != (len(originalText)):
        statement = statement + "..."
    return statement
