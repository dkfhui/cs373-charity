#!/usr/bin/env python
import os, json
from flask import Flask, send_from_directory, request, abort
from application import application, engine
import charities
import countries
import people
import news

STATIC_DIR = "frontend/build"

application.add_url_rule(
    "/charity", "get_charities", view_func=charities.views.get_charities
)
application.add_url_rule(
    "/charity/<ein>", "get_charity", view_func=charities.views.get_charity
)
application.add_url_rule(
    "/charity-states", "get_all_states", view_func=charities.views.get_all_states
)
application.add_url_rule(
    "/charity-classifications",
    "get_all_classifications",
    view_func=charities.views.get_all_classifications,
)
application.add_url_rule(
    "/charity-search", "search_charities", view_func=charities.views.search_charities
)

application.add_url_rule(
    "/countries_per_charity", "get_countries_per_charity", view_func=charities.views.get_countries_per_charity
)

application.add_url_rule(
    "/country", "get_countries", view_func=countries.views.get_countries
)
application.add_url_rule(
    "/country/<int:id>",
    "get_country_by_id",
    view_func=countries.views.get_country_by_id,
)
application.add_url_rule(
    "/country/<string:abbr>",
    "get_country_by_abbr",
    view_func=countries.views.get_country_by_abbr,
)
application.add_url_rule(
    "/charities_per_country_info", "get_charities_by_country", view_func=countries.views.get_charities_by_country
)
application.add_url_rule(
    "/country-search", "search_countries", view_func=countries.views.search_countries
)
application.add_url_rule(
    "/charities_per_country", "num_charities_per_country", view_func=countries.num_charities_per_country
)

application.add_url_rule("/person", "get_people", view_func=people.views.get_people)
application.add_url_rule(
    "/person/<id>", "get_person", view_func=people.views.get_person
)
application.add_url_rule(
    "/person-roles", "get_all_roles", view_func=people.views.get_all_roles
)
application.add_url_rule(
    "/person-search", "search_people", view_func=people.views.search_people
)

application.add_url_rule(
    "/news/<string:name>", "get_news", view_func=news.news.get_news
)


@application.route("/country/")
@application.route("/charity/")
@application.route("/person/")
def badRoute():
    abort(400)


@application.errorhandler(400)
def error400(e):
    return "it seems you made a mistake (check your call and arguments)", 400


@application.errorhandler(404)
def error404(e):
    return "no results were found for your search", 404


@application.errorhandler(500)
def error500(e):
    return "oops we made a mistake", 500


if __name__ == "__main__":
    application.run(host="127.0.0.1", port=8081)
