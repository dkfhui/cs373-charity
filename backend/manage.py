from flask_script import Manager
from application import application
from scripts.charity_scraper import CharityScraper
from scripts.country_scraper import CountryScraper
from scripts.people_scraper import PeopleScraper
from scripts.db_create import DBTableCreate
from scripts.charity_images_scraper import CharityImageScraper
from scripts.people_images_scraper import PeopleImageScraper
from scripts.country_blurb_scraper import CountryBlurbScraper
from scripts.people_attributes_scraper import PeopleAttributesScraper

manager = Manager(application)

manager.add_command("scrape_charities", CharityScraper)
manager.add_command("scrape_countries", CountryScraper)
manager.add_command("scrape_people", PeopleScraper)
manager.add_command("create_tables", DBTableCreate)
manager.add_command("scrape_charity_images", CharityImageScraper)
manager.add_command("scrape_people_images", PeopleImageScraper)
manager.add_command("scrape_country_blurbs", CountryBlurbScraper)
manager.add_command("scrape_people_attributes", PeopleAttributesScraper)

if __name__ == "__main__":
    manager.run()
